#!/usr/bin/env rackup
# encoding: utf-8

# This file can be used to start Padrino,
# just execute it from the command line.

require File.expand_path("../config/boot.rb", __FILE__)

require "dotenv"
Dotenv.load(".env.#{ENV['RACK_ENV']}.local", ".env.#{ENV['RACK_ENV']}", ".env")

run Padrino.application
