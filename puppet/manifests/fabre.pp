node default {
  package { "nginx":
    ensure => present
  }

  package { "git":
    ensure => present
  }

  package { "gnupg":
    ensure => present,
  }

  exec { "wget https://packages.groonga.org/debian/groonga-archive-keyring-latest-buster.deb":
    cwd => "/tmp",
    creates => "/tmp/groonga-archive-keyring-latest-buster.deb",
    path => "/usr/bin",
  }

  exec { "sudo apt install -y -V ./groonga-archive-keyring-latest-buster.deb":
    cwd => "/tmp",
    creates => "/tmp/groonga-archive-keyring-latest-buster.deb",
    path => "/usr/bin",
  }

  exec { "sudo apt update":
    cwd => "/tmp",
    path => "/usr/bin",
  }

  package { "groonga-httpd":
    ensure => present,
  }

  package { "libgroonga-dev":
    ensure => present,
  }

  package { "ruby-bundler":
    ensure => present,
  }

  package { "ruby-dev":
    ensure => present,
  }

  package { "make":
    ensure => present,
  }

  package { "gcc":
    ensure => present,
  }

  package { "postgresql-server-dev-11":
    ensure => present,
  }

  package { "libmagic-dev":
    ensure => present,
  }

  package { "libsqlite3-dev":
    ensure => present,
  }

  package { "gnupg2":
    ensure => present,
  }

  group { "fabre":
    ensure => present,
  }

  user { "fabre":
    ensure => present,
    home => "/var/lib/fabre",
    managehome => true,
    shell => "/bin/bash",
  }

  exec { "git clone https://salsa.debian.org/debbugs-team/debbugs.git":
    cwd => "/var/lib/fabre",
    creates => "/var/lib/fabre/debbugs/.git",
    path => "/usr/bin",
    user => "fabre"
  }

  file { "/var/lib/fabre/data":
    owner => "fabre",
    group => "fabre",
    ensure => directory,
  }

  file { "/var/lib/fabre/data/db":
    owner => "fabre",
    group => "fabre",
    ensure => directory,
  }

}
