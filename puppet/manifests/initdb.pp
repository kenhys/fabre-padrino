exec { "cat /var/lib/fabre/fabre-padrino/db/schema.grn | groonga -n data/db/fabre.db":
  cwd => "/var/lib/fabre",
  creates => "/var/lib/fabre/data/db/fabre.db",
  path => "/usr/bin",
  user => "fabre"
}

exec { "cat /var/lib/fabre/fabre-padrino/db/master.grn | groonga data/db/fabre.db":
  cwd => "/var/lib/fabre",
  path => "/usr/bin",
  user => "fabre"
}
