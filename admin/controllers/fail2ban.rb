Fabre::Admin.controllers :fail2ban do
  get :fail2ban_sshd, :map => "/fail2ban/sshd" do

    db = MaxMindDB.new('/var/lib/GeoIP/GeoLite2-City.mmdb', MaxMindDB::LOW_MEMORY_FILE_READER)
    log_path = File.join(ENV["FABRE_DATA_DIR"], "logs", "fail2ban", "fail2ban.log")
    if File.exist?(log_path)
      @records = []
      File.open(log_path) do |file|
        file.readlines.each do |line|
          #p line.chomp
          case line.chomp
          when /(\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}),\d{3}\sfail2ban.(\w+)\s+.+\[sshd\]\s(\w+)\s(.+)\s-\s(.+)$/
            timestamp = Time.parse($1)
            banaction = $3
            ip = $4
            lookup = db.lookup(ip)
            record = {
              timestamp: Time.parse($1),
              action: $3,
              ip: $4,
              continent: lookup["continent"]["names"]["en"],
              country: lookup["country"]["names"]["en"]
            }
            @records << record
          end
        end
      end
    end
    render "fail2ban/sshd"
  end

  get :fail2ban_botsearch, :map => "/fail2ban/botsearch" do
    render "fail2ban/botsearch"
  end
end
