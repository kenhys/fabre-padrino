Fabre::Admin.controllers :nginx do
  get :nginx_access, :map => "/nginx/access" do
    render "nginx/access"
  end

  get :nginx_error, :map => "/nginx/error" do
    render "nginx/error"
  end
end
