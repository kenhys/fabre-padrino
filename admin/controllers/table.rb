Fabre::Admin.controllers :table do
  get :table_sessions, :map => "/table/sessions" do
    @database = Fabre::Database.new
    @database.open do
      @session = Fabre::Sessions.new
      @records = @session.all
      render "table/sessions"
    end
  end

  get :table_users, :map => "/table/users" do
    @database = Fabre::Database.new
    @database.open do
      @user = Fabre::Users.new
      @records = @user.all.sort([{
                                  key: "updated_at",
                                  order: :descending
                                }], limit: 20)
      render "table/users"
    end
  end

  get :table_service_logs, :map => "/table/servicelogs" do
    @database = Fabre::Database.new
    @database.open do
      @service = Fabre::ServiceLogs.new
      @records = @service.all.sort([{
                                  key: "timestamp",
                                  order: :descending
                                }], limit: 20)
      render "table/servicelogs"
    end
  end

  get :table_service_queues, :map => "/table/servicequeues" do
    @database = Fabre::Database.new
    @database.open do
      @service = Fabre::ServiceQueues.new
      @records = @service.all.sort([{
                                  key: "updated_at",
                                  order: :descending
                                }], limit: 20)
      render "table/servicequeues"
    end
  end
end
