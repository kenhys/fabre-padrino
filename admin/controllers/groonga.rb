Fabre::Admin.controllers :groonga do
  get :groonga_log, :map => "/groonga/log" do
    log_path = File.join(ENV["FABRE_DATA_DIR"], "logs", "groonga", "groonga.log")
    @records = []
    File.open(log_path) do |file|
      parser = GroongaLog::Parser.new
      @records = parser.parse(file.read).to_a
    end
    render "groonga/log"
  end

  get :groonga_query, :map => "/groonga/query" do
    render "groonga/query"
  end
end
