Fabre::App.controllers :homes do
  get :home, map: "/home" do
    check_x_fabre_account
    render "homes/index"
  end

  get :index, map: "/" do
    check_x_fabre_account
    render "homes/index"
  end
end
