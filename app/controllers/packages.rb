Fabre::App.controllers :packages do
  get "/" do
    @elapsed_time = 0
    render "packages/index"
  end

  get :tag do
    check_x_fabre_account
    open_database do
      @reporters = Fabre::Reporters.new
      before = Time.now
      @records = @reporters.lists_debian_org
      @elapsed_time = Time.now - before
      render "packages/tag"
    end
  end

  get :maintainer, with: :email do
    check_x_fabre_account
    open_database do
      @bugs = Fabre::Client::Bugs.new
      before = Time.now
      @records = @bugs.maintainer_bugs(params[:email])
      p @records.size
      @elapsed_time = Time.now - before
      @maintainer = params[:email]
      render "packages/maintainer"
    end
  end

  get :installed do
    check_x_fabre_account
    open_database do
      @users = Fabre::Users.new
      before = Time.now
      @user = @users.find_by_key(params[:email])
      if @user
        keys = @user[:installed].collect(&:_key)
        p keys
        @bugs = Fabre::Bugs.new
        @installed = @bugs.find_by_keys(keys)
      end
      @elapsed_time = Time.now - before
      render "packages/installed"
    end
  end
end
