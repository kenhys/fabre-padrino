Fabre::App.controllers :settings do
  post :quit, map: "/quit" do
    check_x_fabre_account
    session[:usename] = nil
    session[:avatar] = nil
    session[:name] = nil
    session[:weburl] = nil
    open_database do
      @session = Fabre::Client::Sessions.new
      @session.remove({
                        key: salsa_username
                      })
      @user = Fabre::Users.new
      @user.remove({
                     key: salsa_username
                   })
      session[:username] = nil
      session[:name] = nil
      session[:avatar] = nil
      session[:weburl] = nil
      session[:email] = nil
    end
    render "settings/quit"
  end

  get :settings, map: "/debian/settings" do
    check_x_fabre_account
    open_database do
      @users = Fabre::Client::Users.new
      @timezones = TZInfo::Timezone.all_country_zone_identifiers
      @user = @users.find_by_username(session[:username])
      render "settings/show"
    end
  end

  post :popcon, map: "/settings/popcon" do
    check_x_fabre_account
    open_database do
      @users = Fabre::Client::Users.new
      options = {
        key: params[:username],
        api_key: params[:api_key]
      }
      if @users.api_authenticated?(options)
        path = File.join(ENV["FABRE_DATA_DIR"],
                         "uploads",
                         "#{params[:username]}.dat")
        FileUtils.copy(params[:popcondata][:tempfile].path, path)
        parser = Fabre::PopularityParser.new({ input: path })
        @packages = parser.parse
        options = {
          key: params[:username],
          installed: parser.parse
        }
        @users.update(options)
      end
      render "settings/popcon"
    end
  end
end
