Fabre::App.controllers :homes do
  get :profiles, map: "/profile" do
    render "profiles/show"
  end
end
