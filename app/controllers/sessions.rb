Fabre::App.controllers :sessions do
  get :signin, map: "/signin" do
    session[:state] = SecureRandom.uuid
    render "sessions/signin", layout: :sessions
  end

  get :signout, map: "/signout" do
    session[:username] = nil
    session[:state] = nil
    session[:timezone] = nil
    render "sessions/signout", layout: :sessions
  end
end
