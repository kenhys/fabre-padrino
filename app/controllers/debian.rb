Fabre::App.controllers :debian do
  get :dashboard, map: "/debian/dashboard" do
    check_x_fabre_account
    open_database do
      @users = Fabre::Client::Users.new
      before = Time.now
      @bookmarks = @users.bookmark(key: session[:sid])
      @affected = @users.affected_rc_bugs(session[:username])
      @installed = @users.installed_packages(session[:username])
      @since = n_days_ago(30)
      @followups = @users.followup_comment_bugs(session[:username], @since)
      @elapsed_time = Time.now - before
      render "dashboards/index"
    end
  end

  get :show_bug, map: "/debian/bugs/show/:id" do
    check_x_fabre_account
    open_database do
      before = Time.now
      @bugs = Fabre::Client::Bugs.new
      @bug = @bugs.find_by_key(params[:id])
      @elapsed_time = Time.now - before
      unless @bugs.success?
        render "bugs/none"
      else
        @comments = @bug.comment
        @blocked_bugs = @bugs.blocked_by_bugs(params)
        @comments = @bugs.ascending_comments
        @sessions = Fabre::Client::Sessions.new
        params = {
          key: session[:username],
          uuid: session[:state]
        }
        if @sessions.authenticated?(params)
          @user = Fabre::Client::Users.new.find_by_username(session[:username])
        end
        render "bugs/basic"
      end
    end
  end

  get :affected, map: "/debian/bugs/affected" do
    check_x_fabre_account
    open_database do
      @user = Fabre::Client::Users.new
      before = Time.now
      @records = @user.affected_rc_bugs(session[:username])
      @elapsed_time = Time.now - before
      render "bugs/affected"
    end
  end

  get :rc, map: "/debian/bugs/rc" do
    options = {
      severity: "all",
      base_url: "/bugs/rc",
      template: "bugs/rc"
    }
    rc_with_severity(options)
  end

  get :rc_severity, map: "/debian/bugs/rc/:severity" do
    options = {
      severity: params[:severity],
      base_url: "/bugs/rc",
      template: "bugs/rc"
    }
    rc_with_severity(options)
  end

  get :ftbfs, map: "/debian/bugs/ftbfs" do
    ftbfs_with_severity({
                          severity: "all",
                          base_url: "/bugs/ftbfs",
                          template: "bugs/ftbfs"})
  end

  get :ftbfs_with_severity, map: "/debian/bugs/ftbfs/:severity" do
    ftbfs_with_severity({
                          severity: params[:severity],
                          base_url: "/debian/bugs/ftbfs/#{params[:severity]}",
                          template: "bugs/ftbfs"})
  end

  get :created, map: "/debian/bugs/recent/created" do
    options = {
      severity: "all",
      base_url: "/bugs/recent/created",
      template: "bugs/recent_created",
      column: "created_at"
    }
    created_with_severity(options)
  end

  get :created_severity, map: "/debian/bugs/recent/created/:severity" do
    options = {
      severity: params[:severity],
      base_url: "/bugs/recent/created/#{params[:severity]}",
      template: "bugs/recent_created",
      column: "created_at"
    }
    created_with_severity(options)
  end

  get :updated, map: "/debian/bugs/recent/updated" do
    options = {
      severity: "all",
      base_url: "/bugs/recent/updated",
      template: "bugs/recent_updated",
      column: "updated_at"
    }
    updated_with_severity(options)
  end

  get :updated_severity, map: "/debian/bugs/recent/updated/:severity" do
    options = {
      severity: params[:severity],
      base_url: "/bugs/recent/updated/#{params[:severity]}",
      template: "bugs/recent_updated",
      column: "updated_at"
    }
    updated_with_severity(options)
  end

  get :security, map: "/debian/bugs/security" do
    security_with_severity({
                             severity: "all",
                             base_url: "/bugs/security/#{params[:severity]}",
                             template: "bugs/security"})
  end

  get :security_with_severity, map: "/debian/bugs/security/:severity" do
    security_with_severity({
                             severity: params[:severity],
                             base_url: "/bugs/security/#{params[:severity]}",
                             template: "bugs/security"})
  end

  get :within30days, map: "/debian/bugs/recent/30days" do
    check_x_fabre_account
    open_database do
      bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      @severity = "all"
      @records = bugs.recent_updated_within(7, 30).paginate([["updated_at", :descending]], options)
      @base_url = "/bugs/recent/30days"
      @elapsed_time = Time.now - before
      render "bugs/recent_30days"
    end
  end

  get :within30days_severity, map: "/debian/bugs/recent/30days/:severity" do
    check_x_fabre_account
    open_database do
      bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      @severity = params[:severity]
      @records = bugs.recent_updated_within(7, 30, params[:severity]).paginate([["updated_at", :descending]], options)
      @base_url = "/bugs/recent/30days/#{params[:severity]}"
      @elapsed_time = Time.now - before
      render "bugs/recent_30days"
    end
  end

  get :within90days, map: "/debian/bugs/recent/90days" do
    check_x_fabre_account
    open_database do
      bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      @severity = "all"
      @records = bugs.recent_updated_within(30, 90).paginate([["updated_at", :descending]], options)
      @base_url = "/bugs/recent/90days"
      @elapsed_time = Time.now - before
      render "bugs/recent_90days"
    end
  end

  get :within90days_severity, map: "/debian/bugs/recent/90days/:severity" do
    check_x_fabre_account
    open_database do
      bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      @severity = params[:severity]
      @records = bugs.recent_updated_within(30, 90, params[:severity]).paginate([["updated_at", :descending]], options)
      @base_url = "/bugs/recent/90days/#{params[:severity]}"
      @elapsed_time = Time.now - before
      render "bugs/recent_90days"
    end
  end

  get :bookmark, map: "/debian/bugs/bookmark" do
    check_x_fabre_account
    open_database do
      @users = Fabre::Client::Users.new
      before = Time.now
      @records = @users.bookmarked_bugs(session["username"])
      @elapsed_time = Time.now - before
      render "bugs/bookmark"
    end
  end

  get :newcomer, map: "/debian/bugs/newcomer" do
    options = {
      severity: "all",
      base_url: "/bugs/newcomer",
      template: "bugs/newcomer",
      type: "all"
    }
    newcomer_with_severity(options)
  end

  get :newcomer_severity, map: "/debian/bugs/newcomer/:severity" do
    options = {
      severity: params[:severity],
      base_url: "/bugs/newcomer/#{params[:severity]}",
      template: "bugs/newcomer"
    }
    newcomer_with_severity(options)
  end

  get :maintainer, map: "/debian/bugs/maintainer" do
    check_x_fabre_account
    open_database do
      @reporters = Fabre::Client::Reporters.new
      before = Time.now
      @records = @reporters.lists_debian_org
      @elapsed_time = Time.now - before
      render "bugs/maintainer"
    end
  end

  get :usertag, map: "/debian/bugs/usertag" do
    check_x_fabre_account
    open_database do
      @packages = Fabre::Client::Packages.new
      before = Time.now
      @records = @packages.maintainer_email_tags
      @elapsed_time = Time.now - before
      render "bugs/usertag"
    end
  end

  get :archtag, map: "/debian/bugs/archtag" do
    options = {
      port: "all",
      base_url: "/bugs/archtag/#{params[:port]}",
      template: "bugs/archtag"
    }
    archtag_with_ports(options)
  end

  get :archtag_with_ports, map: "/debian/bugs/archtag/:port" do
    options = {
      port: params[:port],
      base_url: "/bugs/archtag/#{params[:port]}",
      template: "bugs/archtag"
    }
    archtag_with_ports(options)
  end

end
