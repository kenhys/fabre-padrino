Fabre::App.controllers :bugs do
  # get :index, :map => '/foo/bar' do
  #   session[:foo] = 'bar'
  #   render 'index'
  # end

  # get :sample, :map => '/sample/url', :provides => [:any, :js] do
  #   case content_type
  #     when :js then ...
  #     else ...
  # end

  # get :foo, :with => :id do
  #   "Maps to url '/foo/#{params[:id]}'"
  # end

  get '/' do
    check_x_fabre_account
    @elapsed_time = 0
    render 'bugs/search'
  end

  get :bugs do
    check_x_fabre_account
    @elapsed_time = 0
    render 'bugs/search'
  end

  get :search do
    check_x_fabre_account
    @elapsed_time = 0
    render 'bugs/search'
  end

  post :search do
    check_x_fabre_account
    open_database do
      @bugs = Fabre::Client::Bugs.new
      before = Time.now
      @records = @bugs.fts(params[:keywords])
      @elapsed_time = Time.now - before
      @keywords = params[:keywords]
      render 'bugs/search'
    end
  end

  get :show, with: :id do
    check_x_fabre_account
    open_database do
      before = Time.now
      @bugs = Fabre::Client::Bugs.new
      @bug = @bugs.find_by_key(params[:id])
      @elapsed_time = Time.now - before
      unless @bugs.success?
        render "bugs/none"
      else
        @comments = @bug.comment
        @blocked_bugs = @bugs.blocked_bugs
        @comments = @bugs.ascending_comments
        @sessions = Fabre::Client::Sessions.new
        params = {
          key: session[:username],
          uuid: session[:state]
        }
        if @sessions.authenticated?(params)
          @user = Fabre::Client::Users.new.find_by_username(session[:username])
        end
        render "bugs/basic"
      end
    end
  end

  get :rc do
    check_x_fabre_account
    open_database do
      @bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      params[:type] = "all"
      @elapsed_time = Time.now - before
      @severity = "all"
      @records = @bugs.rc_bugs_by(params).paginate([["created_at", :descending]], options)
      @base_url = "/bugs/rc"
      @elapsed_time = Time.now - before
      render 'bugs/rc'
    end
  end

  get :rc_with_severity, map: "/bugs/rc/:severity" do
    check_x_fabre_account
    open_database do
      @bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      case params[:severity]
      when "fixme"
        params[:type] = "fixme"
      else
      end
      @severity = params[:severity]
      @records = @bugs.rc_bugs_by(params).paginate([["created_at", :descending]], options)
      @base_url = "/bugs/rc/#{params[:severity]}"
      @elapsed_time = Time.now - before
      render 'bugs/rc'
    end
  end

  get :rc_fixme, map: "/bugs/rc/fixme" do
    check_x_fabre_account
    open_database do
      @bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      params[:type] = "fixme"
      @severity = "fixme"
      @records = @bugs.rc_bugs_by(params).paginate([["created_at", :descending]], options)
      @elapsed_time = Time.now - before
      render 'bugs/rc'
    end
  end

  get :rc_affected, map: "/bugs/rc/affected" do
    check_x_fabre_account
    open_database do
      @bugs = Fabre::Client::Bugs.new
      @users = Fabre::Client::Users.new
      before = Time.now
      installed = @users.installed_packages(session[:username])
      pkgs = installed.collect do |record|
        record._key
      end
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      params[:type] = "affected"
      params[:packages] = pkgs
      @severity = "affected"
      @records = @bugs.rc_bugs_by(params).paginate([["created_at", :descending]], options)
      @base_url = "/bugs/rc/affected"
      @elapsed_time = Time.now - before
      render 'bugs/rc'
    end
  end

  get :affected do
    check_x_fabre_account
    open_database do
      @user = Fabre::Client::Users.new
      before = Time.now
      @records = @user.affected_bugs(session[:username])
      @elapsed_time = Time.now - before
      render "bugs/affected"
    end
  end

  get :created, map: '/bugs/recent/created' do
    check_x_fabre_account
    open_database do
      @bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      params[:column] = "created_at"
      @severity = "all"
      @records = @bugs.recent_bugs_by_column(params).paginate([["created_at", :descending]], options)
      @base_url = "/bugs/recent/created"
      @elapsed_time = Time.now - before
      render 'bugs/recent_created'
    end
  end

  get :updated, map: '/bugs/recent/updated' do
    check_x_fabre_account
    open_database do
      @bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      params[:column] = "created_at"
      @severity = "all"
      @records = @bugs.recent_bugs_by_column(params).paginate([["updated_at", :descending]], options)
      @base_url = "/bugs/recent/updated"
      @elapsed_time = Time.now - before
      render 'bugs/recent_updated'
    end
  end

  get :updated_severity, map: "/bugs/recent/updated/:severity" do
    check_x_fabre_account
    open_database do
      @bugs = Fabre::Client::Bugs.new
      before = Time.now
      @severity = params[:severity]
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      params[:column] = "updated_at"
      @base_url = "/bugs/recent/updated/#{params[:severity]}"
      @records = @bugs.recent_bugs_by_column(params).paginate([["updated_at", :descending]], options)
      @elapsed_time = Time.now - before
      render "bugs/recent_updated"
    end
  end

  get :created_severity, map: "/bugs/recent/created/:severity" do
    check_x_fabre_account
    open_database do
      @bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      @severity = params[:severity]
      params[:column] = "created_at"
      @base_url = "/bugs/recent/created/#{params[:severity]}"
      @records = @bugs.recent_bugs_by_column(params).paginate([["created_at", :descending]], options)
      @elapsed_time = Time.now - before
      render "bugs/recent_created"
    end
  end

  get :noreply do
    check_x_fabre_account
    open_database do
      @bugs = Fabre::Bugs.new
      before = Time.now
      @records = @bugs.missing_comments
      @elapsed_time = Time.now - before
      render "bugs/noreply"
    end
  end

  get :within30days, map: '/bugs/recent/30days' do
    check_x_fabre_account
    open_database do
      bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      @severity = "all"
      @records = bugs.recent_updated_within(7, 30).paginate([["updated_at", :descending]], options)
      @base_url = "/bugs/recent/30days"
      @elapsed_time = Time.now - before
      render "bugs/recent_30days"
    end
  end

  get :within30days_severity, map: '/bugs/recent/30days/:severity' do
    check_x_fabre_account
    open_database do
      bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      @severity = params[:severity]
      @records = bugs.recent_updated_within(7, 30, params[:severity]).paginate([["updated_at", :descending]], options)
      @base_url = "/bugs/recent/30days/#{params[:severity]}"
      @elapsed_time = Time.now - before
      render "bugs/recent_30days"
    end
  end

  get :within90days, map: '/bugs/recent/90days' do
    check_x_fabre_account
    open_database do
      bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      @severity = "all"
      @records = bugs.recent_updated_within(30, 90).paginate([["updated_at", :descending]], options)
      @base_url = "/bugs/recent/90days"
      @elapsed_time = Time.now - before
      render "bugs/recent_90days"
    end
  end

  get :within90days_severity, map: '/bugs/recent/90days/:severity' do
    check_x_fabre_account
    open_database do
      bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      @severity = params[:severity]
      @records = bugs.recent_updated_within(30, 90, params[:severity]).paginate([["updated_at", :descending]], options)
      @base_url = "/bugs/recent/90days/#{params[:severity]}"
      @elapsed_time = Time.now - before
      render "bugs/recent_90days"
    end
  end

  get :bookmark do
    check_x_fabre_account
    open_database do
      @users = Fabre::Client::Users.new
      before = Time.now
      @records = @users.bookmarked_bugs(session["username"])
      @elapsed_time = Time.now - before
      render 'bugs/bookmark'
    end
  end

  get :newcomer do
    check_x_fabre_account
    open_database do
      @bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      @severity = "all"
      params[:type] = "all"
      @records = @bugs.newcomer_bugs_by(params).paginate([["updated_at", :descending]], options)
      @elapsed_time = Time.now - before
      @base_url = "/bugs/newcomer"
      render "bugs/newcomer"
    end
  end

  get :newcomer_severity, map: "/bugs/newcomer/:severity" do
    check_x_fabre_account
    open_database do
      @bugs = Fabre::Client::Bugs.new
      before = Time.now
      options = {
        size: 20,
        page: params[:page] ? params[:page].to_i : 1
      }
      @severity = params[:severity]
      if params[:severity]
        if params[:severity] == "fixme"
          params[:type] = "fixme"
        end
      end
      @records = @bugs.newcomer_bugs_by(params).paginate([["updated_at", :descending]], options)
      @elapsed_time = Time.now - before
      @base_url = "/bugs/newcomer/#{params[:severity]}"
      render "bugs/newcomer"
    end
  end

  get :maintainer do
    check_x_fabre_account
    open_database do
      @reporters = Fabre::Client::Reporters.new
      before = Time.now
      @records = @reporters.lists_debian_org
      @elapsed_time = Time.now - before
      render "bugs/maintainer"
    end
  end

  get :usertag do
    check_x_fabre_account
    open_database do
      @packages = Fabre::Client::Packages.new
      before = Time.now
      @records = @packages.maintainer_email_tags
      @elapsed_time = Time.now - before
      render 'bugs/usertag'
    end
  end

  get :archtag, map: "/bugs/archtag" do
    options = {
      port: "all",
      base_url: "/bugs/archtag/#{params[:port]}",
      template: "bugs/archtag"
    }
    archtag_with_ports(options)
  end

  get :archtag_with_ports, map: "/bugs/archtag/:port" do
    options = {
      port: params[:port],
      base_url: "/bugs/archtag/#{params[:port]}",
      template: "bugs/archtag"
    }
    archtag_with_ports(options)
  end

  get :ftbfs, map: "/bugs/ftbfs" do
    ftbfs_with_severity({
                          severity: "all",
                          base_url: "/bugs/ftbfs/#{params[:severity]}",
                          template: "bugs/ftbfs"})
  end

  get :ftbfs_with_severity, map: "/bugs/ftbfs/:severity" do
    ftbfs_with_severity({
                          severity: params[:severity],
                          base_url: "/bugs/ftbfs/#{params[:severity]}",
                          template: "bugs/ftbfs"})
  end

  get :security, map: "/bugs/security" do
    security_with_severity({
                             severity: "all",
                             base_url: "/bugs/ftbfs/#{params[:severity]}",
                             template: "bugs/security"})
  end

  get :security_with_severity, map: "/bugs/security/:severity" do
    security_with_severity({
                             severity: params[:severity],
                             base_url: "/bugs/ftbfs/#{params[:severity]}",
                             template: "bugs/security"})
  end

  get :blockedby, map: "/bugs/blockedby" do
    blockedby_with_severity({
                              severity: "all",
                              base_url: "/bugs/blockedby",
                              template: "bugs/blockedby"})
  end

  get :blockedby_with_severity, map: "/bugs/blockedby/:severity" do
    blockedby_with_severity({
                              severity: params[:severity],
                              base_url: "/bugs/blockedby/#{params[:severity]}",
                              template: "bugs/blockedby"})
  end
end
