Fabre::App.controllers :faqs do
  get :help, map: "/help" do
    check_x_fabre_account
    @enable_search = false
    render "abouts/help"
  end

  get :search, map: "/about/search" do
    session[:state] = SecureRandom.uuid
    check_x_fabre_account
    render "abouts/search"
  end

  get :privacy, map: "/about/privacy" do
    check_x_fabre_account
    render "abouts/privacy"
  end

  get :faq, map: "/about/faq" do
    check_x_fabre_account
    render "abouts/faq"
  end
end
