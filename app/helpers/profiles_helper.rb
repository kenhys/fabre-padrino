require_relative "homes_helper"

module Fabre
  class App
    module ProfilesHelper
      include HomesHelper
    end

    helpers ProfilesHelper
  end
end
