# Helper methods defined here can be accessed in any controller or view in the application

module Fabre
  class App
    module HomesHelper
      def salsa_avatar
        session[:avatar] || '<i class="ri-user-line"></i>'
      end

      def salsa_username
        session[:username]
      end

      def salsa_name
        session[:name]
      end

      def salsa_weburl
        session[:weburl]
      end

      def salsa_email
        session[:email]
      end
    end

    helpers HomesHelper
  end
end
