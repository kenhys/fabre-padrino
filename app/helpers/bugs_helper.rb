# Helper methods defined here can be accessed in any controller or view in the application

module Fabre
  class App
    module BugsHelper
      # def simple_helper_method
      # ...
      # end
      def page_title
        if @bug
          "Fabre - Bug##{@bug._key} #{@bug.subject}"
        else
          "Fabre - Finding and fixing bug make 'unstable life' comfortable"
        end
      end

      def link_to_bug(key)
        link_to("\##{key}", "/bugs/show/#{key}")
      end

      def link_comment_to_bug(bug)
        link_to("#{bug.comment.size}", "/bugs/show/#{bug._key}")
      end

      def comment_id_tag(comment)
        # id = comment._key.split("#", 2).last
        # tag(:div, tag(:pre, concat_conent(comment.markup)), :id => id, :class => "card-body")
        "<div class='m-3'><pre>#{comment.markup}</pre></div>"
      end

      def to_list(comment)
        comment.to.collect do |record|
          if record.email =~ /^.+@bugs\.debian\.org/
            ObfuscateEmail.new(record.email).to_s
          else
            record.fullname
          end
        end.join(",&nbsp;")
      end

      def bug_tags(bug)
        html = ""
        if bug.tag
          bug.tag.each do |tag|
            html << "<button class='btn btn-sm btn-success disabled'>#{tag._key}</button>&nbsp;"
          end
        end
        html
      end

      def pts_link(bug)
        package = bug.package._key.sub(/src:/, "")
        "https://tracker.debian.org/pkg/#{package}"
      end

      def pts_label(bug)
        bug.package._key.sub(/src:/, "")
      end

      def patch?(bug)
        return false unless bug.tag

        bug.tag.any? { |tag| tag._key == "patch" }
      end

      def pending?(bug)
        return false unless bug.tag

        bug.tag.any? { |tag| tag._key == "pending" } or bug.status == "pending"
      end

      def done?(bug)
        bug[:done_by] or bug.status == "done"
      end

      def ftbfs?(bug)
        bug.tag.any? { |tag| tag._key == "ftbfs" }
      end

      def wontfix?(bug)
        bug.tag.any? { |tag| tag._key == "wontfix" }
      end

      def security?(bug)
        bug.tag.any? { |tag| tag._key == "security" }
      end

      def fixed_upstream?(bug)
        return false unless bug.tag

        bug.tag.any? { |tag| tag._key == "fixed-upstream" }
      end

      def moreinfo?(bug)
        return false unless bug.tag

        bug.tag.any? { |tag| tag._key == "moreinfo" }
      end

      def unreproducible?(bug)
        return false unless bug.tag

        bug.tag.any? { |tag| tag._key == "unreproducible" }
      end

      def time_to_timezone(time)
        session[:timezone] ? time.localtime(session[:timezone]) : time
      end

      def time_to_timezone(time)
        time = session[:timezone] ? time.localtime(session[:timezone]) : time
        duration = (Time.now - time) / (60 * 60 * 24)
        if duration >= 1
          n = (Time.now - time).to_i / (60 * 60 * 24)
          if n >= 365
            n / 365 > 1 ? "#{n/365} years ago" : "#{n/365} year ago"
          elsif n >= 30
            n / 30 > 1 ? "#{n/30} months ago" : "#{n/30} month ago"
          else
            n > 1 ? "#{n} days ago" : "#{n} day ago"
          end
        else
          n = (Time.now - time) / (60 * 60)
          n > 1 ? "#{n.round} hours ago" : "#{n.round} hour ago"
        end
      end

      def count_comment_since(comments, since)
        comments.count do |record|
          record.date > since
        end
      end

      def n_days_ago(n)
        Time.now - 60 * 60 * 24 * n
      end

      def check_x_fabre_account
        @enable_search = false
        @enable_login = false
        case request.env["HTTP_X_FABRE_ACCOUNT"]
        when "user"
          @enable_search = true
          @enable_login = true
        end
        if ENV["RACK_ENV"] == "development"
          @enable_search = true
          @enable_login = true
          open_database do
            @user = Fabre::Client::Users.new.find_by_username(session[:username])
          end
        end
      end

      def created_with_severity(params)
        check_x_fabre_account
        open_database do
          @bugs = Fabre::Client::Bugs.new
          before = Time.now
          @severity = params[:severity]
          options = {
            size: 20,
            page: params[:page] ? params[:page].to_i : 1
          }
          @base_url = params[:base_url]
          @records = @bugs.recent_bugs_by_column(params).paginate([[params[:column], :descending]], options)
          @elapsed_time = Time.now - before
          render params[:template]
        end
      end

      def updated_with_severity(params)
        check_x_fabre_account
        open_database do
          @bugs = Fabre::Client::Bugs.new
          before = Time.now
          @severity = params[:severity]
          options = {
            size: 20,
            page: params[:page] ? params[:page].to_i : 1
          }
          @base_url = params[:base_url]
          @records = @bugs.recent_bugs_by_column(params).paginate([[params[:column], :descending]], options)
          @elapsed_time = Time.now - before
          render params[:template]
        end
      end

      def default_pagination_options(params)
        {
          size: 20,
          page: params[:page] ? params[:page].to_i : 1
        }
      end

      def rc_with_severity(params)
        check_x_fabre_account
        open_database do
          case params[:severity]
          when "affected"
            @bugs = Fabre::Client::Bugs.new
            @users = Fabre::Client::Users.new
            before = Time.now
            @severity = params[:severity]
            affected = @users.affected_rc_bugs(session[:username])
            @records = affected.paginate([["created_at", :descending]],
                                         default_pagination_options(params))
            @base_url = params[:base_url]
            @elapsed_time = Time.now - before
            render params[:template]
          else
            @bugs = Fabre::Client::Bugs.new
            before = Time.now
            case params[:severity]
            when "fixme"
              params[:type] = "fixme"
            else
            end
            @severity = params[:severity]
            @records = @bugs.rc_bugs_by(params).paginate([["created_at", :descending]],
                                                         default_pagination_options(params))
            @base_url = params[:base_url]
            @elapsed_time = Time.now - before
            render params[:template]
          end
        end
      end

      def ftbfs_with_severity(params)
        check_x_fabre_account
        open_database do
          bugs = Fabre::Client::Bugs.new
          before = Time.now
          options = {
            size: 20,
            page: params[:page] ? params[:page].to_i : 1
          }
          @severity = params[:severity]
          @records = bugs.ftbfs_bugs_by(params).paginate([["updated_at", :descending]], options)
          @base_url = params[:base_url]
          render params[:template]
        end
      end

      def security_with_severity(params)
        check_x_fabre_account
        open_database do
          bugs = Fabre::Client::Bugs.new
          before = Time.now
          options = {
            size: 20,
            page: params[:page] ? params[:page].to_i : 1
          }
          @severity = params[:severity]
          @records = bugs.security_bugs_by(params).paginate([["updated_at", :descending]], options)
          @base_url = params[:base_url]
          render "bugs/security"
        end
      end

      def archtag_with_ports(params)
        check_x_fabre_account
        open_database do
          bugs = Fabre::Client::Bugs.new
          before = Time.now
          options = {
            size: 20,
            page: params[:page] ? params[:page].to_i : 1
          }
          @port = params[:port]
          if params[:port] == "fixme"
            params[:type] = "fixme"
          end
          @records = bugs.arch_tags_by(params).paginate([["updated_at", :descending]], options)
          @base_url = "/bugs/archtag/#{params[:port]}"
          @elapsed_time = Time.now - before
          render params[:template]
        end
      end

      def newcomer_with_severity(params)
        check_x_fabre_account
        open_database do
          @bugs = Fabre::Client::Bugs.new
          before = Time.now
          options = {
            size: 20,
            page: params[:page] ? params[:page].to_i : 1
          }
          @severity = params[:severity]
          case params[:severity]
          when "fixme"
            params[:type] = "fixme"
          end
          @records = @bugs.newcomer_bugs_by(params).paginate([["updated_at", :descending]], options)
          @elapsed_time = Time.now - before
          @base_url = params[:base_url]
          render params[:template]
        end
      end

      def blockedby_with_severity(params)
        check_x_fabre_account
        open_database do
          @bugs = Fabre::Client::Bugs.new
          before = Time.now
          options = default_pagination_options(params)
          @severity = params[:severity]
          case params[:severity]
          when "fixme"
            params[:type] = "fixme"
          end
          @records = @bugs.blocked_by_bugs(params).paginate([["updated_at", :descending]], options)
          @elapsed_time = Time.now - before
          @base_url = params[:base_url]
          render params[:template]
        end
      end
    end

    helpers BugsHelper
  end
end
