module Fabre
  class App
    module SettingsHelper
      include HomesHelper
    end

    helpers ProfilesHelper
  end
end
