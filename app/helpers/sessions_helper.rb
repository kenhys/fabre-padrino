# Helper methods defined here can be accessed in any controller or view in the application

module Fabre
  class App
    module SessionsHelper
      def salsa_client_id
        ENV["SALSA_APPLICATION_ID"]
      end

      def salsa_request_uri
        "https://salsa.debian.org/oauth/authorize/"
      end

      def salsa_redirect_uri
        ENV["SALSA_REDIRECT_URI"]
      end

      def salsa_response_type
        "code"
      end

      def salsa_state
        session[:state]
      end

      def salsa_scope
        "read_user read_api"
      end
    end

    helpers SessionsHelper
  end
end
