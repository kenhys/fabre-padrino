require "test/unit"
require "test/unit/rr"
require "stringio"
require "tmpdir"
require "fileutils"
require "groonga/command/parser"

def fixture_content(name)
  File.open(File.join(File.dirname(__FILE__), "fixtures", "#{name}")) do |file|
    file.read
  end
end

def fixture_path(name)
  File.join(File.dirname(__FILE__), "fixtures", "#{name}")
end

def fixture_log_path(name)
  File.join(File.dirname(__FILE__), "fixtures", "spool", "db-h", name[-2..-1], "#{name}.log")
end

def fixture_summary_path(name)
  File.join(File.dirname(__FILE__), "fixtures", "spool", "db-h", name[-2..-1], "#{name}.summary")
end

def fixture_json_path(name)
  File.join(File.dirname(__FILE__), "fixtures", "#{name}.json")
end

def fixture_spool_json_path(name)
  File.join(File.dirname(__FILE__), "fixtures", "spool", name[-2..-1], "#{name}.json")
end

def fixture_spool_json(name)
  File.open(fixture_spool_json_path(name)) do |file|
    return JSON.parse(File.read(file))
  end
end

def fixture_json(name)
  File.open(File.join(File.dirname(__FILE__), "fixtures", "#{name}.json")) do |file|
    return JSON.load(File.read(file))
  end
end

def fixture_grn_path(name)
  File.join(File.dirname(__FILE__), "fixtures", "#{name}.grn")
end

def fixture_grn(name)
  open(File.join(File.dirname(__FILE__), "fixtures", "#{name}.grn")) do |file|
    return File.read(file)
  end
end

def filter_grn(data)
  data.gsub(/\"updated_at\": \"\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} UTC\"/, %Q("updated_at": "2020-01-01 00:00:00 UTC"))
end
