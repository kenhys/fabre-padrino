require_relative "../helper"
require "fabre/builder/json"

class TestJSONIORebuilder < Test::Unit::TestCase
  def setup
    @output = StringIO.new
  end

  def test_json_io_rebuilder
    options = {
      input: fixture_log_path("951581"),
      output: @output
    }
    rebuilder = Fabre::Builder::JSONIO.new(options)
    rebuilder.run
    assert_equal("ITP: lltsv -- List specified keys of LTSV (Labeled Tab Separated Values)",
                 JSON.parse(@output.string)["bug"]["subject"])
  end
end


class TestJSONFileRebuilder < Test::Unit::TestCase
  include Fabre::Helper::Path

  def test_json_file_rebuilder
    options = {
      input: fixture_log_path("951581")
    }
    Dir.mktmpdir do |dir|
      ENV["FABRE_SPOOL_DIR"] = File.join(File.dirname(__FILE__), "../fixtures/spool")
      ENV["FABRE_DATA_DIR"] = dir
      FileUtils.mkdir_p(File.dirname(json_zst_path("951581")))
      rebuilder = Fabre::Builder::JSONFile.new(options)
      rebuilder.run
      assert_true(File.exist?(json_zst_path("951581")))
    end
  end
end
