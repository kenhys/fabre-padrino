require_relative "../helper"
require "fabre/builder/grn"
require "fabre/parser/grnload"

class TestGrnIO < Test::Unit::TestCase
  def setup
    @parser = Fabre::Parser::GrnLoad.new
  end

  def test_grn_io_rebuilder
    ENV["FABRE_DATA_DIR"] = File.join(File.dirname(__FILE__), "..", "fixtures")
    @input = StringIO.new(JSON.pretty_generate(fixture_spool_json("951581")))
    @output = StringIO.new
    options = {
      input: @input,
      output: @output
    }
    Dir.mktmpdir do |dir|
      ENV["FABRE_DATA_DIR"] = dir
      rebuilder = Fabre::Builder::GrnIO.new(options)
      rebuilder.run
      expected = [
        "load",
        "Reporters"
      ]
      assert_equal(expected, [
                     Groonga::Command::Parser.parse(@output.string).command_name,
                     Groonga::Command::Parser.parse(@output.string).arguments[:table]
                   ])
    end
  end

  def test_escape_comments_subject
    @input = StringIO.new(JSON.generate(fixture_json("comments/escape_subject")))
    @output = StringIO.new
    options = {
      input: @input,
      output: @output
    }
    rebuilder = Fabre::Builder::GrnIO.new(options)
    rebuilder.run
    assert_equal(%q(Bug#999999 escape comments 10 " subject),
                 JSON.parse(@parser.parse(@output.string)[1].arguments[:values]).first["subject"])
  end

  def test_escape_comments_body
    @input = StringIO.new(JSON.generate(fixture_json("comments/escape_body")))
    @output = StringIO.new
    options = {
      input: @input,
      output: @output
    }
    rebuilder = Fabre::Builder::GrnIO.new(options)
    rebuilder.run
    assert_equal(%Q(Escape " comments 10),
                 JSON.parse(@parser.parse(@output.string)[1].arguments[:values]).first["content"])
  end

  def test_escape_comments_body_backslash
    @input = StringIO.new(JSON.generate(fixture_json("comments/escape_body_backslash")))
    @output = StringIO.new
    options = {
      input: @input,
      output: @output
    }
    rebuilder = Fabre::Builder::GrnIO.new(options)
    rebuilder.run
    assert_equal(%(Escape \\" comments 10),
                 JSON.parse(@parser.parse(@output.string)[1].arguments[:values]).first["content"])
  end
end

class TestGrnFile < Test::Unit::TestCase
  include Fabre::Helper::Path
end

class TestGrn < Test::Unit::TestCase
  include Fabre::Helper::Path
end
