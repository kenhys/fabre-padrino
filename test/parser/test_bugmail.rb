require "logger"
require "mail"
require_relative "../helper"
require "fabre/parser/bugmail"
require "fabre/parser/controlmail"

class TestBugMailParser < Test::Unit::TestCase
  def setup
    @parser = Fabre::Parser::BugMail.new
  end

  def test_subject_contains_bug
    data = <<-EOS
Subject: Bug#123456:

EOS
    @parser.parse(data)
    assert_equal(123456, @parser.bug?)
  end

  def test_matches_command_mail
    data = <<-EOS
Subject: Processed: closing 973748

Processing commands for control@bugs.debian.org:

> close 973748 0.19.0-1
EOS
    @parser.parse(data)
    assert_true(@parser.command_mail?)
  end

  def test_control_field
    data = <<-EOS
Subject: Control:

Control: reassign -1 src:python
EOS
    @parser.parse(data)
    assert_true(@parser.command_mail?)
    assert_equal([], @parser.changed_bugs)
  end
end
