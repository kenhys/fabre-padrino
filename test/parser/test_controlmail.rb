require_relative "../helper"
require "fabre/parser/controlmail"

class TestControlMailParser < Test::Unit::TestCase
  def setup
    @parser = Fabre::Parser::ControlMail.new
  end

  def test_close_minus
    body = <<-EOS
Processing control commands:

> close -1 1.0.5-1
Bug #972515 
Marked as fixed in versions foo/1.0.5-1.
Bug #972515 
Marked Bug as done
> block -1 by 969550
Bug #972515 
972515 was not blocked by any bugs.
972515 was not blocking any bugs.
Added blocking bug(s) of 972515: 969550
> affects -1 
Bug #972515 
Added indication that 972515 affects 
EOS
    data = @parser.parse(body)
    assert_equal([972515, 969550, [972515, 969550]],
                 [data[:bug], data[:blocked_by], @parser.ids])
  end

  def test_close_only
    body = <<-EOS
Processing commands for control@bugs.debian.org:

> close 941932
Bug #941932 
Marked Bug as done
> thanks
Stopping processing here.
EOS
    data = @parser.parse(body)
    assert_equal([941932, [941932]],
                 [data[:bug], @parser.ids])
  end

  def test_forcemerge
    body = <<-EOS
Processing commands for control@bugs.debian.org:

> forcemerge 941932 941933
EOS
    data = @parser.parse(body)
    assert_equal([941932, 941933], @parser.ids)
  end
end
