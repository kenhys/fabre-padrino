require_relative "../helper"
require "fabre/parser/grnload"

class TestGrnLoadParser < Test::Unit::TestCase
  def setup
    @parser = Fabre::Parser::GrnLoad.new
  end

  def test_one_load
    data = <<-EOS
load --table Sample
[
{"_key": "hello"}
]
EOS
    first = @parser.parse(data).first
    assert_equal(["load", "Sample", %q([{"_key":"hello"}])],
                 [first.command_name, first.arguments[:table], first.arguments[:values]])
  end

  def test_two_load
    data = <<-EOS
load --table Sample
[
{"_key": "hello"}
]

load --table Sample2
[
{"_key": "hello2"}
]
EOS
    first = @parser.parse(data).first
    last = @parser.parse(data).last
    assert_equal([
                   "load", "Sample", %q([{"_key":"hello"}]),
                   "load", "Sample2", %q([{"_key":"hello2"}])
                 ],
                 [
                   first.command_name, first.arguments[:table], first.arguments[:values],
                   last.command_name, last.arguments[:table], last.arguments[:values]
                ])
  end
end
