require "logger"
require "fabre/parser/popcon"

class TestPopconParser < Test::Unit::TestCase
  def setup
    @logger = Logger.new(STDOUT)
    @logger.level = Logger::UNKNOWN
  end

  def test_no_dev
    options = {
      logger: @logger,
      input: fixture_path("parser/no_dev.txt")
    }
    @parser = Fabre::Parser::Popcon.new(options)
    expected = {
      active: 1,
      ignored: 1,
      inactive: 0,
      packages: ["dummy"]
    }
    assert_equal(expected, @parser.parse)
  end

  def test_no_fonts
    options = {
      logger: @logger,
      input: fixture_path("parser/no_fonts.txt")
    }
    @parser = Fabre::Parser::Popcon.new(options)
    expected = {
      active: 1,
      ignored: 1,
      inactive: 0,
      packages: ["libdummy"]
    }
    assert_equal(expected, @parser.parse)
  end

  def test_no_ttf
    options = {
      logger: @logger,
      input: fixture_path("parser/no_ttf.txt")
    }
    @parser = Fabre::Parser::Popcon.new(options)
    expected = {
      active: 1,
      ignored: 1,
      inactive: 0,
      packages: ["libdummy"]
    }
    assert_equal(expected, @parser.parse)
  end

  def test_two_load
  end
end
