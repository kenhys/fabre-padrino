require_relative "../helper"
require "fabre/converter/markup"

class TestMarkupGenerator < Test::Unit::TestCase
  def setup
    @output = StringIO.new
    @converter = Fabre::Converter::Markup.new
  end

=begin
  def test_markup_comment
      template = Tilt[:haml]
      data = <<-EOS
.originator
  %blockquote.blockquote
    aaa
    bbb


    EOS
      content = template.new(nil, 1, {}) { data }.render(Object.new {})
    
  end
=end

  def test_no_comment
    data = <<~EOS
    Alice wrote:
    a>
    b>
    c>
    EOS
    expected = <<~EOS
    Alice wrote:
    a&gt;
    b&gt;
    c&gt;
    EOS
    @output = StringIO.new("")
    @converter.markup(input: data, output: @output)
    assert_equal(expected, @output.string)
  end

  def test_one_comment
    data = <<~EOS
    Alice wrote:
    > a
    > b
    > c
    
    d
    EOS
    expected = <<~EOS
    Alice wrote:
    <blockquote class=\\\"blockquote\\\">
    &gt; a
    &gt; b
    &gt; c
    </blockquote>
    
    d
    EOS
    @output = StringIO.new("")
    @converter.markup(input: data, output: @output)
    assert_equal(expected, @output.string)
  end

  def test_multiple_comment
    data = <<~EOS
    Alice wrote:
    > a
    > b
    > c
    
    d
    
    Bob wrote:
    > e
    > f
    > g
    EOS
    expected = <<~EOS
    Alice wrote:
    <blockquote class=\\\"blockquote\\\">
    &gt; a
    &gt; b
    &gt; c
    </blockquote>

    d
    
    Bob wrote:
    <blockquote class=\\\"blockquote\\\">
    &gt; e
    &gt; f
    &gt; g
    </blockquote>
    EOS
    @output = StringIO.new("")
    @converter.markup(input: data, output: @output)
    assert_equal(expected, @output.string)
  end

  def test_hyperlink
    data = <<~EOS
    https://wiki.debian.org/Ayatana/IndicatorsTransition

    http://wiki.debian.org/Ayatana/IndicatorsTransition
    EOS
    expected = <<~EOS
    <a href="https://wiki.debian.org/Ayatana/IndicatorsTransition"><i class="ri-external-link-line"></i><mark>https://wiki.debian.org/Ayatana/IndicatorsTransition</mark></a>

    <a href="http://wiki.debian.org/Ayatana/IndicatorsTransition"><i class="ri-external-link-line"></i><mark>http://wiki.debian.org/Ayatana/IndicatorsTransition</mark></a>
    EOS
    @output = StringIO.new("")
    @converter.markup(input: data, output: @output)
    assert_equal(expected, @output.string)
  end
end
