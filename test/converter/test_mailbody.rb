require_relative "../helper"
require "fabre/converter/mailbody"

class TestMailBody < Test::Unit::TestCase

  def setup
    @mailbody = Fabre::Converter::MailBody.new
    @output = StringIO.new
  end

  def filter(data, expected)
    @mailbody.filter({
                       :input => data,
                       :output => @output
                     })
    assert_equal(expected, @output.string)
  end

  def test_no_header
    data = <<"EOS"
This is no header
EOS
    filter(data, data)
  end

  def test_one_header
    data = <<"EOS"
Version: 1.0

This is one header
EOS
    expected = "This is one header"
    filter(data, expected)
  end

  def test_multiple_headers
    data = <<"EOS"
Version: 1.0
Package: wnpp

There is multiple headers.
EOS
    expected = data.split("\n")[3..].join("\n")
    filter(data, expected)
  end

  def test_one_signature
    data = <<"EOS"
Regards,

-- 
Anonymous coward
EOS
    expected = <<"EOS"
Regards,
EOS
    filter(data, expected)
  end

  def test_one_comment
    data = <<"EOS"

Alice :
> a
> b
> c

This is body
EOS
    expected = <<"EOS"
Alice :

This is body
EOS
    filter(data, expected)
  end

  def test_multiple_comments
    data = <<"EOS"

Alice :
> a
> b
> c

This is body

Bob wrote:
> d
> e
> f

Thanks
EOS
    expected = <<"EOS"
Alice :

This is body

Bob wrote:

Thanks
EOS
    filter(data, expected)
  end

  def test_signature
    data = <<"EOS"
Body
-- 
John Smith
EOS
    expected = <<"EOS"
Body
EOS
    filter(data, expected)
  end

  def test_pgp_signature
    data = <<"EOS"
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEXT3w9TizJ8CqeneiAiFmwP88hOMFAl8ZdsUACgkQAiFmwP88
-----END PGP SIGNATURE-----
EOS
    expected = <<"EOS"
EOS
    filter(data, expected)
  end

  def test_debconf_information
    data = <<"EOS"

-- System Information:
   system

-- Configuration Files:
   configuration
-- debconf information:
EOS
    expected = <<"EOS"
EOS
    filter(data, expected)
  end

  def test_nodebconf_information
    data = <<"EOS"

-- System Information:
   system

-- Configuration Files:
   configuration

-- no debconf information:
   something
EOS
    expected = <<"EOS"
EOS
    filter(data, expected)
  end

  def test_stable
    data = <<"EOS"

-- System Information:
Debian Release: 9.8
  APT prefers stable-updates
  APT policy: (500, 'stable-updates'), (500, 'stable')
Architecture: amd64 (x86_64)
EOS
    expected = <<"EOS"
EOS
    filter(data, expected)
    assert_true(@mailbody.stable?)
  end
  def test_testing
    data = <<"EOS"

-- System Information:
Debian Release: 9.8
  APT prefers testing
  APT policy: (500, 'stable-updates'), (500, 'stable')
Architecture: amd64 (x86_64)
EOS
    expected = <<"EOS"
EOS
    filter(data, expected)
    assert_true(@mailbody.testing?)
  end

  def test_unstable
    data = <<"EOS"

-- System Information:
Debian Release: 9.8
  APT prefers unstable
  APT policy: (500, 'stable-updates'), (500, 'stable')
Architecture: amd64 (x86_64)
EOS
    expected = <<"EOS"
EOS
    filter(data, expected)
    assert_true(@mailbody.unstable?)
  end
end
  
