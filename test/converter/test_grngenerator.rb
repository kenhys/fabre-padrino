require_relative "../helper"
require "fabre/converter/generator"

class TestGrnIOGenerator < Test::Unit::TestCase
  def setup
    @output = StringIO.new
    @generator = Fabre::Converter::GrnIOGenerator.new
  end

  def test_grn_io_generator
    options = {
      input: StringIO.new(JSON.pretty_generate(fixture_spool_json("951581"))),
      output: @output
    }
    @generator.run(options)
    expected = [
      "load",
      "Reporters"
    ]
    assert_equal(expected, [
                   Groonga::Command::Parser.parse(@output.string).command_name,
                   Groonga::Command::Parser.parse(@output.string).arguments[:table]
                 ])
  end
end

class TestGrnFileGenerator < Test::Unit::TestCase

  def setup
    @output = StringIO.new
    @generator = Fabre::Converter::GrnFileGenerator.new
  end

  def generate_json(data)
    Dir.mktmpdir do |dir|
      json_path = "#{dir}/comments.json"
      File.open(json_path, "w+") do |file|
        file.puts data
        file.rewind
        yield json_path
      end
    end
  end

  def generate(fixture_name)
    @generator.run({
                     :input => fixture_json_path(fixture_name),
                     :output => @output
                   })
  end

  def test_comment_orders
    generate("comments_order")
    assert_equal(["1", "10"], @generator.comment_orders)
  end

  def test_bug305913_null_date
    generate("bug305913_null_date")
    expected = fixture_grn("bug305913_null_date")
    assert_equal(expected, filter_grn(@output.string))
  end

  def test_bug67606_invalid_utc_offset
    generate("bug67606_invalid_utc_offset")
    expected = fixture_grn("bug67606_invalid_utc_offset")
    assert_equal(expected, filter_grn(@output.string))
  end

  def test_bug28207_empty_date
    generate("bug28207_empty_date")
    expected = fixture_grn("bug28207_empty_date")
    assert_equal(expected, filter_grn(@output.string))
  end

  def test_grn
    expected = fixture_grn("comments_order")
    generate("comments_order")
    assert_equal(expected, filter_grn(@output.string))
  end

  def test_bug958477_utf8_from
    generate("bug958477_utf8_from")
    expected = fixture_grn("bug958477_utf8_from")
    assert_equal(expected, filter_grn(@output.string))
  end

end
