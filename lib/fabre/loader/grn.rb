require "open3"
require "groonga-log"
require "timeout"

require "fabre/helper/path"

module Fabre
  module Loader
    class GrnFile
      include Fabre::Helper::Path

      def initialize(options={})
        @output = options[:output] || $stdout
        Groonga::Context.default_options = { encoding: :utf8 }
        path = File.join(ENV["FABRE_DATA_DIR"],
                         "db/fabre.db")
        @database = Groonga::Database.open(File.expand_path(path))
      end

      def parse_option(argv)
        parser = OptionParser.new
        @options = {
          rebuild: false,
          blocked: nil
        }
        parser.on("-r", "--rebuild", "rebuild .grn from .json") do |v|
          @options[:rebuild] = v
        end
        parser.on("-b", "--blocked-by", "process blocked by bug") do |v|
          @options[:blocked] = v
        end
        parser.banner += "BUG1.log [BUG2.log]"
        @bugs = parser.parse!(argv)
      end

      def find_bug_record(bug)
        @bugs = Groonga["Bugs"]
        records = @bugs.select do |record|
          record._key == bug
        end
        records.first
      end

      def rebuild_grn(bug)
        if @options[:rebuild]
          if File.exist?(grn_zst_path(bug))
            File.unlink(grn_zst_path(bug))
          end
          if File.exist?(json_zst_path(bug))
            puts "rebuild: #{grn_zst_path(bug)}"
            puts `ruby exe/fabre-make-grn.rb #{json_path(bug)}`
          else
            puts "failed to rebuild: <#{grn_path(bug)}>"
          end
        end
      end

      def load_blocked_by_grn(record)
        record.blocked_by.each do |bug|
          @output.puts grn_path(bug._key)
          if @options[:rebuild]
            rebuild_grn(bug._key)
          end
          `zstdcat #{grn_zst_path(bug._key)} | groonga data/db/fabre.db`
        end
      end

      def run(argv=[])
        @bugs = parse_option(argv)
        p @bugs
        begin
          @bugs.each do |bug|
            record = find_bug_record(bug)
            if @options[:rebuild]
              if File.exist?(grn_zst_path(bug))
                File.unlink(grn_zst_path(bug))
              end
              if File.exist?(json_zst_path(bug))
                @output.puts `ruby exe/fabre-make-grn.rb #{json_path(bug)}`
                @output.puts `zstdcat  #{grn_zst_path(bug)} | groonga data/db/fabre.db`
              end
              if @options[:blocked]
                load_blocked_by_grn(record)
              end
            elsif @options[:blocked]
              load_blocked_by_grn(record)
            else
              @output.puts `zstdcat #{grn_zst_path(bug)} | groonga data/db/fabre.db `
            end
          end
        rescue => e
          @output.puts e.message
        end
      end
    end
  end
end
