require "open3"
require "groonga-log"
require "timeout"

require "fabre/helper/path"
require "fabre"

module Fabre
  module Loader
    class GrnIO
      include Fabre::Helper::Path

      def initialize(options={})
        @input = options[:input]
        @bug_number = options[:bug_number]
      end

      def run
        begin
          command = "groonga --log-path #{groonga_log_path(@bug_number)} #{database_path}"
          Timeout.timeout(60) do
            Open3.popen3(command) do |stdin, stdout, _stderr, wait|
              @pid = wait.pid
              stdin << @input.string
              stdin.close
            end
          end

          FileUtils.mkdir_p(File.dirname(groonga_log_path(@bug_number)))
          log = File.open(groonga_log_path(@bug_number)) do |file|
            file.read
          end
          parser = GroongaLog::Parser.new
          raise Fabre::GrnLoadError if parser.parse(log).any? do |entry|
            entry.log_level == :error or
              entry.log_level == :critical
          end
        rescue Timeout::Error
          if @pid.nonzero?
            Process.kill("KILL", @pid)
            `groonga #{database_path} clearlock`
          end
          raise Fabre::GrnLoadError
        rescue
          raise Fabre::GrnLoadError
        end
      end
    end
  end
end
