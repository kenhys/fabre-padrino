module Fabre
  module Loader
    class UserLoader
      def load(users)
        @users = Groonga["Users"]
        users.each do |user|
          if @users.key?(user["username"])
            @users[user["username"]] = {
            }
          else
            params = {
            }
            @users.add(user["username"], params)
          end
        end
      end
    end

    class UserAvatar
      def make_grn(users)
        @users = Groonga["Users"]
        data = []
        File.open(@options[:file], "w+") do |file|
          users.each_with_index do |user, index|
            capital = user["username"][0]
            path = ""
            next unless user["state"] == "active"
            open(user["avatar_url"]) do |f|
              Tempfile.create do |tmp|
                tmp.write(f.read)
                puts "[#{index + 1}]: #{user['avatar_url']}"
                tmp.rewind
                mime = FileMagic.new(FileMagic::MAGIC_MIME).file(tmp.path).split(";")[0].to_s
                if mime.start_with?("image/")
                  extention = mime.split("/").last
                  base_image_dir = File.join(
                    "public",
                    "images",
                    "avatar",
                    capital.downcase)
                  FileUtils.mkdir_p(base_image_dir)
                  path = File.join(base_image_dir, "#{user['username'].downcase}.#{extention}")
                  puts "save: <#{path}>"
                  FileUtils.copy(tmp.path, path)
                else
                  path = nil
                end
              end
            end
            if path
              path.sub!(/^public\//, "")
              data << %Q({"_key": "#{user['username']}", "avatar_url": "#{user['web_url']}", "avatar_path": "#{path}"})
            else
              data << %Q({"_key": "#{user['username']}", "avatar_url": "#{user['web_url']}"})
            end
          end
          file.puts("load --table Users")
          file.puts("[")
          file.puts(data.join(",\n"))
          file.puts("]")
        end
      end
    end

    class Avatar
      def initialize(options={})
        @options = options
      end

      def convert(path)
        unless path.end_with?(".json")
          puts "Skip converting: <#{path}>"
          return
        end
        base_name = File.basename(path, ".json")
        grn_path = File.join(File.dirname(path), "#{base_name}.grn")
        if File.exist?(grn_path) and not (File.mtime(grn_path) < File.mtime(path))
          puts "Skip converting: <#{path}>"
          return
        end
        begin
          File.open(grn_path, "w+") do |output|
            options = {
              debug: false,
              input: path,
              output: output
            }
            converter = Fabre::Converter::GrnGenerator.new(options)
            converter.run
          end
        rescue => e
          p e
          puts "Failed to convert: #{path}"
          File.delete(grn_path)
          exit 1
        end
      end

      def active?(entry)
        entry["state"] == "active"
      end

      def run
        if @options[:start] and @options[:end]
          users = []
          @options[:start].upto(@options[:end]) do |page|
            url = "https://salsa.debian.org/api/v4/users?per_page=100&page=#{page}&active=true&private_token=#{ENV['SALSA_TOKEN']}"
            puts "request: <#{url}>"
            open(url, :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE) do |f|
              JSON.parse(f.read).each do |entry|
                next unless active?(entry)

                users << entry
              end
            end
          end
          if @options[:load]
            loader = UserLoader.new
            loader.load(users)
          elsif @options[:file]
            generator = UserAvatar.new
            generator.make_grn(users)
          end
        end
      end
    end
  end
end
