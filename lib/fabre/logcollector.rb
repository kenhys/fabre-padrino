require_relative "helper"

module Fabre
  class LogCollector
    include Fabre::Helper::Path
    include Fabre::Helper::Database

    def initialize(options={})
      @output = options[:output] || $stdout
    end

    def add_log_entry(type, timestamp, message)
      @logs = Groonga["ServiceLogs"]
      @output.puts "add log: #{type} #{message}"
      @logs.add(type: type,
                timestamp: timestamp,
                message: message)
    end

    def copy_nginx_files
      logs = [
        "/var/log/nginx/fabre.xdump.org.access.log",
        "/var/log/nginx/fabre.xdump.org.error.log",
      ]
      log_dir = File.join(ENV["FABRE_DATA_DIR"],
                          "logs", "nginx")
      FileUtils.cp_r(logs, log_dir)
      FileUtils.chown_R("fabre", "fabre", log_dir)
    end

    def copy_fail2ban_files
      logs = [
        "/var/log/fail2ban.log",
        "/var/log/fail2ban.log.1",
        "/var/log/fail2ban.log.2.gz",
        "/var/log/fail2ban.log.3.gz",
        "/var/log/fail2ban.log.4.gz"
      ]      
      log_dir = File.join(ENV["FABRE_DATA_DIR"],
                          "logs", "fail2ban")
      FileUtils.cp_r(logs, log_dir)
      FileUtils.chown_R("fabre", "fabre", log_dir)
    end

    def run
      open_database do
        add_log_entry("logcollector", Time.now, "start log collector")
        copy_fail2ban_files
        copy_nginx_files
        add_log_entry("logcollector", Time.now, "finish log collector")
      end
    end
  end
end
