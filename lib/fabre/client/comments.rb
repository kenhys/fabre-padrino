# frozen_string_literal: true

module Fabre
  module Accessor
    class Bugs
      def initialize(params={})
        @comments = Groonga["Comments"]
      end

      def create(params={})
        return if params[:key].nil? or params[:key].empty?

        now = Time.now.getutc
        @comments.add(
          params[:key],
          bug_number: params[:bug_number],
          comment_number: params[:comment_number],
          subject: params[:subject],
          from: params[:from],
          to: params[:to],
          date: params[:date],
          content: params[:content],
          markup: params[:markup]
        )
      end

      def update_column(params, symbol)
        @comments[params[symbol]] = params[symbol] if params[symbol]
      end
      
      def update(params={})
        return if params[:key].nil? or params[:key].empty?

        return unless @comments.key?(params[:key])

        update_column(params, :bug_number)
        update_column(params, :comment_number)
        update_column(params, :subject)
        update_column(params, :from)
        update_column(params, :to)
        update_column(params, :date)
        update_column(params, :content)
        update_column(params, :markup)
      end

      def find_by_key(key)
        records = @comments.select do |record|
          record._key == key
        end
        records
      end
    end
  end
end
