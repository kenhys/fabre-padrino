module Fabre
  module Client
    class Users
      attr :status
      attr :elapsed_time

      def initialize(options={})
        @output = options[:output] || $stdout
        @elapsed_time = 0.0
        @status = true
        @users = Groonga["Users"]
      end

      def success?
        @status
      end

      def create(params={})
        key = params[:key] || SecureRandom.uuid
        now = Time.now.getutc
        if @users
          if @users.key?(key)
            @users[key] = {
              avatar_url: params[:avatar_url],
              updated_at: now
            }
          else
            @users.add(key,
                       created_at: now,
                       updated_at: now
                      )
          end
        end
        @users[key]
      end

      def update(params={})
        return if params[:key].nil? or params[:key].empty?

        columns = %i(admin affected api_key avatar_path avatar_url bookmark email installed like sid timezone updated_at)
        values = params.reject do |key,value|
          key == :key or not columns.include?(key)
        end
        @users[params[:key]] = values
        @users[params[:key]]
      end

      def remove(params={})
        return if params[:key].nil? or params[:key].empty?

        @users = Groonga["Users"]
        if @users.key?(params[:key])
          @users.delete do |record|
            record.key =~ params[:key]
          end
        else
          false
        end
      end

      def find_by_key(key)
        bugs = Groonga["Bugs"]
        records = bugs.select do |record|
          record._key == key.to_i
        end
        @bug = records.first
        @bug
      end

      def find_by_username(username)
        @records = @users.select do |record|
          record._key == username
        end
        @records.first
      end

      def find_by_sid(key)
        records = @users.select do |record|
          record.sid =~ key
        end
        records.first
      end

      def find_by_updated_year(year)
        @users = Groonga["Users"]
        records = @users.select do |record|
          record["updated_at"] >= Time.utc(year, 1, 1)
        end
        records
      end


      def bookmarked_bugs(username)
        return [] if username.nil? or username.empty?

        @users = Groonga["Users"]
        user = find_by_username(username)
        return [] unless user
        return [] unless user.bookmark

        keys = user.bookmark.collect(&:_key).compact
        query = "in_values(_key, "
        keys.each_with_index do |key, index|
          query << "#{key}"
          query << ", " if index != keys.size - 1
        end
        query << ")"
        options = [
          {
            key: "severity.level",
            order: :descending
          }
        ]
        @bugs = Groonga["Bugs"]
        @bugs.select(query, :syntax => :script).sort(options).records
      end

      def update_affected(username, pkgs)
        @bugs = Groonga["Bugs"]
        records = @bugs.select do |record|
          conditions = []
          if pkgs.size > 0
            conditions << record.call("in_values", record.package._key, *pkgs)
          end
          conditions << record.call("in_values", record.severity._key, "critical", "grave", "serious")
          conditions << (record[:updated_at] > Time.now - 60 * 60 * 24 * 3)
          conditions
        end
        @user = find_by_username(username)
        @user["affected"] = records.collect(&:_key)
        @output.puts("affected count: <#{records.size}>")
        records.each_with_index do |record, index|
          @output.puts("#{index+1}:#{record["updated_at"]}:#{record.severity._key}: <#{record.subject}>")
        end
      end

      def affected_rc_bugs(username)
        return [] if username.nil? or username.empty?

        @users = Groonga["Users"]
        user = find_by_username(username)
        return [] unless user
        return [] unless user.installed

        @bugs = Groonga["Bugs"]
        @records = []
        if user.affected and not user.affected.empty?
          affected_bugs = user.affected.collect(&:_key)
          @records = @bugs.select do |record|
            conditions = []
            conditions << record.call("in_values", record.severity, "important", "grave", "serious", "critical")
            if affected_bugs.size > 0
              conditions << record.call("in_values", record._key, *affected_bugs)
            else
              installed_pkgs = user.installed.collect(&:_key)
              conditions << record.call("in_values", record.package, *installed_pkgs)
            end
            conditions << (record["updated_at"] > Time.now - 60 * 60 * 24 * 30)
            conditions
          end
        else
          installed_pkgs = user.installed.collect(&:_key)
          @bugs = Groonga["Bugs"]
          @records = @bugs.select do |record|
            conditions = []
            conditions << record.call("in_values", record.severity, "important", "grave", "serious", "critical")
            conditions << record.call("in_values", record.package, *installed_pkgs)
            past_timestamp = (Time.now - 60 * 60 * 24 * 30).iso8601
            # conditions << record.call("between",
            #                           record.updated_at,
            #                           past_timestamp,
            #                           "include",
            #                           Time.now.iso8601,
            #                           "include")
            conditions
          end
        end
        @records
      end

      def installed_packages(username)
        records = []
        @users = Groonga["Users"]
        user = @users.select do |record|
          record._key == username
        end.first
        if user
          records = user[:installed]
        end
        records
      end

      def all
        @users = Groonga["Users"]
        @records = @users.select do |record|
          record._id > 0
        end
        @records
      end

      def bookmark(params)
        return [] if params[:key].nil? or params[:key].empty?
        @users = Groonga["Users"]
        user = @users.select do |record|
          record._key == params[:key]
        end
        @records = user.first.bookmark
      end

      def api_authenticated?(params)
        return false if params[:key].nil? or params[:api_key].empty?
        @users = Groonga["Users"]
        user = @users.select do |record|
          record._key == params[:key] and
            record.api_key == params[:api_key]
        end
        user and user.size == 1
      end

      def followup_comment_bugs(username, since)
        @bugs = Groonga["Bugs"]
        @users = Groonga["Users"]
        @user = find_by_username(username)
        @records = []
        if @user and @bugs
          query = "sub_filter(comment, \"(" + @user.email.collect do |email|
            "from @ \\\"#{email._key}\\\""
          end.join(" || ") + ")"
          query << " && date > #{since.to_i}\")"
          options = [
            {
              :key => "updated_at",
              :order => :descending
            }
          ]
          @records = @bugs.select(query, :syntax => :script).sort(options).records
          @records.each do |bug|
            bug.comment.each do |comment|
              if comment.from
                p "#{comment._key} #{comment.date} #{bug.subject}"
              end
            end
          end
        end
        @records
      end
    end
  end
end
