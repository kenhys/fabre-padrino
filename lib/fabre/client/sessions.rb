# frozen_string_literal: true

module Fabre
  module Client
    class Sessions
      def initialize(params={})
        @sessions = Groonga["Sessions"]
      end

      def create(params={})
        return if params[:key].nil? or params[:key].empty?

        now = Time.now.getutc
        if @sessions.key?(params[:key])
          @sessions[params[:key]] = {
            updated_at: now,
            expired_at: params[:expired_at]
          }
        else
          @sessions.add(params[:key],
                        created_at: now,
                        updated_at: now,
                        expired_at: params[:expired_at]
                       )
        end
      end

      def update(params={})
        return false if params[:key].nil? or params[:key].empty?
        return false if @sessions.key?(params[:key])

        if @sessions.key?(params[:key])
          params.delete(:key)
          params[:updated_at] = Time.now.getutc
          @sessions[key] = params
        end
        true
      end

      def remove(params={})
        return if params[:key].nil? or params[:key].empty?

        if @sessions.key?(params[:key])
          @sessions.delete do |record|
            record.key =~ params[:key]
          end
        else
          false
        end
      end

      def authenticated?(params)
        @sessions = Groonga["Sessions"]
        @users = Groonga["Users"]
        key = params[:key]
        return false if key.nil?

        @sessions.key?(key) and
          @sessions[key]["expired_at"] > Time.now and
          @users[key]["state"] == params[:state]
      end

      def find_by_key(key)
        records = @sessions.select do |record|
          record._key == key
        end
        records.first
      end

      def find_by_sid(salsa_id)
        records = @sessions.select do |record|
          record.user.sid == salsa_id
        end
        records.first
      end

      def expired?(username)
        unless username
          return true
        end
        records = @sessions.select do |record|
          record.key =~ username &&
            record.expired_at < Time.now
        end
        records.size > 0
      end

      def expire!(username)
        unless username
          return
        end
        @sessions.delete do |record|
          record.key =~ username
        end
      end

      def all
        @records = @sessions.select do |record|
          record._id > 0
        end
        @records
      end
    end
  end
end
