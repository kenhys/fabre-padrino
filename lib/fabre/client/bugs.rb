module Fabre
  module Client
    class Bugs
      attr :bug
      attr :comments
      attr :status
      attr :elapsed_time
      attr :n_records

      BUG_SEVERITY  = %w(critical grave serious important normal minor wishlist)
      ALL_ARCHS = %w(armel armhf arm64 hurd-i386 mips64el mipsel ppc64 ppc64lel riscv64 sparc64 sh sh4 s390x)

      def initialize(options={})
        @elapsed_time = 0.0
        @status = false
        @logger = options[:logger] || Logger.new(STDOUT)
        @bug = nil
      end

      def success?
        @status
      end

      def comment
        @bug.comment
      end

      def create(params={})
        return if params[:key].nil? or params[:key].empty?

        now = Time.now.getutc
        @bugs ||= Groonga["Bugs"]
        @bugs.add(
          params[:key],
          subject: params[:subject],
          package: params[:package],
          originator: params[:originator],
          severity: params[:severity],
          category: params[:category],
          archived: params[:archived],
          created_at: now,
          updated_at: now
        )
      end

      def update(params={})
        return if params[:key].nil? or params[:key].empty?

        @bugs ||= Groonga["Bugs"]
        return unless @bugs.key?(params[:key])

        values = {}
        values[:done_by] = params[:done_by] if params[:done_by]
        values[:comment] = params[:comment] if params[:comment]
        values[:updated_at] = params[:updated_at]
        @bugs[params[:key]] = values
      end

      def find(id_or_key)
        if id_or_key.is_a?(Integer) or /\A[0-9]+\z/.match?(id_or_key)
          @bug = find_by_id(id_or_key.to_i)
        else
          @bug = find_by_key(id_or_key)
        end
        @bug
      end

      def find_by_key(key)
        @bugs = Groonga["Bugs"]
        records = @bugs.select do |record|
          record._key == key.to_i
        end
        if records.size > 0
          @status = true
          @bug = records.first
        end
        @bug
      end

      def find_by_id(id)
        @bugs ||= Groonga["Bugs"]
        records = @bugs.select do |record|
          record._id == id
        end
        @bug = records.first
        @bug
      end

      def ascending_comments
        @bugs ||= Groonga["Bugs"]
        @bug.comment.sort do |a, b|
          a._key <=> b._key
        end
      end

      def blocked_by_bugs(params)
        @bugs ||= Groonga["Bugs"]

        all_severity = "(" + BUG_SEVERITY.collect do |key|
          "severity._key == '#{key}'"
        end.join(" || ") + ")"

        query = [
          "package.popcon > 0",
          "vector_size(blocked_by) > 0",
          "done_by._key == ''",
          "archived == false",
        ]
        if params[:severity] == "all"
          query.unshift(all_severity)
        elsif BUG_SEVERITY.include?(params[:severity])
          query.unshift("severity._key == '#{params[:severity]}'")
        end

        @bugs.select(query.join(" && "), :syntax => :script)
      end

      def newcomer_bugs_by(params)
        @bugs ||= Groonga["Bugs"]
        query = [
          "tag @ 'newcomer'",
          "done_by._key == ''",
          "archived == false",
          "package.popcon > 0"
        ]
        if params[:type] and params[:type] == "fixme"
          query << "vector_size(comment) == 1"
        end
        newcomer_all_severity = "(" + %w(important normal minor wishlist).collect do |key|
          "severity._key == '#{key}'"
        end.join(" || ") + ")"

        if params[:severity]
          if %w(important normal minor wishlist).include?(params[:severity])
            query.unshift("severity._key == '#{params[:severity]}'")
          else
            query.unshift(newcomer_all_severity)
          end
        else
          query.unshift(newcomer_all_severity)
        end
        options = [
          {
            :key => "severity.level",
            :order => :descending
          }
        ]
        @bugs.select(query.join(" && "), :syntax => :script)
      end

      def maintainer_bugs(email)
        @bugs ||= Groonga["Bugs"]
        query = [
          "package.maintainer @ '#{email}'",
          "archived == false",
          "package.popcon > 0"
        ].join(" && ")
        options = [
          {
            :key => "package.popcon",
            :order => :descending
          }
        ]
        @bugs.select(query, :syntax => :script)
      end

      def sort_comment_keys
        @bug.comment.sort do |a, b|
          c = a.sub(/(\d+?)#(\d+)/, "\\2").to_i
          d = b.sub(/(\d+?)#(\d+)/, "\\2").to_i
          c <=> d
        end
      end

      def recent_bugs(params={})
        @bugs ||= Groonga["Bugs"]
        date = Time.now - (60 * 60 * 24 * 7)
        options = {
          host: "127.0.0.1",
          protocol: "http"
        }
        @records = []
        query = [
          "created_at > #{date.strftime('%Y-%m-%d')}",
          "done_by._key == ''",
          "archived == false",
          "package.popcon > 0"
        ]
        if params[:severity]
          if params[:severity] == "fixme"
            query << "vector_size(comment) == 1"
            query << "((affects_unstable == true) || (affects_stable == false && affects_testing == false && affects_unstable == false))"
          elsif BUG_SEVERITY.include?(params[:severity])
            query << "severity == '#{params[:severity]}'"
          end
        end
        options = [
          {
            :key => "created_at",
            :order => :descending
          }
        ]
        @bugs.select(query.join(" && "), :syntax => :script)
      end


      def recent_created_bugs(params={})
        @bugs ||= Groonga["Bugs"]
        date = Time.now - (60 * 60 * 24 * 7)
        @records = []
        query = [
          "created_at > #{date.strftime('%Y-%m-%d')}",
          "done_by._key == ''",
          "archived == false",
          "package.popcon > 0",
          "((affects_stable == false && affects_testing == false && affects_unstable == false) || affects_unstable == true)"
        ]
        if params[:severity]
          if params[:severity] == "fixme"
            query << "vector_size(comment) == 1"
          elsif BUG_SEVERITY.include?(params[:severity])
            query << "severity == '#{params[:severity]}'"
          end
        end
        options = [
          {
            :key => "created_at",
            :order => :descending
          }
        ]
        @bugs.select(query.join(" && "), :syntax => :script)
      end

      def recent_updated_bugs(params={})
        @bugs ||= Groonga["Bugs"]
        days = Time.now - (60 * 60 * 24 * 7)
        @records = []
        query = [
          "updated_at > #{days.strftime('%Y-%m-%d')}",
          "done_by._key == ''",
          "archived == false",
          "package.popcon > 0"
        ]
        if params[:severity]
          if params[:severity] == "fixme"
            query << "vector_size(comment) == 1"
            query << "((affects_unstable == true) || (affects_stable == false && affects_testing == false && affects_unstable == false))"
          elsif BUG_SEVERITY.include?(params[:severity])
            query << "severity == '#{params[:severity]}'"
          end
        end
        options = [{
                     :key => "updated_at",
                     :order => :descending
                   }]
        @bugs.select(query.join(" && "), :syntax => :script)
      end

      def all
        @bugs ||= Groonga["Bugs"]
        @records = @bugs.select do |record|
          record._key > 0 and record.archived == 0
        end.sort([{
                    key: "_key",
                    order: :descending
                  }])
        @records
      end

      def newcomer_bugs
        @bugs ||= Groonga["Bugs"]
        date = Time.now - (60 * 60 * 24 * 7)
        @records = []
        query = [
          "tag @ 'newcomer'",
          "done_by._key == ''",
          "archived == false",
          "package.popcon > 0"
        ].join(" && ")
        options = [{
                     :key => "created_at",
                     :order => :descending
                   }]
        @bugs.select(query, :syntax => :script)
      end

      def recent_bugs_by_column(params={})
        @bugs ||= Groonga["Bugs"]
        date = Time.now - (60 * 60 * 24 * 7)
        @records = []
        column = params[:column]
        query = [
          "#{column} > #{date.strftime('%Y-%m-%d')}",
          "done_by._key == ''",
          "archived == false",
          "package.popcon > 0",
          "((affects_stable == false && affects_testing == false && affects_unstable == false) || affects_unstable == true)"
        ]
        if params[:severity]
          if params[:severity] == "fixme"
            query << "vector_size(comment) == 1"
          elsif BUG_SEVERITY.include?(params[:severity])
            query << "severity == '#{params[:severity]}'"
          end
        end
        @bugs.select(query.join(" && "), :syntax => :script)
      end

      def recent_updated_within(min_days, max_days, severity=nil)
        @bugs ||= Groonga["Bugs"]
        min = Time.now - (60 * 60 * 24 * min_days)
        max = Time.now - (60 * 60 * 24 * max_days)
        @records = []
        query = [
          "between(updated_at, #{max.to_i}, #{min.to_i})",
          "done_by._key == ''",
          "archived == false",
          "package.popcon > 0",
          "((affects_stable == false && affects_testing == false && affects_unstable == false) || affects_unstable == true)"
        ]
        if severity
          if severity == "fixme"
            query << "vector_size(comment) == 1"
          elsif BUG_SEVERITY.include?(severity)
            query << "severity == '#{severity}'"
          end
        end
        @bugs.select(query.join(" && "), :syntax => :script)
      end

      def filter_in_values(keys)
        query = "in_values(_key, "
        keys.each_with_index do |key, index|
          if key.is_a?(Integer)
            query << "#{key}"
          else
            query << "\"#{key}\""
          end
          query << ", " if keys.size - 1 != index
        end
        query << ")"
      end

      def filter_by_keys(keys)
        query = ""
        keys.each_with_index do |key, index|
          if key.is_a?(Integer)
            query << "(_key == #{key})"
          else
            query << "(_key @ \"#{key}\")"
          end
          query << " || " if keys.size - 1 != index
        end
        query
      end

      def rc_bugs_by(params={})
        @bugs = Groonga["Bugs"]
        @records = []
        query = [
          "done_by._key == ''",
          "archived == false",
          "package.popcon > 0",
          "((affects_stable == false && affects_testing == false && affects_unstable == false) || affects_unstable == true)"
        ]
        if params[:severity]
          if %w(critical grave serious).include?(params[:severity])
            query.unshift("severity._key == '#{params[:severity]}'")
          else
            query.unshift("(severity._key == 'serious' || severity._key == 'critical' || severity._key == 'grave')")
          end
        else
          query.unshift("(severity._key == 'serious' || severity._key == 'critical' || severity._key == 'grave')")
        end
        if params[:type] and params[:type] == "fixme"
          query << "vector_size(comment) == 1"
        end
        if params[:packages] and params[:packages].size > 0
          pkgs = params[:packages].map do |package|
            %('#{package}', 'src:#{package}')
          end.join(",")
          query << "in_values(package, #{pkgs})"
        end
        @bugs.select(query.join(" && "), :syntax => :script)
      end

      def blocked_bugs
        @bug.blocked_by.sort do |a, b|
          if a.package and b.package
            a.package.popcon <=> b.package.popcon
          else
            -1
          end
        end.reverse
      end

      def missing_comments
        @bugs = Groonga["Bugs"]
        query = [
          "done_by._key == ''",
          "archived == false",
          "vector_size(comment) == 0"
        ].join(" && ")
        options = [
          {
            :key => "package.popcon",
            :order => :descending
          }
        ]
        @records = @bugs.select(query, :syntax => :script).sort(options,
                                                                :limit => 100).records
      end

      def arch_tags_by(params={})
        @bugs = Groonga["Bugs"]
        @records = []
        query = [
          "done_by._key == ''",
          "archived == false",
          "package.popcon > 0",
          "((affects_stable == false && affects_testing == false && affects_unstable == false) || affects_unstable == true)"
        ]
        if params[:port] == "fixme"
          query << "vector_size(comment) == 1"
          ports = ALL_ARCHS.each.collect do |port|
            "tag @ '#{port}'"
          end
          query.unshift("(" + ports.join(" || ") + ")")
        elsif ALL_ARCHS.include?(params[:port])
          query.unshift("tag @ '#{params[:port]}'")
        else
          ports = ALL_ARCHS.each.collect do |port|
            "tag @ '#{port}'"
          end
          query.unshift("(" + ports.join(" || ") + ")")
        end
        @logger.debug(query.join(" && "))
        @bugs.select(query.join(" && "), :syntax => :script)
      end

      def ftbfs_bugs_by(params={})
        @bugs = Groonga["Bugs"]
        @records = []
        query = [
          "done_by._key == ''",
          "archived == false",
          "package.popcon > 0",
          "((affects_stable == false && affects_testing == false && affects_unstable == false) || affects_unstable == true)"
        ]
        if params[:severity]
          if BUG_SEVERITY.include?(params[:severity])
            query << "severity._key == '#{params[:severity]}'"
          elsif params[:severity] == "fixme"
            query << "vector_size(comment) == 1"
          end
        end
        query << "tag @ 'ftbfs'"
        @logger.debug(query.join(" && "))
        @bugs.select(query.join(" && "), :syntax => :script)
      end

      def security_bugs_by(params={})
        @bugs = Groonga["Bugs"]
        @records = []
        query = [
          "done_by._key == ''",
          "archived == false",
          "package.popcon > 0",
          "((affects_stable == false && affects_testing == false && affects_unstable == false) || affects_unstable == true)"
        ]
        if params[:severity]
          if params[:severity] == "fixme"
            query << "vector_size(comment) == 1"
          elsif BUG_SEVERITY.include?(params[:severity])
            query << "severity == '#{params[:severity]}'"
          end
        end
        query << "tag @ 'security'"
        @logger.debug(query.join(" && "))
        @bugs.select(query.join(" && "), :syntax => :script)
      end

      # Groonga::Client related methods

      def select(options={})
        @keys = options[:keys]
        Groonga::Client.open(host: "localhost", protocol: :http, port: 10041) do |client|
          return {} if @keys.empty?

          response = client.select(:table => "Bugs", :filter => filter_in_values(@keys))
          @status = response.success?
          unless response.success?
            return {} if @keys.empty?

          else
            @elapsed_time += response.header[2]
            @bug = response.records.first

            p filter_in_values(@bug.comment)
            p sort_comment_keys
            p sort_comment_keys.reverse
            p filter_in_values(sort_comment_keys)
            response2 = client.select(table: "Comments",
                                      filter: filter_in_values(sort_comment_keys),
                                      sort_keys: "comment_number")
            @status = response2.success?
            if response2.success?
              @comments = response2.records
              #p @comments
            end

            response = client.select(:table => "Bugs", :filter => filter_by_keys(blocked_bugs))
            @status = response.success?
            if response.success?
              @elapsed_time += response.header[2]
            end
            @bug
          end
        end
      end

      def filter_content_in_comments(input)
        words = input.split
        query = ""
        words.each_with_index do |word, index|
          query << "content @ \"#{word}\""
          query << " || " if words.size - 1 != index
        end
        query
      end

      def highlight_full_output_options(input)
        query = "highlight_full(content, \"NormalizerAuto\", true, "
        input.split.each_with_index do |keyword, index|
          query << "\"#{keyword}\", \"<mark>\", \"</mark>\""
          query << ", " if index < input.split.size - 1
        end
        query << ")"
        query
      end

      def fts(input)
        records = []
        Groonga::Client.open(host: "localhost", protocol: :http, port: 10041) do |client|
          return {} if input.empty?

          puts filter_content_in_comments(input)
          puts highlight_full_output_options(input)
          output_columns = "_key, bug, comment_number, subject, from, to, date, _score, #{highlight_full_output_options(input)}"
          puts output_columns
          response = client.select(table: "Comments",
                                   output_columns: output_columns,
                                   command_version: 2,
                                   sort_keys: "bug._key, comment_number",
                                   limit: 10,
                                   offset: 0)
          @status = response.success?
          if response.success?
            @elapsed_time += response.header[2]
            records = response.records
          else
            p response.error_message
          end
        end
        records
      end

      def fts_highlight(input)
        @bugs = Groonga["Bugs"]
        words = input.split
        @snippet = Groonga::Snippet.new(width: 100,
                                        default_open_tag: "<mark>",
                                        default_close_tag: "</mark>",
                                        html_escape: true,
                                        normalize: true)
        words.each do |word|
          @snippet.add_keyword(word)
        end
        options = [
          {
            :key => "package.popcon",
            :order => :descending
          }
        ]
        @records = @bugs.select do |record|
          conditions = []
          unless words.empty?
            match_target = record.match_target do |match_record|
              #(match_record.index("Terms.bugs_index") * 1000) | (match_record.index("Terms.comments_index") * 10)
              (match_record.index("Terms.bugs_index") * 1000)
            end
            full_text_search = words.collect do |word|
              match_target =~ word
            end.inject(&:&)
            conditions << full_text_search
          end
          conditions
        end.sort(options,
                 :limit => 20)
      end

      
    end
  end
end
