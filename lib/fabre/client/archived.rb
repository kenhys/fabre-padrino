# frozen_string_literal: true

module Fabre
  module Client
    class ArchivedBugs
      def initialize(params={})
        @bugs = Groonga["ArchivedBugs"]
      end

      def create(params={})
        return if params[:key].nil? or params[:key].empty?

        now = Time.now.getutc
        @bugs.add(
          params[:key],
          subject: params[:subject],
          package: params[:package],
          originator: params[:originator],
          severity: params[:severity],
          category: params[:category],
          archived: params[:archived],
          created_at: now,
          updated_at: now
        )
      end

      def update(params={})
        return if params[:key].nil? or params[:key].empty?

        return unless @bugs.key?(params[:key])

        values = {}
        values[:done_by] = params[:done_by] if params[:done_by]
        values[:comment] = params[:comment] if params[:comment]
        values[:updated_at] = params[:updated_at]
        @bugs[params[:key]] = values
      end

      def find(id_or_key)
        if id_or_key.numeric?
          find_by_id(id_or_key)
        else
          find_by_key(id_or_key)
        end
      end

      def find_by_key(key)
        @records = @bugs.select do |record|
          record._key == key
        end
        @records
      end

      def find_by_id(id)
        @records = @bugs.select do |record|
          record._id == id
        end
        @records
      end

      def all
        @records = @bugs.select do |record|
          record._key > 0
        end.sort([{
                    key: "_key",
                    order: :descending
                  }])
        @records
      end
    end
  end
end
