module Fabre
  module Client
    class Packages
      def initialize(params={})
        @packages = Groonga["Packages"]
      end

      def maintainer_email_tags
        records = @packages.select do |record|
          record.maintainer.suffix_search("lists.debian.org")
        end
        @records = records.group(["maintainer"]).collect do |record|
          reporter = record.key
          [
            record.n_sub_records,
            reporter["email"],
            record.popcon
          ]
        end
        @records
      end
    end
  end
end
