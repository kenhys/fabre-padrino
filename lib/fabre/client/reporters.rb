module Fabre
  module Client
    class Reporters
      def initialize(params={})
        @reporters = Groonga["Reporters"]
      end

      def lists_debian_org
        records = @reporters.select do |record|
          record.email =~ "lists.debian.org"
        end.sort([{key: "email", order: :ascending}]).uniq
        @records = []
        @names = {}
        records.each do |record|
          unless @names.has_key?(record.email)
            @names[record.email] = true
            @records << {email: record.email, fullname: record.fullname}
          end
        end
        @records
      end
    end
  end
end
