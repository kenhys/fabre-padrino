# frozen_string_literal: true

module Fabre
  module Client
    module Service
      class Queues
        def initialize(params={})
          @queues = Groonga["ServiceQueues"]
        end

        def all
          @records = @queues.select do |record|
            record._id > 0
          end
          @records
        end
      end
    end
  end
end
