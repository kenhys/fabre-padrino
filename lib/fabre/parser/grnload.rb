module Fabre
  module Parser
    class GrnLoad
      def initialize(options={})
        @output = options[:output]
        @commands = []
      end

      def parse(input)
        @input = input.is_a?(String) ? StringIO.new(input) : input
        buffer = []
        @input.string.split("\n").each do |line|
          case line
          when /^load /
            @loading = true
            buffer << line
          when /^\]$/
            buffer << line
            @commands << Groonga::Command::Parser.parse(buffer.join("\n"))
            buffer = []
            @loading = false
          else
            if @loading
              buffer << line
            end
          end
        end
        @commands
      end
    end
  end
end
