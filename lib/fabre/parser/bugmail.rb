module Fabre
  module Parser
    class BugMail
      attr_reader :bug

      def initialize(options={})
        @options = options
        @logger = options[:logger] || Logger.new(STDOUT)
        @parser = Fabre::Parser::ControlMail.new
        @bug = nil
      end

      def bug?
        @bug
      end

      def plain_text
        if @m.multipart?
          if @m.text_part
            body = @m.text_part.decoded
          elsif @m.html_part
            body = @m.html_part.decoded
          end
        else
          body = @m.body.raw_source
        end
        body
      end

      def command_mail?
        @parser.command_mail?
      end

      def changed_bugs
        @parser.ids
      end

      def parse(rfc822)
        @m = Mail.new(rfc822)
        /^Bug\#(\d+?):/.match(@m.subject) do |matched|
          @bug = matched[1].to_i
          @logger.debug("subject: <#{@m.subject}>")
        end
        text = plain_text
        unless text
          @logger.warn("failed to extract")
          return
        end
        if text.is_a?(Array)
          @logger.warn("failed to extract")
          return
        end
        @parser.parse(text)
      end
    end
  end
end
