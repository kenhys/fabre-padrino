module Fabre
  module Parser
    class Popcon
      def initialize(options={})
        @options = options
        @input = options[:input]
      end

      def decrypt
        data = []
        command = "gpg --no-default-keyring --homedir #{ENV["FABRE_GPG_HOMEDIR"]} --trust-model=always -d #{@input}"
        Open3.popen3(command) do |_stdin, stdout, _stderr, wait|
          content = stdout.read
          data = parse_popcon_data(content)
        end
        data
      end

      def parse_popcon_data(data)
        packages = []
        @ignored = 0
        @active = 0
        @inactive = 0
        @packages = []
        data.split("\n").each do |line|
          case line.chomp
          when /^POPULARITY-CONTEST-0/
          when /^END-POPULARITY-CONTEST-0/
          else
            line.chomp.match(/^(\d+?)\s(\d+?)\s(.+?)\s.+/) do |matched|
              if matched[1].to_i.positive? and matched[2].to_i.positive?
                name = matched[3]
                unless name.end_with?("-dev") or name.start_with?("fonts-") or name.start_with?("ttf-")
                  @packages << name
                  @active += 1
                else
                  @ignored += 1
                end
              else
                @inactive += 1
              end
            end
          end
        end
        {
          active: @active,
          inactive: @inactive,
          ignored: @ignored,
          packages: @packages
        }
      end

      def parse
        data = {}
        if @options[:decrypt]
          data = decrypt
        else
          File.open(@input) do |file|
            data = parse_popcon_data(file.read)
          end
        end
        data
      end
    end
  end
end
