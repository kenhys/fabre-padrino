module Fabre
  module Parser
    class ControlMail

      def initialize
        @command_mail = false
        @data = {}
      end

      def parse(body)
        body.each_line do |line|
          case line.chomp
          when /^Processing control commands:/
            @command_mail = true
          when /^Processing commands for control@bugs.debian.org:/
            @command_mail = true
          when /^> close (-?\d+)\s?([0-9\-.]+)?/
            @data[:bug] = $1.to_i
            @data[:version] = $2
          when /^> close (-?\d+?)/
            @data[:bug] = $1.to_i
          when /^> block (-?\d+?) by (\d.+)/
            @data[:blocked] = $1.to_i
            @data[:blocked_by] = $2.to_i
          when /^> affects (-?\d+?) /
            @data[:affects] = $1.to_i
          when /^> unmerge (-?\d+?) /
            @data[:unmerge] = $1.to_i
          when /^> owner (-?\d+?) /
            @data[:owner] = $1.to_i
          when /^> forcemerge /
            m = line.chomp.match(/^> forcemerge /)
            @data[:forcemerge] = m.post_match.scan(/-?\d+/).map(&:to_i)
          when /^Bug \#(\d+) /
            @data[:target] = $1.to_i
          when /^Control: reassign /
            m = line.chomp.match(/^Control: (?<command>\w+) /)
            @data[:reassign] = m.post_match.scan(/-?\d+/).map(&:to_i)
            @command_mail = true
          else
          end
        end
        unless @command_mail
          @data = {}
        end
        if @data[:target] and (@data[:bug] == -1 or @data[:blocked] == -1 or @data[:affects] == -1)
          @data[:bug] = @data[:blocked] = @data[:affects] = @data[:target]
        end
        %i[reassign reopen found notfound fixed notfixed submitter forwarded notforwarded severity outlook clone merge forcemerge unmerge tags block unblock close  owner noowner archive unarchive].each do |key|
          if @data[key] and @data[key].include?(-1)
            @data[key] = @data[key].each.reject { |i| i == -1 }
          end
        end
        @data
      end

      def command_mail?
        @command_mail
      end

      def ids
        if @data.empty?
          []
        else
          [@data[:bug],
           @data[:target],
           @data[:blocked],
           @data[:blocked_by],
           @data[:unmerge],
           @data[:owner],
           @data[:affects],
           @data[:forcemerge],
           @data[:reassign]
          ].flatten.uniq.compact
        end
      end
    end
  end
end
