require "timeout"
require "fileutils"
require "linguist"
require "github/markup"

module Fabre
  module Checker
    class CommentFormat
      def initialize(options={})
        @output = options[:output] || $stdout
        @options = {}
      end

      def parse_options(argv)
        parser = OptionParser.new
        parser.on("-s", "--since") do |v|
        end
        parser.on("-y", "--year") do |v|
        end
        parser.on("-g", "--grn") do |v|
        end
        parser.parse!(argv)
      end

      def collect_comment_keys(args)
        keys = []
        args.each do |candidate|
          if candidate.match?(/^\d+#\d+/)
            keys << candidate
          else
            records = @comments.select do |record|
              record._key =~ /^#{candidate}/
            end
            records.each do |record|
              keys << record._key
            end
          end
        end
        p keys
        keys.sort.uniq
      end

      def run(argv=[])
        args = parse_options(argv)
        begin
          @database = Fabre::Database.new
          @database.open do
            @comments = Groonga["Comments"]
            collect_comment_keys(args).each do |key|
              p key
              content = @comments[key]["content"]
              puts "|#{content[0..600]}|"
              blob = ::Linguist::Blob.new("", content, symlink: false)
              p ::Linguist.detect(blob)
              p ::GitHub::Markup.renderer("", content)
              p ::GitHub::Markup.can_render?("", content)
              puts ::GitHub::Markup.render_s(GitHub::Markups::MARKUP_MARKDOWN, content)
            end
          end
        end
      end
    end
  end
end
