require "net/http"
require "securerandom"
require "gitlab"

module Fabre
  module Salsa
    class Authentication
      attr_reader :user

      def initialize
        @user = nil
      end

      def authenticate!(params)
        uri = URI.parse("https://salsa.debian.org/oauth/token")
        form_data = {
          client_id: params[:client_id],
          client_secret: params[:client_secret],
          code: params[:code],
          grant_type: "authorization_code",
          redirect_uri: params[:redirect_uri]
        }
        response = Net::HTTP.post_form(uri, form_data)
        case response
        when Net::HTTPSuccess
          json = JSON.parse(response.body)
          @client = Gitlab.client(
            endpoint: 'https://salsa.debian.org/api/v4',
            private_token: json["access_token"]
          )
          @user = @client.user
          # @client.user_projects(@user.id, membership: true).each do |project|
          #   pp project.path_with_namespace
          # end
          #@client.groups(min_access_level: 30).each do |group|
          #pp group.web_url
          #end
        else
          p response
        end
      end

      def success?
        !!@user
      end
    end
  end
end
