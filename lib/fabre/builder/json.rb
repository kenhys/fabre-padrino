require "open3"

require "fabre/helper/path"

module Fabre
  module Builder
    class JSONIO
      include Fabre::Helper::Path

      def initialize(options={})
        @input = options[:input]
        @logger = options[:logger] || Logger.new(STDOUT)
        @output = options[:output]
      end

      def run
        perl_script = File.expand_path(File.join(File.dirname(__FILE__), "../../../", "exe/fabre-extract-log.pl"))
        command = "perl -I#{ENV['DEBBUGS_LIB_DIR']} #{perl_script} --log-file=#{@input}"
        unless File.exist?(@input)
          @logger.error("no such a file: <#{@input}>")
          return
        end
        @logger.debug("execute: <#{command}>")
        Open3.popen3(command) do |_stdin, stdout, _stderr, _wait|
          @output << stdout.read
        end
      end
    end

    class JSONFile
      include Fabre::Helper::Path

      def initialize(options={})
        @input = options[:input]
        @output = options[:output] || json_zst_path(File.basename(@input, LOG_SUFFIX))
      end

      def run
        command = "perl -I#{ENV['DEBBUGS_LIB_DIR']} exe/fabre-extract-log.pl --log-file=#{@input}"
        Open3.popen3(command) do |_stdin, stdout, _stderr, _wait|
          json_path = @output.sub(/\.zst/, "")
          File.open(json_path, "w+") do |file|
            file.puts(stdout.read)
          end
          `zstd --force --compress -19 --rm #{json_path}`
        end
      end
    end

    class JSON
      include Fabre::Helper::Path

      def initialize(options={})
        @options = options
        @input = options[:input]
        @output = options[:output] || $stdout
        @console = options[:console] || $stderr
        @pipeline = options[:pipeline] || true
      end

      def run
        unless @input.end_with?(LOG_SUFFIX)
          @console.puts("skip converting log: <#{@input}>")
          return
        end
        @console.puts("rebuild json from: <#{@input}>")
        options = {
          input: @input,
          output: @output
        }
        rebuilder = @pipeline ? JSONIO.new(options) : JSONFile.new(options)
        rebuilder.run
      end
    end
  end
end
