require "fabre/helper/path"
require "fabre/helper/database"
require "fabre/loader/grn"
require "fabre/converter/generator"
require "fabre/builder/json"
require "fabre/builder/pipeline"
require "fabre/client/archived"

module Fabre
  module Builder
    class GrnIO
      include Fabre::Helper::Path

      def initialize(options={})
        @options = {}
        @input = options[:input]
        @output = options[:output] || STDOUT
        @logger = options[:logger] || Logger.new(STDOUT)
        @archived = options[:archived] || false
        @bug_number = options[:bug_number] || 0
      end

      def run
        begin
          options = {
            input: @input,
            output: @output,
            archived: @archived
          }
          converter = Fabre::Converter::GrnIOGenerator.new(options)
          converter.run
        rescue StandardError => e
          @logger.debug("failed to rebuild buffered grn")
          @logger.debug("error: <#{e.message}>")
        end
      end

      def save
        zst_path = grn_zst_path(@bug_number)
        grn_path = zst_path.sub(/.zst/, "")
        File.open(grn_path, "w+") do |file|
          file.puts(@output.string)
        end
        `zstd --compress -19 -f --rm #{grn_path}`
      end
    end

    class GrnFile
      include Fabre::Helper::Path
      include Fabre::Helper::Database

      def initialize(options={})
        @options = options
        @input = options[:input]
        @output = options[:output] || STDOUT
        @logger = options[:logger] || Logger.new(STDOUT)
        @archived = false
      end

      def run
        unless @input.end_with?(JSON_SUFFIX)
          @logger.warn("json file is required :<#{@input}>")
          return
        end
        base_name = File.basename(@input, JSON_SUFFIX)
        @grn_path = File.join(File.dirname(@input), "#{base_name}.grn")
        @zst_path = "#{@grn_path}.zst"
        begin
          File.open(@grn_path, "w+") do |output|
            options = {
              debug: false,
              input: @input,
              output: output,
              logger: @logger,
              archived: @archived
            }
            converter = GrnFileGenerator.new(options)
            converter.run
          end
          FileUtils.rm_f(@zst_path)
          @logger.debug("compress by zstd: <#{@grn_path}>")
          `zstd --compress -19 -f --rm #{@grn_path}`
        rescue StandardError => e
          @logger.error("failed to rebuild grn: <#{@input}>")
          @logger.error("error: <#{e.messages}>")
          FileUtils.rm_f(@grn_path)
        end
      end
    end

    class Grn
      include Fabre::Helper::Path
      include Fabre::Helper::Database

      def initialize(options={})
        @options = options
        @input = options[:input]
        @output = options[:output] || STDOUT
        @load = options[:load] || true
        @logger = options[:logger] || Logger.new(STDOUT)
        @on_memory = options.key?(:on_memory) ? options[:on_memory] : true
        @keep = options[:keep]
        @archived = false
        @bug = 0
      end

      def archived?
        archived = false
        open_database do
          archived_bugs = Fabre::Client::ArchivedBugs.new
          bug = File.basename(@input, LOG_SUFFIX)
          records = archived_bugs.find_by_key(bug)
          if records and records.count == 1
            archived = true
          end
        end
        archived
      end

      def run
        @logger.info("converting: <#{@input}>")
        @bug = File.basename(@input, LOG_SUFFIX)
        options = {
          bug_number: @bug,
          input: @input,
          archived: archived?,
          logger: @logger,
          load: @load,
          keep: @keep
        }
        if @on_memory
          @runner = GrnIOPipeline.new(options)
        else
          @runner = GrnFilePipeline.new(options)
        end
        @runner.run
      end

      def save
        unless @on_memory
          grn_path = grn_zst_path(@bug).sub(".zst", "")
          FileUtils.mkdir_p(File.dirname(grn_path))
          File.open(grn_path, "w+") do |file|
            file.puts(@output.string)
          end
        end
      end

      def skip_convert?(path)
        unless File.exist?(path)
          return true
        end

        unless File.exist?(@zst_path)
          return false
        end

        if File.mtime(@zst_path) < File.mtime(path)
          false
        else
          unless @options[:force]
            puts "skip converting: <#{path}>"
            return true
          end
        end
        false
      end
    end
  end
end
