require "fabre/helper"
require "fabre/loader"

module Fabre
  module Builder
    class GrnIOPipeline
      include Fabre::Helper::Path

      def initialize(options={})
        @input = options[:input]
        @logger = options[:logger] || Logger.new(STDOUT)
        @bug_number = options[:bug_number]
        @load = options[:load]
        @keep = options[:keep]
      end

      def run
        @json_output = StringIO.new
        @logger.debug("input: <#{@input}>")
        options = {
          input: @input,
          output: @json_output,
          bug_number: @bug_number,
          logger: @logger
        }
        json_rebuilder = JSONIO.new(options)
        json_rebuilder.run

        @input = @json_output
        @grn_output = StringIO.new
        options = {
          input: @input,
          output: @grn_output,
          bug_number: @bug_number,
          logger: @logger
        }
        rebuilder = GrnIO.new(options)
        rebuilder.run

        options = {
          input: @grn_output,
          bug_number: @bug_number,
          logger: @logger
        }
        @loader = Fabre::Loader::GrnIO.new(options)
        if @load
          @logger.debug("load: <#{@bug_number}>")
          begin
            @loader.run
            unless @keep
              @logger.debug("remove auxiliary files: <#{@bug_number}>")
              FileUtils.rm_f(log_path(@bug_number))
              FileUtils.rm_f(json_path(@bug_number))
              FileUtils.rm_f(grn_path(@bug_number))
              FileUtils.rm_f(groonga_log_path(@bug_number))
            else
              keep_files
            end
          rescue Fabre::GrnLoadError => e
            keep_files
          ensure
            FileUtils.rm_f(groonga_log_path(@bug_number))
          end
        else
          rebuilder.save
        end
        true
      end

      def keep_files
        # save .json, .grn
        File.open(json_path(@bug_number), "w+") do |file|
          file.puts(@json_output.string)
        end
        `zstd --compress -19 -f --rm #{json_path(@bug_number)}`
        File.open(grn_path(@bug_number), "w+") do |file|
          file.puts(@grn_output.string)
        end
        `zstd --compress -19 -f --rm #{grn_path(@bug_number)}`
      end
    end

    class GrnFilePipeline
      include Fabre::Helper::Path

      def initialize(options={})
        @input = options[:input]
        @bug_number = options[:bug_number]
        @logger = options[:logger] || Logger.new(STDOUT)
      end

      def run
        options = {
          input: @input,
          output: json_zst_path(@bug_number),
          logger: @logger
        }
        json_rebuilder = JSONFile.new(options)
        json_rebuilder.run

        @input = @output
        options = {
          input: json_zst_path(@bug_number),
          output: grn_zst_path(@bug_number),
          logger: @logger
        }
        rebuilder = GrnFile.new(options)
        rebuilder.run

        @loader = Fabre::GrnLoader.new
        if @load
          @loader.run([bug_number])
        end
      end
    end
  end
end
