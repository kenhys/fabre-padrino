require "fabre/helper"
require "fabre/builder/grn"
require "concurrent"

module Fabre
  module Builder
    class Worker
      include Fabre::Helper::Path

      def initialize(options={})
        @options = options
        @logger = options[:logger] || Logger.new(STDOUT)
        @on_memory = options.key?(:on_memory) ? options[:on_memory] : true
        @keep = options[:keep]
      end

      def run(args=[])
        units = []
        workers = []
        n_units = args.size
        n_worker = 1
        if (args.size / Concurrent.processor_count).positive?
          n_units = args.size / Concurrent.processor_count
          n_worker = Concurrent.processor_count
        end
        @logger.debug("bugs: <#{args.size}> workers: <#{n_worker}> units: <#{n_units}>")
        units = args.each_slice(n_units + 1).to_a
        n_worker.times do |i|
          @logger.debug("worker units[#{i}]: <#{units[i].size}>")
          workers << Thread.new do
            units[i].each do |unit|
              path = unit
              unless unit.end_with?(JSON_SUFFIX) or unit.end_with?(GRN_SUFFIX)
                path = log_path(unit)
              end
              unless File.exist?(path)
                @logger.warn("input file does not exist: <#{path}>")
                next
              end
              output = StringIO.new
              options = {
                input: path,
                output: output,
                on_memory: @on_memory,
                logger: @logger,
                load: true,
                keep: @keep
              }
              grn = Fabre::Builder::Grn.new(options)
              grn.run
              grn.save
            end
          end
        end
        workers.each(&:join)
      end
    end
  end
end
