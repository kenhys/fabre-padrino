require "email_reply_parser"

module Fabre
  module Converter
    class MailBody
      HEADER_PATTERN = /^(([A-Z].+):\s(.+))/
      BEGIN_PGP_SIGNATURE = "-----BEGIN PGP SIGNATURE-----"
      END_PGP_SIGNATURE = "-----END PGP SIGNATURE-----"
      SYSTEM_INFORMATION = "-- System Information:"

      def initialize(options={})
        @debug = options[:debug] || false
        @input = options[:input] || ""
        @output = options[:output]
        @console = options[:console] || $stdout
        @headers = {}
        @stable = false
        @testing = false
        @unstable = false
      end

      def filter(options={})
        @input = options[:input] if options[:input]
        @output = options[:output] if options[:output]
        @output << filter_comment
        @output.string
      end

      def debug?
        @debug and @input.size < 1000
      end

      def stable?
        @stable
      end

      def testing?
        @testing
      end

      def unstable?
        @unstable
      end

      def header?(fragment)
        @console.puts("before header: <#{fragment}>") if debug?
        @last_empty_index = fragment.to_s.split("\n", 10).index(&:empty?)
        return false if @last_empty_index.nil?

        @last_matched_index = fragment.to_s.split("\n", @last_empty_index + 1)[0..@last_empty_index - 1].rindex do |line|
          line.match?(HEADER_PATTERN)
        end
        @last_matched_index and @last_matched_index >= 0
      end

      def filter_header(fragment)
        @console.puts(fragment.to_s) if debug?
        buffer = fragment.to_s
        if header?(fragment)
          if fragment.to_s.split("\n")[@last_matched_index + 2..]
            buffer = fragment.to_s.split("\n")[@last_matched_index + 2..].join("\n")
          else
            buffer = ""
          end
        end
        @console.puts("filter header: <#{buffer}>") if debug?
        buffer
      end

      def check_prefers(line)
        if line and line.strip.start_with?("APT prefers stable")
          @stable = true
        end
        if line and line.strip.start_with?("APT prefers testing")
          @testing = true
        end
        if line and line.strip.start_with?("APT prefers unstable")
          @unstable = true
        end
      end

      def filter_comment
        buffer = []
        parser = EmailReplyParser.read(@input)
        parser.fragments.each_with_index do |fragment, index|
          @console.puts("check fragment[#{index}]: <#{fragment}>") if debug?
          if fragment.signature?
            # 965166#5
            if fragment.to_s.start_with?("---")
              buffer << filter_header(fragment)
            else
              lines = fragment.to_s.split("\n")
              index = lines.index(SYSTEM_INFORMATION)
              if index and index >= 0
                check_prefers(lines[index + 2])
              end
            end
          elsif fragment.quoted?
          else
            lines = fragment.to_s.split("\n")
            bindex = lines.find_index(BEGIN_PGP_SIGNATURE)
            eindex = lines.find_index(END_PGP_SIGNATURE)
            if bindex and eindex and bindex >= 0 and eindex > 0
              if bindex > 0
                buffer << lines[0..(lines.index(BEGIN_PGP_SIGNATURE)-1)].join("\n")
              end
              if eindex > 0
                buffer << lines[(lines.index(END_PGP_SIGNATURE)+1)..-1].join("\n")
              end
            elsif lines.index("-- ") and lines.index("-- ") > 0
              buffer << lines[0..lines.index("-- ")-1].join("\n") + "\n"
            else
              index = lines.index(SYSTEM_INFORMATION)
              if index and index >= 0
                if index > 0
                  buffer << lines[0..index-1].join("\n")
                end
                check_prefers(lines[index + 2])
              else
                buffer << filter_header(fragment)
              end
            end
          end
        end
        filtered = buffer.join("\n")
        @console.puts("filter comment: <#{filtered}>") if debug?
        filtered
      end
    end
  end
end
