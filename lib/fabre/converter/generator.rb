require "json"
require "time"
require "stringio"
require "mail"
require "kconv"
require "zstd-ruby"
require "jay"
require "logger"

require_relative "markup"
require_relative "mailbody"

module Fabre
  module Converter
    class GrnGenerator
      def last_modified
        to_bug_time(@headers["last_modified"])
      end

      def reported_date
        to_bug_time(@headers["date"])
      end

      def to_bug_time(unixtime_string)
        # Note that "UTC" causes unexpected error
        Time.at(unixtime_string.to_i, in: "+00:00").to_i
      end

      def comments_fields
        ["bug", "comment_number", "from", "to", "subject", "date", "content", "markup"].sort
      end

      def comment_orders
        @comments.keys.sort do
          |a, b| a.to_i <=> b.to_i
        end
      end

      def json?(path)
        path.end_with?(".json.zst") or path.end_with?(".json")
      end

      def write_load(table)
        @output.puts "\nload --table #{table}"
        @output.puts "["
        yield @output
        @output.puts "]"
      end

      def append_from(key, value)
        if value and not value.empty?
          @from << value
          @buffer << %("#{key}": "#{Jay.j(value.toutf8)}")
        end
      end

      def spam?(email)
        email.downcase.start_with?("undisclosed")
      end

      def append_to(key, value)
        if value and not value.empty?
          if spam?(value)
            @to << value
            @buffer << %("spam": true)
          else
            to_list = []
            value.split(",").each do |to|
              normalized = to.toutf8.gsub(/\"/, "").strip
              @to << normalized
              to_list << normalized
            end
            content = to_list.join("\",\"")
            @buffer << %("#{key}": ["#{content}"])
          end
        end
      end

      def write_comments
        @affects_stable = false
        @affects_testing = false
        @affects_unstable = false
        write_load("Comments") do |output|
          data = []
          comment_orders.each do |comment_number|
            comment = @comments[comment_number]
            @buffer = [%("_key": #{@bug_number.to_i * 10_000 + comment_number.to_i})]
            comments_fields.each do |key|
              case key
              when "bug"
                @buffer << %("bug": #{@bug_number})
              when "comment_number"
                @buffer << %("comment_number": #{comment_number.to_i})
              when "date"
                if comment[key] and not comment[key].empty?
                  unixtime = 0
                  begin
                    unixtime = Time.parse(comment[key]).to_i
                  rescue ArgumentError
                    # force fallback to UTC
                    timestamp = Time.parse(comment[key].sub(/[\+-]\d{4}/, "+00:00Z"))
                    unixtime = timestamp.to_i
                  end
                  @logger.debug "#{comment[key]} #{unixtime} #{Time.at(unixtime, in: '+00:00')}" if @debug
                  @buffer << %("date": #{unixtime})
                end
              when "content"
                options = {
                  debug: @debug,
                  input: comment['body'],
                  output: StringIO.new
                }
                mailbody = Fabre::Converter::MailBody.new(options)
                content = mailbody.filter
                unless content.empty?
                  @buffer << %("content": "#{Jay.j(content)}")
                end
                if mailbody.stable?
                  @affects_stable = :stable
                end
                if mailbody.testing?
                  @affects_testing = true
                end
                if mailbody.unstable?
                  @affects_unstable = true
                end
              when "markup"
                options = {
                  debug: @debug,
                  input: comment['body'],
                  output: StringIO.new
                }
                converter = Fabre::Converter::Markup.new(options)
                content = converter.markup
                unless content.empty?
                  @buffer << %("markup": "#{Jay.j(content)}")
                end
              when "from"
                append_from(key, comment[key])
              when "to"
                append_to(key, comment[key])
              else
                if comment[key] and not comment[key].empty?
                  @buffer << %("#{key}": "#{comment[key].gsub(/\n/, '').gsub(/"/, "\\\"")}")
                end
              end
            end
            data << sprintf("{%s}", @buffer.join(", "))
          end
          output.puts data.join(",\n")
        end
        if @affects_stable or @affects_testing or @affects_unstable
          write_load("Bugs") do |output|
            if @affects_stable
              output.puts(%({"_key": #{@bug_number}, "affects_stable": true}))
            end
            if @affects_testing
              output.puts(%({"_key": #{@bug_number}, "affects_testing": true}))
            end
            if @affects_unstable
              output.puts(%({"_key": #{@bug_number}, "affects_unstable": true}))
            end
          end
        end
      end

      def bugs_fields
        [
          "package",
          "subject",
          "originator",
          "created_at",
          "updated_at",
          "severity",
          "done_by",
          "blocked_by",
          "tag",
          "comment",
          "forwarded"
        ].sort
      end

      def write_bugs
        @originator = @headers["originator"]
        table = @archived ? "ArchivedBugs" : "Bugs"
        write_load(table) do |output|
          data = {}
          bugs_fields.each do |key|
            case key
            when "package"
              unless @headers[key].start_with?("src:")
                data[key] = "src:#{@headers[key]}"
              else
                data[key] = @headers[key]
              end
            when "created_at"
              data[key] = reported_date
            when "updated_at"
              data[key] = last_modified
            when "done_by"
              if @headers["done"]
                data[key] = @headers["done"]
                @done_by = @headers["done"]
              end
            when "blocked_by"
              if @headers["blockedby"] and not @headers["blockedby"].empty?
                data[key] = @headers["blockedby"].split(" ").map(&:to_i)
              end
            when "tag"
              if @headers["keywords"] and not @headers["keywords"].empty?
                data[key] = @headers["keywords"].split(" ")
              end
            when "comment"
              keys = []
              @comments.keys.each do |number|
                keys << @bug_number.to_i * 10_000 + number.to_i
              end
              data[key] = keys
            when "forwarded"
              if @headers["forwarded"] and not @headers["forwarded"].empty?
                data[key] = @headers["forwarded"]
              end
            else
              data[key] = @headers[key]
            end
          end
          buffer = ["\"_key\": #{@headers['bug_num'].to_i}"]
          data.each do |key,value|
            if value.kind_of?(Array)
              buffer << "\"#{key}\": #{value.to_s}"
            elsif ["created_at", "updated_at"].include?(key)
              buffer << "\"#{key}\": #{value}"
            else
              buffer << "\"#{key}\": \"#{Jay.j(value)}\""
            end
          end
          output.printf"{%s}\n", buffer.join(", ")
        end
      end

      def reporter(target)
        data = []
        if target =~ /Debian Bug Tracking System:/
          # Remove semicolon explicitly
          target.sub!(/:/, "")
        end
        # Bug647900
        target.gsub!(/"/, "")
        target.gsub!(/\n/, "")
        target.gsub!(/\t/, "")
        address = ""
        begin
          address = Mail::Address.new(target.toutf8)
          unquote = address.to_s.gsub('"', "")
          data << %("_key": "#{unquote}") unless address.to_s.empty?
          data << %("email": "#{address.address}") unless address.address.nil? or address.address.empty?
          data << %("fullname": "#{address.display_name}") unless address.display_name.nil? or address.display_name.empty?
        rescue
          # Bug10400
          address = target.toutf8
          data << %("_key": "#{address}") unless address.empty?
        end
        sprintf("{%s}", data.join(", "))
      end

      def write_reporters
        write_load("Reporters") do |output|
          data = []
          if @originator
            unless @originator.empty?
              data << reporter(@originator)
            end
          end
          if @done_by
            unless @done_by.empty?
              data << reporter(@done_by)
            end
          end
          @from.each do |from|
            next if from.nil? or from.empty?

            data << reporter(from)
          end
          @to.each do |to|
            next if to.nil? or to.empty?

            to.split(",").each do |target|
              unless target.strip.empty?
                data << reporter(target.strip)
              end
            end
          end
          output.printf("%s\n", data.sort.uniq.join(",\n"))
        end
      end

      def read_json(file)
        if file.path.end_with?(".json.zst")
          Zstd.decompress(File.read(file))
        else
          File.read(file)
        end
      end
    end

    class GrnIOGenerator < GrnGenerator
      def initialize(options={})
        @input = options[:input]
        @output = options[:output] || STDOUT
        @logger = options[:logger] || Logger.new(STDOUT)
        @archived = options[:archived] || false
        @debug = options[:debug] || false
        @from = []
        @to = []
        @done_by = nil
      end

      def run(options={})
        @input = options[:input] if options[:input]
        @output = options[:output] if options[:output]
        @archived = options[:archived] if options[:archived]

        @logger.debug("Read: <#{@input}>") if @debug
        json = JSON.parse(@input.string)
        @headers = json["bug"]
        @bug_number = @headers["bug_num"]
        @comments = json["comments"]
        @headers["created_at"] = reported_date
        write_bugs
        write_comments
        write_reporters
      end
    end

    class GrnFileGenerator < GrnGenerator
      def initialize(options={})
        @input = options[:input]
        @output = options[:output] || $stdout
        @archived = options[:archived] || false
        @debug = options[:debug] || false
        @from = []
        @to = []
        @done_by = nil
      end

      def json?(path)
        path.end_with?(".json.zst") or path.end_with?(".json")
      end

      def run(options={})
        @input = options[:input] if options[:input]
        @output = options[:output] if options[:output]
        @archived = options[:archived] if options[:archived]
        unless json?(@input)
          puts "input is not json: <#{@input}>"
          raise ArgumentError
        end

        unless File.exist?(@input)
          puts "missing: <#{@input}>"
          return
        end

        puts "Read: <#{@input}>" if @debug
        File.open(@input, "r") do |file|
          json = JSON.parse(read_json(file))
          @headers = json["bug"]
          @bug_number = @headers["bug_num"]
          @comments = json["comments"]
          @headers["created_at"] = reported_date
          write_bugs
          write_comments
          write_reporters
        end
      end
    end
  end
end
