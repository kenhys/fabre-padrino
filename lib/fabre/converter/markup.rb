require "strscan"
require "email_reply_parser"

module Fabre
  module Converter
    class Markup
      def initialize(options={})
        @debug = options[:debug] || false
        @input = options[:input] || ""
        @output = options[:output] || $stdout
      end

      def markup(options={})
        @input = options[:input] if options[:input]
        @output = options[:output] if options[:output]
        markup_comment
        @output.string
      end

      def markup_comment
        parser = EmailReplyParser.read(@input)
        parser.fragments.each_with_index do |fragment, index|
          if fragment.quoted?
            puts("quoted?[#{index}]: <#{convert_fragment(fragment)}>") if @debug
            @output << %(\n<blockquote class=\\"blockquote\\">\n)
            @output << convert_fragment(fragment)
            @output << %(\n</blockquote>\n)
          elsif fragment.signature?
            @output << %(<div class=\\"signagure\\">\n)
            @output << convert_fragment(fragment)
            @output << %(</div>\n)
          else
            puts("normal: <#{convert_fragment(fragment)}>") if @debug
            @output << convert_fragment(fragment)
          end
        end
        unless @output.string.empty?
          unless @output.string.end_with?("\n")
            if @output.string.split("\n").count > 1
              @output << "\n"
            end
          end
        end
      end

      def convert_fragment(fragment)
        fragment.to_s.gsub(/>/, "&gt;").gsub(/</, "&lt;").gsub(/\"/, "&quot;").split("\n").collect do |line|
          match = /\A(cf\.)?(\s+)?(?<link>https?:\/\/[^ ]+)(?<rest>.*)/.match(line)
          if match
            url = match["link"]
            if match["rest"]
              %(<a href="#{url}"><i class="ri-external-link-line"></i><mark>#{url}</mark></a>#{match["rest"]})
            else
              %(<a href="#{url}"><i class="ri-external-link-line"></i><mark>#{url}</mark></a>)
            end
          else
            line
          end
        end.join("\n")
      end
    end
  end
end
