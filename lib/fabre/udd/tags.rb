require "fabre/udd/base"

module Fabre
  module Udd
    class Tags < Base
      def initialize(options={})
        super
        @options = options
        @logger = options[:logger]
        @n_page = 0
      end

      def where_clause(order_by: false)
        query = ""
        if @options[:year]
          query = " DATE(bugs.last_modified) BETWEEN '#{@options[:year]}-01-01' AND '#{@options[:year]}-12-31'"
        elsif @options[:since]
          tm = Time.parse(@options[:since])
          ymd = tm.strftime("%Y-%m-%d")
          query = " DATE(bugs.last_modified) >= '#{ymd}'"
        elsif @options[:term_start] and @options[:term_end]
          query = " DATE(bugs.last_modified) BETWEEN '#{@options[:term_start]}' AND '#{@options[:term_end]}'"
        else
          timestamp = DateTime.now - 3
          ymd = timestamp.strftime('%Y-%m-%d')
          query = " DATE(bugs.last_modified) >= '#{ymd}'"
        end
        if order_by
          query << " ORDER BY bugs.id DESC"
        end
        query
      end

      def query_count
        @query = "SELECT count(bugs.id) FROM bugs inner join bugs_tags on bugs.id = bugs_tags.id WHERE "
        @query << where_clause(order_by: false)
        @logger.debug("query: <#{@query}>")
        results = @connect.exec(@query)
        @logger.debug("fetched <#{results[0]['count']}>")
        count = results[0]["count"].to_i
        count
      end

      def select_query
        "SELECT bugs.id AS id, bugs_tags.tag AS tag FROM bugs inner join bugs_tags on bugs.id = bugs_tags.id WHERE "
      end

      def select(options={})
        @options.merge!(options)
        count = query_count
        @n_page = (count / 10_000).zero? ? 1 : count / 10_000
        @bugs = {}
        @n_page.times do |index|
          offset = 10_000 * index
          @logger.debug("offset <#{offset}>")
          query = select_query
          query << where_clause(order_by: true)
          query << " OFFSET #{offset}"
          @logger.debug("query: <#{query}>")
          results = @connect.exec(query)
          results.each do |record|
            id = record["id"]
            unless @bugs.has_key?(id)
              @bugs[id] = [record["tag"]]
            else
              @bugs[id] << record["tag"]
            end
          end
        end
        @bugs
      end

      def execute(query)
        @connect.exec(query)
      end
    end
  end
end
