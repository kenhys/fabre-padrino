require "pg"

module Fabre
  module Udd
    class Base
      def initialize(options={})
        @connect = PG.connect(host: "udd-mirror.debian.net",
                              user: "udd-mirror",
                              password: "udd-mirror",
                              dbname: "udd", port: "5432")
      end

      def finish
        @connect.finish
      end
    end
  end
end
