require "fabre/udd/base"

module Fabre
  module Udd
    class UserTags < Base
      def initialize(options={})
        super
        @options = options
        @logger = options[:logger]
        @n_page = 0
      end

      def where_clause(order_by: false)
        query = ""
        if @options[:year]
          query = " DATE(bugs.last_modified) BETWEEN '#{@options[:year]}-01-01' AND '#{@options[:year]}-12-31'"
        elsif @options[:since]
          tm = Time.parse(@options[:since])
          ymd = tm.strftime("%Y-%m-%d")
          query = " DATE(bugs.last_modified) >= '#{ymd}'"
        elsif @options[:term_start] and @options[:term_end]
          query = " DATE(bugs.last_modified) BETWEEN '#{@options[:term_start]}' AND '#{@options[:term_end]}'"
        else
          timestamp = DateTime.now - 3
          ymd = timestamp.strftime('%Y-%m-%d')
          query = " DATE(bugs.last_modified) >= '#{ymd}'"
        end
        if order_by
          query << " ORDER BY bugs.id DESC"
        end
        query
      end

      def where_tags
        query = ""
        p @options
        if @options[:tag] == "all"
          ports = []
          %w(armel armhf arm64 hurd-i386 mips64el mipsel ppc64 ppc64el riscv64 sparc64 sh sh4 s390x).each.collect do |port|
            ports << "bugs_usertags.tag = '#{port}'"
          end
          query << "(" + ports.join(" OR ") + ") AND "
        else
          query << "bugs_usertags.tag = '#{@options[:tag]}' AND "
        end
        query
      end

      def query_count
        @query = "SELECT count(bugs.id) FROM bugs inner join bugs_usertags on bugs.id = bugs_usertags.id WHERE "
        @query << where_tags
        @query << where_clause(order_by: false)
        @logger.debug("query: <#{@query}>")
        results = @connect.exec(@query)
        @logger.debug("fetched <#{results[0]['count']}>")
        count = results[0]["count"].to_i
        count
      end

      def select(options={})
        @options.merge!(options)
        count = query_count
        @n_page = (count / 10_000).zero? ? 1 : count / 10_000
        @bugs = {}
        @n_page.times do |index|
          offset = 10_000 * index
          @logger.debug("offset <#{offset}>")
          query = "SELECT bugs.id AS id, bugs_usertags.tag AS tag FROM bugs inner join bugs_usertags on bugs.id = bugs_usertags.id WHERE "
          query << where_tags
          query << where_clause(order_by: true)
          query << " OFFSET #{offset}"
          @logger.debug("query: <#{query}>")
          results = @connect.exec(query)
          results.each do |record|
            id = record["id"]
            unless @bugs.has_key?(id)
              @bugs[id] = [record["tag"]]
            else
              @bugs[id] << record["tag"]
            end
          end
        end
        @bugs
      end

      def execute(query)
        @connect.exec(query)
      end

      def save
        @path = "#{@options[:tag]}.list"
        if @options[:since]
          @path = "#{@options[:tag]}.since#{@options[:since]}.list"
        elsif @options[:year]
          @path = "#{@options[:tag]}.#{@options[:year]}.list"
        end
        File.open(@path, "w+") do |file|
          @bugs.each do |bug|
            file.puts(bug)
          end
        end
        @logger.info("saved: <#{@path}>")
      end
    end
  end
end
