require "fabre/udd/base"

module Fabre
  module Udd
    class ArchivedBugs < Base
      def initialize(options={})
        super
        @options = options
        @logger = options[:logger] || Logger.new(STDOUT)
        @n_page = 0
        @n_max = 0
      end

      def where_clause(order_by: false)
        query = ""
        if @options[:year]
          query = " DATE(last_modified) BETWEEN '#{@options[:year]}-01-01' AND '#{@options[:year]}-12-31'"
        elsif @options[:since]
          query = " DATE(last_modified) >= '#{@options[:since]}'"
        elsif @options[:term_start] and @options[:term_end]
          query = " DATE(last_modified) BETWEEN '#{@options[:term_start]}' AND '#{@options[:term_end]}'"
        else
          timestamp = DateTime.now - 3
          ymd = timestamp.strftime('%Y-%m-%d')
          query = " DATE(last_modified) >= '#{ymd}'"
        end
        if order_by
          query << " ORDER BY id DESC"
        end
        query
      end

      def count(options={})
        @options.merge!(options)
        @query = "SELECT count(id) FROM archived_bugs WHERE"
        @query << where_clause(order_by: false)
        @logger.debug("query: <#{@query}>")
        results = @connect.exec(@query)
        @logger.debug("fetched <#{results[0]['count']}>")
        count = results[0]["count"].to_i
        @n_max = (count / 10_000).zero? ? 1 : (count / 10_000) + 1
        count
      end

      def select_id_by_terms(term_start, term_end)
        @n_page = (count / 10_000).zero? ? 1 : count / 10_000
        select_to_file
      end

      def rest?
        @n_page < @n_max
      end

      def select
        offset = 10_000 * @n_page
        @logger.debug("offset <#{offset}>")
        query = "SELECT * FROM archived_bugs WHERE "
        query << where_clause(order_by: true)
        query << " OFFSET #{offset}"
        query << " LIMIT 10000"
        @logger.debug("query: <#{query}>")
        @n_page += 1
        @connect.exec(query)
      end
    end
  end
end
