require "fabre/udd/base"

module Fabre
  module Udd
    class Bugs < Base
      def initialize(options={})
        super
        @options = options
        @logger = options[:logger]
        @n_page = 0
      end

      def where_clause(order_by: false)
        query = ""
        if @options[:year]
          query = " DATE(last_modified) BETWEEN '#{@options[:year]}-01-01' AND '#{@options[:year]}-12-31'"
        elsif @options[:since]
          tm = Time.parse(@options[:since])
          ymd = tm.strftime("%Y-%m-%d")
          query = " DATE(last_modified) >= '#{ymd}'"
        elsif @options[:term_start] and @options[:term_end]
          query = " DATE(last_modified) BETWEEN '#{@options[:term_start]}' AND '#{@options[:term_end]}'"
        else
          timestamp = DateTime.now - 3
          ymd = timestamp.strftime('%Y-%m-%d')
          query = " DATE(last_modified) >= '#{ymd}'"
        end
        if order_by
          query << " ORDER BY id DESC"
        end
        query
      end

      def query_count
        @query = "SELECT count(id) FROM bugs WHERE"
        @query << where_clause(order_by: false)
        @logger.debug("query: <#{@query}>")
        results = @connect.exec(@query)
        @logger.debug("fetched <#{results[0]['count']}>")
        count = results[0]["count"].to_i
        count
      end

      def select_id_by_terms(term_start, term_end)
        count = query_count
        @n_page = (count / 10_000).zero? ? 1 : count / 10_000
        select_to_file
      end

      def select(options={})
        @options.merge!(options)
        count = query_count
        @n_page = (count / 10_000).zero? ? 1 : count / 10_000
        @bugs = []
        @n_page.times do |index|
          offset = 10_000 * index
          @logger.debug("offset <#{offset}>")
          query = "SELECT id FROM bugs WHERE "
          query << where_clause(order_by: true)
          query << " OFFSET #{offset}"
          @logger.debug("query: <#{query}>")
          results = @connect.exec(query)
          results.each do |record|
            @bugs << record["id"]
          end
        end
        @bugs
      end

      def execute(query)
        @connect.exec(query)
      end

      def select_maintainer_bugs(email)
        bugs = []
        query = "SELECT distinct(bugs.id) FROM bugs inner join packages on bugs.package = packages.package AND packages.maintainer_email = '#{email}' "
        query << "ORDER BY bugs.id"
        @logger.debug("query: <#{query}>")
        results = @connect.exec(query)
        results.each do |record|
          bugs << record["id"]
        end
        bugs
      end

      def select_status(email)
        count = query_count
        @n_page = (count / 10_000).zero? ? 1 : count / 10_000
        bugs = {}
        @n_page.times do |index|
          offset = 10_000 * index
          @logger.debug("offset <#{offset}>")
          query = "SELECT id,status FROM bugs WHERE "
          query << where_clause(order_by: true)
          query << " OFFSET #{offset}"
          @logger.debug("query: <#{query}>")
          results = @connect.exec(query)
          results.each do |record|
            id = record["id"]
            status = record["status"]
            bugs[id] = status
          end
        end
        bugs
      end

      def save
        @path = "default.list"
        if @options[:since]
          @path = "since#{@options[:since]}.list"
        elsif @options[:year]
          @path = "#{@options[:year]}.list"
        end
        File.open(@path, "w+") do |file|
          @bugs.each do |bug|
            file.puts(bug)
          end
        end
        @logger.info("saved: <#{@path}>")
      end
    end
  end
end
