require "fabre/udd/base"

module Fabre
  module Udd
    class Popcon < Base
      def initialize(options={})
        super
      end

      def select_by_package
        @connect.exec("SELECT * FROM popcon ORDER BY package")
      end

      def select_by_source
        @connect.exec("SELECT * FROM popcon_src ORDER BY source")
      end
    end
  end
end

