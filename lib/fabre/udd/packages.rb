require "fabre/udd/base"

module Fabre
  module Udd
    class Packages < Base
      def initialize(options={})
        super
        @options = options
        @logger = options[:logger]
      end

      def query_count
        @query = "SELECT count(package) FROM packages "
        @logger.info("query: <#{@query}>")
        results = @connect.exec(@query)
        @logger.info("fetched <#{results[0]['count']}>")
        count = results[0]["count"].to_i
        count
      end

      def select_query
        "SELECT package, source FROM packages "
      end

      def select
        count = query_count
        @n_page = (count / 10_000).zero? ? 1 : count / 10_000
        @packages = []
        @n_page.times do |index|
          offset = 10_000 * index
          @logger.info("offset <#{offset}>")
          query = select_query
          query << " OFFSET #{offset} LIMIT 10000"
          @logger.info("query <#{query}>")
          results = @connect.exec(query)
          results.each do |record|
            unless @packages.include?(record["package"])
              @packages << record["package"]
            end
            unless @packages.include?(record["source"])
              @packages << record["source"]
            end
          end
        end
        @packages.sort.uniq
      end
    end
  end
end
