require "fabre/helper"
require "fabre/builder/grn"
require "fabre/spooler"

module Fabre
  module Command
    module Sync
      class Spool
        include Fabre::Helper::Path
        include Fabre::Helper::Database

        def initialize(options={})
          @logger = options[:logger] || Logger.new(STDOUT)
          @rsync = Fabre::Spooler::Rsync.new
          @log = Fabre::Spooler::Log.new
          @bugs = []
        end

        def parse_options(argv)
          @options = {}
          parser = OptionParser.new
          parser.on("-f", "--full", "Rsync full spool directory") do |v|
            @options[:full] = v
          end
          parser.on("-i", "--input-list=FILE", "Sync spool with specified list") do |v|
            @options[:input_list] = v
          end
          parser.on("--missing-comment", "Sync spool which is missing comment") do |v|
            @options[:missing_comment] = v
          end
          parser.on("-s", "--suffix=SUFFIX", "Sync spool which contains suffix") do |v|
            @options[:suffix] = v
          end
          parser.parse!(argv)
        end

        def run(argv=[])
          @bugs = parse_options(argv)

          if ENV["FABRE_SPOOL_DIR"].nil? or ENV["FABRE_SPOOL_DIR"].empty?
            @logger.error("empty FABRE_SPOOL_DIR environment variable")
            return false
          end

          @db_h_dir = File.join(ENV["FABRE_SPOOL_DIR"], "db-h")
          unless Dir.exist?(@db_h_dir)
            @logger.error("empty db-h directory")
            return false
          end

          if @options[:full]
            @rsync.fullsync
            return true
          elsif @options[:input_list]
            sync_by_input_list
          elsif @options[:suffix]
            @rsync.suffix(@options[:suffix])
          else
            @rsync.run(@bugs)
          end
          true
        end
      end
    end
  end
end
