require "fabre/helper"

module Fabre
  module Command
    module Spool
      class Rsync
        def initialize(options={})
          @output = options[:output] || STDOUT
          @logger = options[:logger] || Logger.new(STDOUT)
          @db_h_dir = File.join(ENV["FABRE_SPOOL_DIR"], "db-h")
          @spool_dir = ENV["FABRE_SPOOL_DIR"]
          @bugs = options[:bugs]
        end

        def run(bugs=[])
          return false if bugs.empty?

          unless bugs.empty?
            @bugs = bugs
          end
          Dir.chdir(@db_h_dir) do
            Tempfile.open do |f|
              @logger.debug(files_from(@bugs))
              f.puts(files_from(@bugs))
              sync_files_from(f.path)
            end
          end
          true
        end
      end
    end
  end
end
