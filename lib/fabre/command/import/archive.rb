require "time"
require "fabre/udd"
require "logger"

require "fabre/importer/archived"

module Fabre
  module Command
    module Import
      class Archive
        def initialize(options={})
          @options = {}
          @options = options
          @output = options[:output] || $stdout
          @logger = options[:logger] || Logger.new(STDOUT)
        end

        def parse_options(values=[])
          parser = OptionParser.new

          @options = {
            file: false
          }
          parser.on("-f", "--file") do |v|
            @options[:file] = v
          end
          parser.on("-s", "--since YYYYMMDD") do |v|
            @options[:since] = v
          end
          parser.on("-y", "--year YYYY") do |v|
            @options[:year] = v
          end
          parser.on("-t", "--term TERM", "Specify term with hyphen (YYYYMMDD-YYYYMMDD)") do |v|
            @options[:term] = true
            term = v.split("-", 2)
            s = Time.parse(term[0])
            e = Time.parse(term[1])
            @options[:term_start] = Time.parse(s.strftime("%Y/%m/%d 00:00:00"))
            @options[:term_end] = Time.parse(e.strftime("%Y/%m/%d 00:00:00"))
          end
          parser.on("--log-level LEVEL") do |v|
            @options[:log_level] = case v
                                   when "info"
                                     Logger::Severity::INFO
                                   when "debug"
                                     Logger::Severity::DEBUG
                                   when "warn"
                                     Logger::Severity::WARN
                                   when "error"
                                     Logger::Severity::ERROR
                                   else
                                     Logger::Severity::INFO
                                   end
          end
          parser.parse!(values)
        end

        def run(argv=[])
          parse_options(argv)
          begin
            @importer = Fabre::Importer::Archived.new(@options)
            @importer.run
          end
          true
        end
      end
    end
  end
end
