require "optparse"
require "fabre/importer/popcon"

module Fabre
  module Command
    module Import
      class Popcon

        def initialize
          @options = {
            log_level: Logger::INFO
          }
          @logger = Logger.new(STDOUT)
        end

        def parse_options(argv)
          @parser = OptionParser.new
          @parser.on("--log-level", "Set log level") do |v|
            @options[:log_level] = case v
                                   when "debug"
                                     Logger::DEBUG
                                   when "info"
                                     Logger::INFO
                                   when "error"
                                     Logger::ERROR
                                   when "warn"
                                     Logger::WARN
                                   when "fatal"
                                     Logger::FATAL
                                   end
          end
          @parser.parse!(argv)
        end

        def run(argv=[])
          parse_options(argv)
          @logger.level = @options[:log_level]
          @options[:logger] = @logger
          importer = Fabre::Importer::Popcon.new(@options)
          importer.run
        end
      end
    end
  end
end
