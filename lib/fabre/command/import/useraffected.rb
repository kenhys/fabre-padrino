module Fabre
  module Command
    module Import
      class UserAffected

        include Fabre::Helper::Path
        include Fabre::Helper::Database

        def initialize(options={})
          @output = options[:output] || $stdout
          @options = {}
        end

        def parse_options(argv)
          parser = OptionParser.new
          parser.on("-s", "--since") do |v|
          end
          parser.on("-y=YEAR", "--year=YEAR") do |v|
            @options[:year] = v
          end
          parser.on("-u=USERNAME", "--username=USERNAME") do |v|
            @options[:username] = v
          end
          parser.parse!(argv)
        end

        def run(argv=[])
          args = parse_options(argv)
          begin
            affected = Fabre::Importer::UserAffected.new
            open_database do
              if @options[:username]
                update_by_user(@options[:username])
              elsif @options[:year]
                update_by_year(@options[:username])
              end
            end
          end
        end
      end
    end
  end
end
