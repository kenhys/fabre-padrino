require "timeout"
require "fileutils"
require "English"

require "fabre/helper"
require "fabre/spooler"
require "fabre/builder"
require "fabre/importer/grn"

module Fabre
  module Command
    module Importer
      class Grn
        include Fabre::Helper::Path
        include Fabre::Helper::Database

        def initialize
          @options = {
            grn: true,
            json: true,
            load: true,
            blocked: true,
            rebuild: true,
            sync: true
          }
          @logger = Logger.new(STDOUT)
          @rsync = Fabre::Spooler::Log.new
          @bugs = []
        end

        def parse_options(argv)
          parser = OptionParser.new
          parser.on("-j", "--[no-]json", "Generate .json files") do |v|
            @options[:parse] = v
          end
          parser.on("-g", "--[no-]grn", "Generate .grn file") do |v|
            @options[:grn] = v
          end
          parser.on("-l", "--[no-]load", "Load bugs") do |v|
            @options[:load] = v
          end
          parser.on("-b", "--blocked", "Rebuild blocked bugs") do |v|
            @options[:blocked] = v
          end
          parser.on("-r", "--rebuild", "Rebuild specified bugs") do |v|
            @options[:rebuild] = v
          end
          parser.on("-i", "--input-list=FILE", "Use list of bug number") do |v|
            @options[:input_list] = v
          end
          parser.on("-k", "--keep-file", "Do not remove auxiliary files") do |v|
            @options[:keep] = v
          end
          parser.on("-s", "--[no-]sync", "Sync with UDD") do |v|
            @options[:sync] = v
          end
          parser.on("-a", "--archived", "Import even though archived bugs") do |v|
            @options[:archived] = v
          end
          parser.on("-m", "--missing-comment", "Reload bugs which has no comment") do |v|
            @options[:missing_comment] = v
          end
          parser.on("--log-level", "Set logger debug level") do |v|
            @logger.level = case v
                            when "debug"
                              Logger::DEBUG
                            else
                              Logger::INFO
                            end
          end
          parser.parse!(argv)
        end

        def load_list
          unless File.exist?(@options[:list_path])
            puts "list doesn't exist: <#{@options[:list_path]}>"
            return
          end
          @files = []
          File.open(@options[:list_path]) do |file|
            file.readlines.each do |log|
              basename = File.basename(log.chomp, ".log")
              suffix = basename[-2..-1]
              grn_path = File.join(ENV["FABRE_DATA_DIR"],
                                   "spool",
                                   suffix,
                                   "#{basename}.grn.zst")
              if File.exist?(grn_path)
                @files << grn_path
              else
                puts "grn file doesn't exist: <#{grn_path}>"
              end
            end
          end
        end

        def add_queue_entry(path, created_at, updated_at, status)
          @queues = Groonga["ServiceQueues"]
          bug = File.basename(path, ".grn.zst")
          if @queues.key?(bug)
            puts "update queue: <#{bug}>"
            @queues[bug.to_i] = {
              updated_at: updated_at,
              loaded: status
            }
          else
            puts "add queue: #{bug}"
            @queues.add(bug.to_i,
                        created_at: created_at,
                        updated_at: updated_at,
                        loaded: status)
          end
        end

        def add_service_log(type, timestamp, message)
          @logs = Groonga["ServiceLogs"]
          puts "add service log: <#{type}> <#{message}>"
          @logs.add({
                      timestamp: timestamp,
                      type: type,
                      message: message
                    })
        end

        def load_file
          add_service_log("grnloader", Time.now, "loader started")
          @files.each do |path|
            created = Time.now
            pid = 0
            begin
              Timeout.timeout(60) do
                basename = File.basename(path, ".grn.zst")
                suffix = basename[-2..-1]
                log_dir = File.join(ENV["FABRE_DATA_DIR"],
                                    "logs",
                                    "grnloader",
                                    suffix)
                FileUtils.mkdir_p(log_dir)
                log_path = File.join(log_dir,
                                     "#{basename}.groonga.log")
                query_log = File.join(log_dir,
                                      "#{basename}.query.log")
                FileUtils.rm_f(log_path)
                FileUtils.rm_f(query_log)
                command = "zstdcat #{path} | groonga --log-path #{log_path} --query-log-path #{query_log} #{database_path}"
                IO.popen(command) do |io|
                  pid = io.pid
                end
                status = $CHILD_STATUS
                add_queue_entry(path, created, Time.now, status.exited?)
              end
            rescue Timeout::Error
              if pid.positive?
                add_queue_entry(path, created, Time.now, false)
                Process.kill('KILL', pid)
                system("groonga #{database_path} clearlock")
              end
            end
          end
          add_service_log("grnloader", Time.now, "loader finished")
        end

        def list_bugs
          bugs = []
          File.open(@options[:input_list]) do |file|
            file.readlines.each do |line|
              bugs << line.chomp!.to_i
            end
          end
          bugs
        end

        def run(argv=[])
          @bugs = parse_options(argv)
          @loader = Fabre::Importer::Grn.new(@options)
          if @options[:input_list]
            @bugs = list_bugs
          end
          p @bugs
          @loader.sync(@bugs)
          @loader.rebuild(@bugs)
          true
        end
      end
    end
  end
end
