require "time"
require "fabre/udd"
require "logger"

require "fabre/importer/affects"

module Fabre
  module Command
    module Import
      class Affects
        def initialize(options={})
          @options = {}
          @options = options
          @output = options[:output] || $stdout
          @logger = options[:logger] || Logger.new(STDOUT)
        end

        def parse_options(values=[])
          parser = OptionParser.new

          @options = {
            file: false
          }
          parser.on("-f", "--file") do |v|
            @options[:file] = v
          end
          parser.on("-s", "--since YYYYMMDD") do |v|
            @options[:since] = v
          end
          parser.on("-y", "--year YYYY") do |v|
            @options[:year] = v
          end
          parser.on("-t", "--term TERM", "Specify term with hyphen (YYYYMMDD-YYYYMMDD)") do |v|
            @options[:term] = true
            term = v.split("-", 2)
            s = Time.parse(term[0])
            e = Time.parse(term[1])
            @options[:term_start] = Time.parse(s.strftime("%Y/%m/%d 00:00:00"))
            @options[:term_end] = Time.parse(e.strftime("%Y/%m/%d 00:00:00"))
          end
          parser.parse!(values)
        end

        def run(argv=[])
          parse_options(argv)
          begin
            @options[:logger] = @logger
            @importer = Fabre::Importer::Affects.new(@options)
            @importer.run
          end
          true
        end
      end
    end
  end
end
