require "time"
require "fabre/udd"
require "logger"
require "groonga"

require "fabre/helper"
require "fabre/client/bugs"
require "fabre/client/bugs"

module Fabre
  module Command
    module Import
      class List

        include Fabre::Helper::Path
        include Fabre::Helper::Database

        def initialize(options={})
          @options = {}
          @options = options
          @output = options[:output] || $stdout
          @logger = options[:logger] || Logger.new(STDOUT)
        end

        def parse_options(values=[])
          parser = OptionParser.new

          @options = {
            file: false,
            missing: false,
            archived: false
          }
          parser.on("-f", "--file") do |v|
            @options[:file] = v
          end
          parser.on("-s", "--since YYYYMMDD") do |v|
            @options[:since] = v
          end
          parser.on("-y", "--year YYYY") do |v|
            @options[:year] = v
          end
          parser.on("-t", "--term TERM", "Specify term with hyphen (YYYYMMDD-YYYYMMDD)") do |v|
            @options[:term] = true
            term = v.split("-", 2)
            s = Time.parse(term[0])
            e = Time.parse(term[1])
            @options[:term_start] = Time.parse(s.strftime("%Y/%m/%d 00:00:00"))
            @options[:term_end] = Time.parse(e.strftime("%Y/%m/%d 00:00:00"))
          end
          parser.on("-m", "--missing", "Make list of bugs which is missing comment") do |v|
            @options[:missing] = v
          end
          parser.on("--local-rc", "Make list of rc bugs which is stored in local") do |v|
            @options[:local_rc] = v
          end
          parser.on("--[no-]archived", "Include/Exclude archived bugs") do |v|
            @options[:archived] = v
          end
          parser.on("--maintainer EMAIL", "Make list of maintainer bugs") do |v|
            @options[:maintainer] = v
          end
          parser.on("--log-level LEVEL", "Set log level") do |v|
            @options[:log_level] = case v
                                   when "info"
                                     Logger::Severity::INFO
                                   when "debug"
                                     Logger::Severity::DEBUG
                                   when "warn"
                                     Logger::Severity::WARN
                                   when "error"
                                     Logger::Severity::ERROR
                                   else
                                     Logger::Severity::INFO
                                   end
          end
          parser.parse!(values)
        end
        
        def save_missing_comment
          open_database do
            @bugs = Fabre::Bugs.new
            File.open("missing.list", "w+") do |file|
              @bugs.missing_comments.each do |record|
                file.puts(record._key)
              end
            end
          end
        end

        def run(argv=[])
          parse_options(argv)
          begin
            if @options[:missing]
              save_missing_comment
            elsif @options[:local_rc]
              open_database do
                @bugs = Fabre::Client::Bugs.new
                @records = @bugs.rc_bugs_by
                ids = @records.collect do |record| record._key end.sort
                File.open("rc.list", "w+") do |file|
                  file.puts(ids.join("\n"))
                end
              end
            elsif @options[:maintainer]
              @options[:logger] = @logger
              @bugs = Fabre::Udd::Bugs.new(@options)
              bugs = @bugs.select_maintainer_bugs(@options[:maintainer])
              unless bugs.size > 0
                @logger.warn("#{@options[:maintainer]} maintained bugs: #{bugs.size}")
                return false
              end
              @logger.info("#{@options[:maintainer]} maintained bugs: #{bugs.size}")
              File.open("maintainer.list", "w+") do |file|
                file.puts(bugs.join("\n"))
              end
            else
              @bugs = Fabre::Udd::Bugs.new(logger: @logger)
              @bugs.select(@options)
              @bugs.save
            end
          end
          true
        end
      end
    end
  end
end
