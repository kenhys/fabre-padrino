module Fabre
  module Command
    module Import
      class BugMail

        def initialize
          @options = {}
        end

        def parse_options(argv)
          parser = OptionParser.new
          parser.on("--imap-host=HOST") do |v|
            @options[:imap_host] = v
          end
          parser.on("--imap-port=PORT") do |v|
            @options[:imap_port] = v
          end
          parser.on("--imap-username=USER") do |v|
            @options[:imap_username] = v
          end
          parser.on("--imap-password=PASSWORD") do |v|
            @options[:imap_password] = v
          end
          parser.on("--dry-run") do |v|
            @options[:dry_run] = v
          end
          parser.parse!(argv)
        end

        def run(argv=[])
          parse_options(argv)
          p @options
          mailer = Fabre::Importer::BugMail.new(@options)
          mailer.run
        end
      end
    end
  end
end
