module Fabre
  module Command
    module Import
      class UserTags

        def initialize
          @options = {}
          @logger = Logger.new(STDOUT)
          @options[:logger] = @logger
        end

        def parse_options(argv)
          parser = OptionParser.new
          parser.on("-t TAG", "--tag=TAG") do |v|
            @options[:tag] = v
          end
          parser.on("-y YEAR", "--year=YEAR") do |v|
            @options[:year] = v
          end
          parser.on("-s", "--since YYYYMMDD") do |v|
            @options[:since] = v
          end
          parser.parse!(argv)
        end

        def run(argv=[])
          parse_options(argv)
          importer = Fabre::Importer::UserTags.new(@options)
          importer.run
        end
      end
    end
  end
end
