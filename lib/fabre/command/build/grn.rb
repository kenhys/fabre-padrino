require "logger"

require "fabre/helper"
require "fabre/builder/grn"
require "fabre/builder/worker"
require "fabre/spooler"
require "fabre/client/bugs"

module Fabre
  module Command
    module Build
      class Grn
        include Fabre::Helper::Path
        include Fabre::Helper::Database

        def initialize(options={})
          @options = {
            json: true,
            grn: true,
            load: true,
            keep: false,
            on_memory: true,
            sync: true,
            verbose: Logger::Severity::INFO,
            log_level: Logger::Severity::INFO,
            missing: false,
            blocked: false
          }
          @logger = Logger.new(STDOUT)
          @logger.level = Logger::Severity::INFO
          @options[:logger] = @logger
          @archived = false
          @bugs = []
        end

        def parse_options(argv)
          parser = OptionParser.new
          parser.on("-a", "--all") do |v|
            @options[:all] = v
          end
          parser.on("-f", "--force", "Force to build bugs") do |v|
            @options[:force] = v
          end
          parser.on("-g", "--[no-]grn", "Build .grn") do |v|
            @options[:grn] = v
          end
          parser.on("-i FILE", "--input-list=FILE", String, "Specify list of target bugs") do |v|
            unless File.exist?(v)
              message = "input list file is not found: <#{v}>"
              @logger.error(message)
              raise ArgumentError, message
            end
            @options[:input_list] = v
          end
          parser.on("-j", "--[no-]json", "Build .json (default: #{@options[:json]})") do |v|
            @options[:json] = v
          end
          parser.on("-k", "--[no-]keep-file", "Keep auxiliary files") do |v|
            @options[:keep] = v
          end
          parser.on("-l", "--[no-]load", "Load bugs (default: #{@options[:load]})") do |v|
            @options[:load] = v
          end
          parser.on("-m", "--missing", "Build bugs which is missing comment (default: #{@options[:missing]})") do |v|
            @options[:missing] = v
          end
          parser.on("-p", "--[no-]pipeline", "Enable on memory processing (default: #{@options[:on_memory]})") do |v|
            @options[:on_memory] = v
          end
          parser.on("-s", "--[no-]sync", "Sync with UDD (default: #{@options[:sync]})") do |v|
            @options[:sync] = v
          end
          parser.on("-v", "--verbose [LEVEL]", "Specify logging level (default: #{@options[:verbose]})") do |v|
            @options[:verbose] = v
          end
          parser.on("--log-level LEVEL", "Specify logging level (default: #{@options[:log_level]})") do |v|
            @logger.level = case v
                            when "debug"
                              Logger::Severity::DEBUG
                            when "info"
                              Logger::Severity::INFO
                            else
                              Logger::Severity::INFO
                            end
          end
          parser.parse!(argv)
        end

        def run(argv=[])
          @bugs = parse_options(argv)
          begin
            if @bugs.empty?
              if @options[:input_list]
                File.open(@options[:input_list]) do |file|
                  file.readlines.each do |line|
                    @bugs << line.chomp
                  end
                end
              elsif @options[:missing]
                open_database do
                  @bug = Fabre::Bugs.new
                  @bug.missing_comments.each do |record|
                    @bugs << record._key.to_s
                  end
                end
              end
            end
            @log = Fabre::Spooler::Log.new(@options)
            @log.sync(@bugs)
            worker = Fabre::Builder::Worker.new(@options)
            worker.run(@bugs.sort.reverse)
          rescue => e
            @logger.error(e.message)
            return false
          end
          true
        end
      end
    end
  end
end
