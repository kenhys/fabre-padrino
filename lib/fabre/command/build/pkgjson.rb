require "logger"

require "fabre/helper"
require "fabre/udd/packages"

module Fabre
  module Command
    module Build
      class PkgJson
        include Fabre::Helper::Path
        include Fabre::Helper::Database

        def initialize(options={})
          @options = {
            verbose: Logger::Severity::INFO
          }
          @logger = Logger.new(STDOUT)
          @logger.level = Logger::Severity::INFO
          @options[:logger] = @logger
        end

        def parse_options(argv)
          parser = OptionParser.new
          parser.on("-v", "--verbose [LEVEL]", "Specify logging level (default: #{@options[:verbose]})") do |v|
            @options[:verbose] = v
          end
          parser.parse!(argv)
        end

        def run(argv=[])
          parse_options(argv)
          @packages = Fabre::Udd::Packages.new(@options)
          data = []
          @packages.select.each do |name|
            data << {"package" => name}
          end
          File.open("packages.json", "w+") do |file|
            file.puts(JSON.dump(data))
          end
          true
        end
      end
    end
  end
end
