require "timeout"
require "fileutils"
require "English"

require "fabre/helper"
require "fabre/spooler"
require "fabre/builder"

module Fabre
  module Importer
    class Grn
      include Fabre::Helper::Path
      include Fabre::Helper::Database

      def initialize(options={})
        @options = options
        @rsync = Fabre::Spooler::Log.new
        @logger = options[:logger] || Logger.new(STDOUT)
        @bugs = []
      end

      def load_list
        unless File.exist?(@options[:list_path])
          puts "list doesn't exist: <#{@options[:list_path]}>"
          return
        end
        @files = []
        File.open(@options[:list_path]) do |file|
          file.readlines.each do |log|
            basename = File.basename(log.chomp, ".log")
            suffix = basename[-2..-1]
            grn_path = File.join(ENV["FABRE_DATA_DIR"],
                                 "spool",
                                 suffix,
                                 "#{basename}.grn.zst")
            if File.exist?(grn_path)
              @files << grn_path
            else
              puts "grn file doesn't exist: <#{grn_path}>"
            end
          end
        end
      end

      def add_queue_entry(path, created_at, updated_at, status)
        @queues = Groonga["ServiceQueues"]
        bug = File.basename(path, ".grn.zst")
        if @queues.key?(bug)
          puts "update queue: <#{bug}>"
          @queues[bug.to_i] = {
            updated_at: updated_at,
            loaded: status
          }
        else
          puts "add queue: #{bug}"
          @queues.add(bug.to_i,
                      created_at: created_at,
                      updated_at: updated_at,
                      loaded: status)
        end
      end

      def add_service_log(type, timestamp, message)
        @logs = Groonga["ServiceLogs"]
        puts "add service log: <#{type}> <#{message}>"
        @logs.add({
                    timestamp: timestamp,
                    type: type,
                    message: message
                  })
      end

      def load_file
        add_service_log("grnloader", Time.now, "loader started")
        @files.each do |path|
          created = Time.now
          pid = 0
          begin
            Timeout.timeout(60) do
              basename = File.basename(path, ".grn.zst")
              suffix = basename[-2..-1]
              log_dir = File.join(ENV["FABRE_DATA_DIR"],
                                  "logs",
                                  "grnloader",
                                  suffix)
              FileUtils.mkdir_p(log_dir)
              log_path = File.join(log_dir,
                                   "#{basename}.groonga.log")
              query_log = File.join(log_dir,
                                    "#{basename}.query.log")
              FileUtils.rm_f(log_path)
              FileUtils.rm_f(query_log)
              command = "zstdcat #{path} | groonga --log-path #{log_path} --query-log-path #{query_log} #{database_path}"
              IO.popen(command) do |io|
                pid = io.pid
              end
              status = $CHILD_STATUS
              add_queue_entry(path, created, Time.now, status.exited?)
            end
          rescue Timeout::Error
            if pid.positive?
              add_queue_entry(path, created, Time.now, false)
              Process.kill('KILL', pid)
              system("groonga #{database_path} clearlock")
            end
          end
        end
        add_service_log("grnloader", Time.now, "loader finished")
      end

      def list_bugs
        File.open(@options[:input_list]) do |file|
          file.readlines.each do |line|
            @bugs << line.chomp!
          end
        end
      end

      def rebuild(bugs=[])
        return unless @options[:rebuild]
        @bugs = bugs unless bugs.empty?
        open_database do
          @archived = Groonga["ArchivedBugs"]
          @bugs.each do |bug|
            options = {
              input: log_path(bug),
              load: true,
              keep: @options[:keep]
            }
            @rebuilder = Fabre::Builder::Grn.new(options)
            @rebuilder.run
          end
        end
      end

      def sync(bugs=[])
        return unless @options[:sync]
        @logger.debug("sync")
        @bugs = bugs unless bugs.empty?
        p @bugs
        @rsync.run(@bugs)
      end
    end
  end
end
