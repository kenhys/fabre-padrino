require "logger"

require_relative "../helper/path"
require_relative "../helper/database"

require "fabre/udd/bugs"

module Fabre
  module Importer
    class Affects
      include Fabre::Helper::Path
      include Fabre::Helper::Database

      def initialize(options={})
        @options = {}
        @options = options
        @logger = options[:logger] || Logger.new(STDOUT)
      end

      def fields_data(record)
        data = []
        %w[key affects_unstable affects_testing affects_stable].each do |key|
          case key
          when "affects_unstable", "affects_testing", "affects_stable"
            affect = record[key] == "t"
            data << %("#{key}": #{affect})
          else
            data << %("#{key}": #{record['id']})
          end
        end
        data
      end

      def load
        @bugs = Groonga["Bugs"]
        @n_page.times do |index|
          offset = 10_000 * index
          @query = "SELECT id, affects_unstable, affects_testing, affects_stable FROM bugs WHERE"
          @query << @db.where_clause(order_by: false)
          @query << " OFFSET #{offset}"
          @query << " LIMIT 10000"
          @logger.debug("query: <#{@query}>")
          results = @db.execute(@query)
          offset = 0
          @logger.debug("fetched <#{results.count}> offset: <#{offset}>")
          results.each do |record|
            bug = record["id"].to_i
            next unless @bugs.key?(bug)
            %w[affects_unstable affects_testing affects_stable].each do |column|
              if record[column] == "t"
                @bugs[bug][column] = true
                @logger.debug("#{bug} #{column}: true")
              end
            end
          end
        end
      end

      def run(values=[])
        @db = Fabre::Udd::Bugs.new(@options)
        open_database do
          count = @db.query_count
          @n_page = count.zero? ? 1 : (count / 10_000) + 1
          load
        end
      end
    end
  end
end
