module Fabre
  module Importer
    class UserAffected

      def initialize(options={})
        @options = {}
        @logger = options[:logger] || Logger.new(STDOUT)
      end

      def update_by_user(username)
        @users = Fabre::Client::Users.new
        @output.puts("updating affected start: <#{username}>")
        @user = @users.find_by_username(username)
        pkgs = @user["installed"].collect(&:_key)
        @output.puts("installed packages count: <#{pkgs.size}>")
        @bugs = Fabre::Client::Bugs.new
        @records = @users.update_affected(username, pkgs)
        @output.puts("updating affected done: <#{username}>")
      end

      def update_by_year(year)
        @users = Fabre::Client::Users.new
        targets = @users.find_by_updated_year(@options[:year])
        targets.each do |user|
          update_by_user(user._key)
        end
      end
    end
  end
end
