require_relative "../helper/path"
require_relative "../helper/database"
require "fabre/udd/usertags"

module Fabre
  module Importer
    class UserTags
      include Helper::Path
      include Helper::Database

      def initialize(options={})
        @logger = options[:logger] || $stdout
        @options = options
      end

      def run
        usertags = Fabre::Udd::UserTags.new(@options)
        bugs = usertags.select(@options)
        @logger.debug("target bugs count: #{bugs.size}")
        begin
          open_database do
            @bugs = Groonga["Bugs"]
            bugs.each do |id, tags|
              if @bugs.key?(id)
                if @bugs[id].tag
                  current_tags = @bugs[id].tag.collect { |record| record._key }
                  if @options[:tag] == "all"
                    delta = current_tags - tags
                    @bugs[id].tag = (current_tags + tags - ["all"]).uniq
                    @logger.debug("#{id}: #{current_tags} => #{(current_tags + tags).uniq}")
                  else
                    @bugs[id].tag = (current_tags + @options[:tag] - ["all"]).uniq
                    @logger.debug("#{id}: #{current_tags} => #{(current_tags + @options[:tag]).uniq}")
                  end
                else
                  @bugs[id].tag = tags
                  @logger.debug("#{id}: [] => => [#{tag}")
                end
              end
            end
          end
        rescue => e
          p e
        end
        true
      end
    end
  end
end
