require_relative "../helper"

module Fabre
  class PackageSourceImporter
    include Helper::Path
    include Helper::Database

    def initialize(options={})
      @output = options[:output] || $stdout
      @options = {}
      @from = "FROM packages LEFT JOIN descriptions ON packages.package = descriptions.package "
      @where = <<-EOS
WHERE packages.distribution = 'debian' and packages.release = 'sid' and packages.component = 'main' and packages.architecture = 'amd64' and language = 'en'
EOS
    end

    def select_to_load
      open_database do
        @packages = Groonga["Packages"]
        @reporters = Groonga["Reporters"]
        sources = {}
        puts @options[:page]
        @options[:page].times do |n|
          offset = n * 10_000
          @query = "SELECT packages.source, packages.maintainer, packages.maintainer_email, packages.maintainer_name, descriptions.long_description "
          @query << @from
          @query << @where
          @query << "ORDER BY source LIMIT 10000 "
          @query << "OFFSET #{offset}"
          results = @connect.exec(@query)
          now = Time.now
          results.each do |record|
            key = "src:%s" % record["source"]
            unless sources[key]
              @output.puts("key: <#{key}> maintainer: #{record['maintainer'].toutf8}>")
              params = {
                maintainer: record["maintainer"].toutf8,
              }
              if record["long_description"]
                params[:long_description] = record["long_description"].toutf8
              end
              if @packages.key?(key)
                params[:updated_at] = now
              else
                params[:created_at] = now
                params[:updated_at] = now
              end
              @packages[key] = params
              sources[key] = true
            end
            
            maintainer = record["maintainer"].toutf8
            params = {
              email: record["maintainer_email"],
              fullname: record["maintainer_name"].toutf8
            }
            if @reporters.key?(maintainer)
              @reporters[maintainer] = params
            else
              @reporters.add(maintainer, params)
            end
          end
        end
      end
    end

    def select_count
      @query = "SELECT count(*) "
      @query << @from
      @query << @where
      puts "fetch from packages: <#{@query}>"
      results = @connect.exec(@query)
      count = results[0]["count"].to_i
      if count < 10_000
        @options[:page] = 1
      else
        @options[:page] = (count / 10_000)
      end
    end

    def run
      begin
        @connect = PG.connect(host: "udd-mirror.debian.net", user: "udd-mirror", password: "udd-mirror", dbname: "udd", port: "5432")
        select_count
        select_to_load
        @output.puts "fetch packages"
      ensure
        @connect.finish
      end
    end
  end
end
