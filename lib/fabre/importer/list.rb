#require "active_record"

module Fabre
  module Importer
    class List
      def initialize(options={})
        @options = {}
        @options = options
        @output = options[:output] || $stdout
      end

      def select_to_file
        if @options[:since]
          path = "since#{@options[:since]}.list"
        elsif @options[:year]
          path = "#{@options[:year]}.list"
        end
        File.open(path, "w+") do |file|
          @n_page.times do |index|
            offset = 10_000 * index
            query = "SELECT id FROM bugs WHERE "
            query << where_clause(order_by: true)
            query << " OFFSET #{offset}"
            puts "query: <#{query}>"
            results = @connect.exec(query)
            results.each do |record|
              file.puts(record["id"])
            end
          end
        end
      end

      def where_clause(order_by: false)
        query = ""
        if @options[:year]
          query = " DATE(last_modified) BETWEEN '#{@options[:year]}-01-01' AND '#{@options[:year]}-12-31'"
        elsif @options[:since]
          query = " DATE(last_modified) >= '#{@options[:since]}'"
        else
          timestamp = DateTime.now - 3
          ymd = timestamp.strftime('%Y-%m-%d')
          query = " DATE(last_modified) >= '#{ymd}'"
        end
        if order_by
          query << " ORDER BY id"
        end
        query
      end

      def query_count
        @query = "SELECT count(id) FROM bugs WHERE"
        @query << where_clause(order_by: false)
        puts "query: <#{@query}>"
        results = @connect.exec(@query)
        puts "fetched <#{results[0]['count']}>"
        count = results[0]["count"].to_i
        count
      end

      def run(values=[])
        begin
=begin
          @connect = PG.connect(host: "udd-mirror.debian.net", user: "udd-mirror", password: "udd-mirror", dbname: "udd", port: "5432")
          count = query_count
          @n_page = (count / 10_000).zero? ? 1 : count / 10_000
          select_to_file
=end
          @bugs = Fabre::Udd::Bugs.new
          p @bugs.select("id").where(last_modified: [@options[:term_start]..@options[:term_end]])
        ensure
          @connect.finish
        end
      end
    end
  end
end
