require "kconv"
require "fabre/helper"

module Fabre
  module Importer
    class Archived
      include Fabre::Helper::Path
      include Fabre::Helper::Database

      def initialize(options={})
        @options = options
        @logger = options[:logger] || Logger.new(STDOUT)
        if @options[:log_level]
          @logger.level = @options[:log_level]
        else
          @logger.level = Logger::Severity::INFO
        end
      end

      def fields_data(record)
        data = []
        %w[key package severity originator subject created_at updated_at archived].each do |key|
          case key
          when "subject"
            data << %("#{key}": ") + record['title'].toutf8.gsub(/\"/, '\"') + %(")
          when "key"
            data << %("_key": #{record['id']})
          when "package"
            data << %("#{key}": "#{record['source']}")
          when "originator"
            data << %("#{key}": ") + record['submitter'].toutf8.gsub(/\"/, '\"') + %(")
          when "created_at"
            data << %("#{key}": "#{record['arrival']}")
          when "updated_at"
            data << %("#{key}": "#{record['last_modified']}")
          when "archived"
            data << %("#{key}": 1)
          else
            data << %("#{key}": "#{record[key]}")
          end
        end
        data
      end
      
      def copy_column(id, column, options={})
        if @bugs[id][column]
          if options[:vector]
            keys = @bugs[id][column].collect(&:_key).compact
            unless keys.empty?
              @archived_bugs[id][column] = keys
            end
          else
            @archived_bugs[id][column] = @bugs[id][column]
          end
        end
      end

      def copy_comment(id)
        @comments = Groonga["Comments"]
        @archived_comments = Groonga["Comments"]
        records = @comments.select do |record|
          record.bug._key == id
        end
        ids = records.collect(&:_key)
        records.each do |record|
          data = {
            bug: id,
            comment_number: record["comment_number"],
            subject: record["subject"],
            date: record["date"],
            content: record["content"],
            markup: record["markup"]
          }
          if record["from"]
            data[:from] = record["from"]
          end
          if record["to"]
            data[:to] = record["to"].collect do |r|
              r&._key
            end.compact
          end
          if @archived_comments.key?(record._key)
            @logger.debug("update ArchivedComments: <#{record._key}>")
            @archived_comments[record._key] = data
          else
            @logger.debug("add ArchivedComments: <#{record._key}>")
            @archived_comments.add(record._key, data)
          end
        end
        ids.each do |id|
          @logger.debug("delete comment from Comments: <#{id}>")
          @comments.delete(id)
        end
      end

      def blocked_bugs
        block_list = []
        block_2012 = [
          668863
        ]
        block_2015 = [
          800959,
          802569
        ]
        block_2016 = [
          811265,
          823191,
          823193,
          823194,
          824675
        ]
        block_2017 = [
          825349,
          833631,
          833669,
          837508
        ]
        block_list.concat(block_2012, block_2015, block_2016, block_2017)
      end

      def migrate(record)
        bug = record["id"].to_i
        unless @bugs.key?(bug)
          return
        end
        data = {}
        %w[package severity originator subject created_at updated_at archived].each do |key|
          data[key] = record[key]
        end
        if @bugs.key?(bug)
          @bugs[bug] = data
        end
        data.delete("archived")
        @bugs.select("blocked_by @ #{record['id']}", :syntax => :script).records.each do |bug|
          ids = bug["blocked_by"].collect do |entry|
            entry._key
          end
          @logger.debug("#{bug._key}.blocked_by: removed <#{record['id']}>")
          ids.delete(record["id"])
          bug["blocked_by"] = ids
        end
        @logger.debug("check ArchivedBugs: <#{bug}>")
        if @archived_bugs.key?(bug)
          @archived_bugs[bug] = data
          @logger.debug("update ArchivedBugs: <#{bug}: #{data}>")
        else
          @archived_bugs.add(bug, data)
          @logger.debug("insert ArchivedBugs: <#{bug}: #{data}>")
        end
        if @bugs.key?(bug)
          @logger.debug("migrate from Bugs to ArchivedBugs: <#{bug}>")
          copy_comment(bug)
          copy_column(bug, "comment", vector: true)
          copy_column(bug, "category", vector: true)
          copy_column(bug, "fixed_versions", vector: true)
          copy_column(bug, "done_by")
          copy_column(bug, "blocked_by", vector: true)
          copy_column(bug, "tag", vector: true)
          copy_column(bug, "forwarded")
          @bugs.delete(bug)
          @logger.debug("removed Bugs[#{bug}]")
        end
        @logger.debug("done Bugs[#{bug}]")
      end

      def import(record)
        data = {}
        bug = record["id"].to_i
        %w[id, source severity submitter title arrival last_modified].each do |key|
          case key
          when "source"
            data["package"] = "src:#{record[key]}"
          when "submitter"
            data["originator"] = record[key].toutf8
          when "title"
            data["subject"] = record[key].toutf8
          when "arrival"
            data["created_at"] = record[key]
          when "last_modified"
            data["updated_at"] = record[key]
          end
        end
        begin
          if @archived_bugs.key?(bug)
            @archived_bugs[bug] = data
            @logger.debug("update ArchivedBugs: <#{bug}: #{data}>")
          else
            @archived_bugs.add(bug, data)
            @logger.debug("insert ArchivedBugs: <#{bug}: #{data}>")
          end
        rescue
          @logger.warn("failed to import ArchivedBugs: <#{bug}: #{data}>")
        end
      end

      def run
        @udd = Fabre::Udd::ArchivedBugs.new
        open_database do
          @archived_bugs = Groonga["ArchivedBugs"]
          @bugs = Groonga["Bugs"]
          @udd.count(@options)
          while @udd.rest? do
            @udd.select.each do |record|
              if @bugs.key?(record["id"])
                migrate(record)
              else
                import(record)
              end
            end
          end
        end
      end
    end
  end
end
