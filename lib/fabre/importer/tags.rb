require_relative "../helper/path"
require_relative "../helper/database"
require "fabre/udd/tags"

module Fabre
  module Importer
    class Tags
      include Helper::Path
      include Helper::Database

      def initialize(options={})
        @logger = options[:logger] || $stdout
        @options = options
      end

      def run
        udd_tags = Fabre::Udd::Tags.new(@options)
        bugs = udd_tags.select(@options)
        @logger.debug("target bugs count: #{bugs.size}")
        begin
          open_database do
            @bugs = Groonga["Bugs"]
            bugs.each do |id, tags|
              if @bugs.key?(id)
                if @bugs[id].tag
                  current_tags = @bugs[id].tag.collect { |record| record._key }
                  @bugs[id].tag = (current_tags + tags).uniq
                  if current_tags.size == (current_tags + tags).uniq.size
                    @logger.debug("#{id}: #{current_tags}")
                  else
                    @logger.debug("#{id}: #{current_tags} => #{(current_tags + tags).uniq}")
                  end
                else
                  @bugs[id].tag = tags
                  @logger.debug("#{id}: [] => => [#{tag}")
                end
              end
            end
          end
        rescue => e
          p e
        end
        true
      end
    end
  end
end
