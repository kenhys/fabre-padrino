require "net/imap"
require "kconv"
require "mail"

require "fabre/parser/controlmail"
require "fabre/helper/path"
require "fabre/helper/database"
require "fabre/parser/bugmail"
require "fabre/builder"

module Fabre
  module Importer
    class BugMail
      include Fabre::Helper::Path
      include Fabre::Helper::Database

      def initialize(options={})
        @options = options
        p options
        @logger = options[:logger] || Logger.new(STDOUT)
      end

      def select
        @imap = Net::IMAP.new(@options[:imap_host], @options[:imap_port], true)
        @imap.login(@options[:imap_username], @options[:imap_password])
        @imap.select("INBOX")
        @ids = @imap.search(["ALL"])
      end

      def ignored_bugs(bugs)
        ignored = []
        open_database do
          archived = Groonga["ArchivedBugs"]
          bugs.each do |bug|
            if archived.key?(bug)
              ignored << bug
            end
          end
        end
        ignored
      end

      def run(argv=[])
        messages = []
        begin
          bugs = []
          select
          @logger.debug("check messages: <#{@ids.size}>")
          @imap.fetch(@ids, "RFC822").each_with_index do |mail, index|
            m = Fabre::Parser::BugMail.new
            m.parse(mail.attr["RFC822"])
            if m.bug?
              messages << @ids[index]
              bugs << m.bug
              @logger.info("[#{index}] changed bugs: <#{m.bug}>")
            elsif m.command_mail?
              messages << @ids[index]
              @logger.info("[#{index}] changed bugs: <#{m.changed_bugs}>")
              bugs.concat(m.changed_bugs)
            else
              @logger.warn("[#{index}] unknown mail: <#{index}>")
            end
          end
          @logger.info("number of spool bugs: <#{bugs.uniq.size}>")
          options = {
            logger: @logger
          }
          rsync = Fabre::Spooler::Rsync.new
          rsync.sync_bugs(bugs.uniq)
          ignored = ignored_bugs(bugs.uniq)
          bugs.each do |bug|
            if ignored.include?(bug)
              @logger.info("skipping archived bug: <#{bug}>")
              next
            end
            options = {
              logger: @logger,
              input: log_path(bug)
            }
            builder = Fabre::Builder::Grn.new(options)
            builder.run
          end
          messages.each do |message_id|
            unless @options[:dry_run]
              @imap.copy(message_id, "Trash")
              @imap.store(message_id, "+FLAGS", [:deleted])
            end
          end
          @logger.info("number of delete messages: <#{messages.size}>")
        ensure
          unless @options[:dry_run]
            @imap.expunge
            @imap.logout
          end
        end
      end
    end
  end
end
