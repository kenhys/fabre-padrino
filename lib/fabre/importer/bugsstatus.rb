require_relative "../helper/path"
require_relative "../helper/database"
require "fabre/udd/bugs"

module Fabre
  module Importer
    class BugsStatus
      include Helper::Path
      include Helper::Database

      def initialize(options={})
        @logger = options[:logger] || $stdout
        @options = options
      end

      def run
        bugs = Fabre::Udd::Bugs.new(@options)
        stats = bugs.select_status(@options)
        @logger.debug("target bugs count: #{stats.size}")
        begin
          open_database do
            @bugs = Groonga["Bugs"]
            stats.each do |id, status|
              if @bugs.key?(id)
                @logger.debug("#{id}: #{status}")
                @bugs[id].status = status
              end
            end
          end
        rescue => e
          p e
        end
        true
      end
    end
  end
end
