require_relative "../helper/path"
require_relative "../helper/database"

require "fabre/udd/popcon"

module Fabre
  module Importer
    class Popcon
      include Helper::Path
      include Helper::Database

      def initialize(options={})
        @logger = options[:logger] || Logger.new(STDOUT)
      end

      def run
        begin
          @popcon = Fabre::Udd::Popcon.new
          open_database do
            @packages = Groonga["Packages"]
            pkgs = @popcon.select_by_package
            @logger.info("popcon packages: <#{pkgs.ntuples}>")
            pkgs.each do |record|
              next if record["package"] == "_submissions"

              @logger.debug("package: <#{record['package']}> recent: <#{record['recent']}>")
              @packages[record["package"]] = {
                popcon: record["recent"]
              }
            end
            pkgs = @popcon.select_by_source
            @logger.info("popcon source packages: <#{pkgs.ntuples}>")
            pkgs.each do |record|
              data = {
                "popcon": record["recent"].to_i
              }
              @packages["src:" + record["source"]] = data
              @logger.debug("package: <#{record['source']}> recent: <#{record['recent']}>")
            end
          end
        ensure
          @popcon.finish
          @logger.info("popcon import has finished")
        end
        true
      end
    end
  end
end
