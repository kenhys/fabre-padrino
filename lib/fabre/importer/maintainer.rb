require_relative "../helper"

module Fabre
  class PackageMaintainerImporter
    include Helper::Path
    include Helper::Database

    def initialize(options={})
      @output = options[:output] || $stdout
      @options = {}
    end

    def select_to_load
      Groonga::Context.default_options = { encoding: :utf8 }
      open_database do
        @reporters = Groonga["Reporters"]
        @options[:page].times do |n|
          offset = n * 10_000
          @query = "SELECT distinct(maintainer), maintainer_name, maintainer_email FROM packages "
          @query << "WHERE distribution = 'debian' and release = 'sid' and component = 'main' "
          @query << "ORDER BY maintainer LIMIT 10000 "
          @query << "OFFSET #{offset}"
          results = @connect.exec(@query)
          results.each do |record|
            key = record["maintainer"].toutf8
            @output.puts("key: <#{key}> name: #{record['maintainer_name'].toutf8} email: <#{record['maintainer_email'].toutf8}>")
            @reporters[key] = {
              email: record["maintainer_email"].toutf8,
              fullname: record["maintainer_name"].toutf8
            }
          end
        end
      end
    end

    def select_count
      @query = "SELECT distinct count(maintainer) FROM packages "
      @query << "WHERE distribution = 'debian' AND release = 'sid' AND component = 'main'"
      puts "fetch from packages: <#{@query}>"
      results = @connect.exec(@query)
      count = results[0]["count"].to_i
      if count < 10_000
        @options[:page] = 1
      else
        @options[:page] = (count / 10_000)
      end
    end

    def run
      begin
        @connect = PG.connect(host: "udd-mirror.debian.net", user: "udd-mirror", password: "udd-mirror", dbname: "udd", port: "5432")
        select_count
        select_to_load
        @output.puts "fetch packages"
      ensure
        @connect.finish
      end
    end
  end
end
