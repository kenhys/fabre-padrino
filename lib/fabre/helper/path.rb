module Fabre
  module Helper
    module Path
      
      LOG_SUFFIX = ".log"
      JSON_SUFFIX = ".json.zst"
      GRN_SUFFIX = ".grn.zst"
      SUMMARY_SUFFIX = ".summary"

      def bug_number(path)
        basename = ""
        if path.end_with?(LOG_SUFFIX)
          basename = File.basename(path, LOG_SUFFIX)
        end
        if path.end_with?(JSON_SUFFIX)
          basename = File.basename(path, JSON_SUFFIX)
        end
        if path.end_with?(GRN_SUFFIX)
          basename = File.basename(path, GRN_SUFFIX)
        end
        return basename
      end

      def groonga_log_path(bug)
        File.join(ENV["FABRE_DATA_DIR"],
                  "logs",
                  "grnloader",
                  bug.to_s[-2..-1],
                  "#{bug}.groonga.log")
      end

      def groonga_query_log_path(bug)
        File.join(ENV["FABRE_DATA_DIR"],
                  "logs",
                  "grnloader",
                  bug.to_s[-2..-1],
                  "#{bug}.query.log")
      end

      def log_path(bug)
        path = File.join(ENV["FABRE_SPOOL_DIR"],
                         "db-h",
                         bug.to_s[-2..-1],
                         bug.to_s + LOG_SUFFIX)
      end

      def summary_path(bug)
        path = File.join(ENV["FABRE_SPOOL_DIR"],
                         "db-h",
                         bug[-2..-1],
                         bug + SUMMARY_SUFFIX)
      end

      def to_log_path(path)
        if path.end_with?(LOG_SUFFIX)
          unless File.exist?(path)
            bug = File.basename(path, LOG_SUFFIX)
            path = File.join(ENV["FABRE_SPOOL_DIR"],
                             "db-h",
                             bug[-2..-1],
                             bug + LOG_SUFFIX)
          end
        else
          suffix = ""
          suffix = JSON_SUFFIX if path.end_with?(JSON_SUFFIX)
          suffix = GRN_SUFFIX if path.end_with?(GRN_SUFFIX)
          basename = File.basename(path, suffix)
          path = File.join(ENV["FABRE_SPOOL_DIR"],
                           "db-h",
                           basename[-2..-1],
                           basename + LOG_SUFFIX)
        end
        path
      end

      def json_path(bug)
        File.join(ENV["FABRE_DATA_DIR"],
                  "spool",
                  bug.to_s[-2..-1],
                  "#{bug}.json")
      end

      def json_zst_path(bug)
        json_path(bug) + ".zst"
      end

      def grn_path(bug)
        File.join(ENV["FABRE_DATA_DIR"],
                  "spool",
                  bug.to_s[-2..-1],
                  "#{bug}.grn")
      end

      def grn_zst_path(bug)
        grn_path(bug) + ".zst"
      end

      def database_path
        if ENV["DATABASE_URL"]
          File.join(File.dirname(__FILE__),
                    "../../..",
                    ENV["DATABASE_URL"])
        else
          File.join(File.dirname(__FILE__),
                    "../../..",
                    "data/db/fabre.db")
        end
      end
    end
  end
end
