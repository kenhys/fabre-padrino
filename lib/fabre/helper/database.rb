require "groonga"
require "fabre/helper/path"

module Fabre
  module Helper
    module Database

      def configure_groonga_logger
        if ENV["RACK_ENV"] == "development"
          log_dir = File.join(
            File.dirname(__FILE__),
            "../../..",
            "data",
            "logs"
          )
          Groonga::Logger.path = File.join(log_dir, "rgroonga.log")
          Groonga::Logger.max_level = :debug
          Groonga::QueryLogger.path = File.join(log_dir, "rquery.log")
        end
      end

      def open_database
        begin
          Groonga::Context.default_options = { encoding: :utf8 }
          configure_groonga_logger
          @database ||= Groonga::Database.open(File.expand_path(database_path))
          yield
        ensure
          if @database
            @database.close
            @database = nil
          end
        end
      end
    end
  end  
end
