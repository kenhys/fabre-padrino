require "mail"
require "kconv"
require "zstd-ruby"
require "pg"

require "fabre/client/archived"
require "fabre/loader/grn"
require "fabre/helper"
require "fabre/builder/json"
require "fabre/builder/grn"

module Fabre
  module Converter
    class Collector
      include Fabre::Helper::Path

      def initialize(options={})
        @options = {}
        @options[:input_list] = options[:input_list]
      end

      def log_files
        files = []
        if @options[:input_list]
          File.open(@options[:input_list]) do |file|
            file.readlines.each do |line|
              files << log_path(line.chomp)
            end
          end
        else
          Dir.glob("#{ENV['FABRE_SPOOL_DIR']}/**/*.log") do |log|
            bug = File.basename(log, ".log")
            if not File.exist?(json_zst_path(bug)) or
              not File.exist?(grn_zst_path(bug))
              files << log
            end
          end
        end
        files.sort
      end

      def json_files
        files = []
        if @options[:all]
          @connect = PG.connect(host: "udd-mirror.debian.net", user: "udd-mirror", password: "udd-mirror", dbname: "udd", port: "5432")
          results = @connect.exec("SELECT count(*) FROM bugs")
          count = results[0]["count"].to_i
          page = count < 10_000 ? 1 : (count / 10_000)
          page.times do |n|
            offset = n * 10_000
            results = @connect.exec("SELECT id FROM bugs ORDER BY id LIMIT 10000 OFFSET #{offset}")
            results.each do |record|
              suffix = record["id"][-2..-1]
              json_path = File.join(ENV["FABRE_DATA_DIR"],
                                    "spool",
                                    suffix,
                                    "#{record['id']}.json.zst")
              files << json_path
            end
          end
          @connect.finish
        else
          Dir.glob("#{ENV['FABRE_DATA_DIR']}/**/*.json.zst") do |path|
            grn_path = path.sub(/json/, "grn")
            next if File.exist?(grn_path)

            files << path
          end
        end
        files.sort
      end
    end
  end
end
