require "tempfile"

module Fabre
  module Spooler
    class Rsync
      def initialize
        @db_h_dir = File.join(ENV["FABRE_SPOOL_DIR"], "db-h")
        @logger = Logger.new(STDOUT)
      end

      def files_from(bugs)
        data = ""
        bugs.sort.each do |bug|
          data << "#{bug.to_s[-2..-1]}/#{bug}.log\n"
          data << "#{bug.to_s[-2..-1]}/#{bug}.summary\n"
        end
        @logger.debug("generate list of files for rsync")
        data
      end

      def sync_bugs(bugs)
        status = false
        Dir.chdir(@db_h_dir) do
          Tempfile.open do |f|
            list = files_from(bugs)
            @logger.debug(list)
            f.puts(list)
            f.rewind
            command = "rsync #{default_options} --no-implied-dirs --files-from=#{f.path} #{remote_spool}/ #{@db_h_dir}/"
            @logger.debug("execute: <#{command}>")
            `#{command}`
            status = $?.success?
          end
        end
        status
      end

      def remote_spool
        "rsync://bugs-mirror.debian.org/bts-spool-db"
      end

      def default_options
        "-avz --delete"
      end

      def exclude_options
        "--exclude='*.status' --exclude='*.report'"
      end

      def fullsync
        @logger.info("sync: <#{@spool_dir}/{00..99}>")
        100.times.each do |num|
          if num < 10
            sync_suffix("0#{num}")
          else
            sync_suffix(num)
          end
        end
      end

      def sync_suffix(suffix)
        command = "rsync #{default_options} #{exclude_options}  #{suffix} #{@spool_dir}/#{suffix}"
        @logger.info("sync: <#{@spool_dir}/#{suffix}>")
        @logger.debug("execute: <#{command}>")
        `#{command}`
      end

      def run(bugs)
        sync_bugs(bugs)
      end
    end
  end
end
