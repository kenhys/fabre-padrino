require "logger"

require "fabre/spooler/rsync"

module Fabre
  module Spooler
    class Log
      def initialize(options={})
        @options = options
        @logger = options[:logger] || Logger.new(STDOUT)
        @rsync = Fabre::Spooler::Rsync.new
        @bugs = []
      end

      def sync_blocked_by(bug)
        unless @options[:blocked]
          @logger.debug("no blocked option")
          return
        end

        File.open(json_zst_path(bug)) do |file|
          json = JSON.parse(Zstd.decompress(file.read))
          bugs = json["bug"]["blockedby"].split
          @rsync.run(bugs)
          options = {
            input: log_path(bug),
            bug_number: bug,
            load: true,
            logger: @logger
          }
          pipeline = Fabre::Converter::GrnIOPipeline.new(options)
          pipeline.run
        end
      end

      def sync_by_input_list
        @logger.info("collecting bugs from <#{@options[:input_list]}>")
        File.open(@options[:input_list]) do |file|
          file.readlines.each do |line|
            @bugs << line.chomp
          end
        end
        p @bugs
        @rsync.sync_bugs(@bugs)
      end

      def sync(bugs)
        @rsync.sync_bugs(bugs)
      end

      def run(bugs)
        @rsync.sync_bugs(bugs)
      end

      def sync_by_missing_comment
        open_database do
          table = Groonga["Bugs"]
          records = table.select do |record|
            record._key
          end
          ids = []
          records.each do |record|
            ids << record._key.to_s if record.comment.size == 0
          end
          bugs = ids.sort.reverse
          @logger.debug("the number of missing comment: <#{bugs.size}>")
          archived = Groonga["ArchivedBugs"]
          @bugs = ids.collect do |bug|
            archived.key?(bug) ? nil : bug
          end.compact
          @logger.debug("without ArchivedBugs bugs: <#{@bug.size}>")
        end
      end
    end
  end
end
