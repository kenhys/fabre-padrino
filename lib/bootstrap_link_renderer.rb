#
# Copyright 2020 by Kentaro Hayashi <kenhys@gmail.com>
#
# This class is implemented based on https://gist.github.com/phildionne/5785087
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


module WillPaginate
  module ViewHelpers

    # WillPaginate link renderer for Twitter Bootstrap
    class BoostrapLinkRenderer < WillPaginate::Sinatra::LinkRenderer
      protected

      def page_number(page)
        unless page == current_page
          tag(:li, link(page, page, :rel => rel_value(page), :class => "page-link"))
        else
          tag(:li, link(page, '#', class: "page-link"), :class => 'active page-item')
        end
      end

      def next_page
        num = @collection.current_page < total_pages && @collection.current_page + 1
        previous_or_next_page(num, @options[:next_label], 'next')
      end

      def previous_page
        num = @collection.current_page > 1 && @collection.current_page - 1
        previous_or_next_page(num, @options[:previous_label], 'previous')
      end

      def previous_or_next_page(page, text, classname)
        if page
          tag(:li, link(text, page, class: "page-link"), :class => classname + " page-item")
        else
          tag(:li, link(text, '#', class: "page-link"), :class => classname + ' disabled page-item')
        end
      end

      def html_container(html)
        tag(:nav, tag(:ul, html, container_attributes))
      end

    end
  end
end
