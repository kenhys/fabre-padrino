#!/bin/bash

#
# Copyright 2020 by Kentaro Hayashi <kenhys@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

if [ -z "$FABRE_DATA_DIR" ]; then
    echo "No such a FABRE_DATA_DIR: <$FABRE_DATA_DIR>"
    exit 1
fi

SCHEMA=$(dirname $0)/../db/schema.grn
MASTER=$(dirname $0)/../db/master.grn
DB_DIR=$FABRE_DATA_DIR/db

echo $DB_DIR
rm -fr $DB_DIR/*
mkdir -p $DB_DIR
groonga -n $DB_DIR/fabre.db < $SCHEMA
groonga $DB_DIR/fabre.db < $MASTER
