#!/usr/bin/ruby
#
# Copyright 2020 by Kentaro Hayashi <kenhys@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require "optparse"
require "open-uri"
require "openssl"
require "json"
require "mime/types"
require "groonga"
require "filemagic"
require "fileutils"

options = {
  load: false,
  start: 1,
  end: 1
}

parser = OptionParser.new
parser.on("-l", "--load") do |v|
  options[:load] = v
end
parser.on("-f", "--file FILE") do |v|
  options[:file] = v
end
parser.on("-s", "--start PAGE") do |v|
  options[:start] = v.to_i
end
parser.on("-e", "--end PAGE") do |v|
  options[:end] = v.to_i
end
parser.on("-p", "--page PAGE") do |v|
  options[:start].delete
  options[:end].delete
  options[:page] = v.to_i
end
parser.parse!(ARGV)

avatar = Avatar.new(options)
avatar.run
