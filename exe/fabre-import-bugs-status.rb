#!/usr/bin/ruby

#
# Copyright 2021 by Kentaro Hayashi <kenhys@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require "groonga"
require "optparse"

$LOAD_PATH.unshift(File.join(__dir__, '..', 'lib'))
require "fabre/importer/bugsstatus"
require "fabre/command/import/bugsstatus"

bugs_status = Fabre::Command::Import::BugsStatus.new
exit(bugs_status.run(ARGV))
