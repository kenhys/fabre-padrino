#!/bin/bash

#
# Copyright 2020 by Kentaro Hayashi <kenhys@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

echo $0
echo $(dirname $(realpath $0))/../db/

if [ -z "$FABRE_SPOOL_DIR" ]; then
    echo "ERROR: empty FABRE_SPOOL_DIR environment variable"
    exit 1
fi

if [ -z "$FABRE_DATA_DIR" ]; then
    echo "ERROR: empty FABRE_DATA_DIR environment variable"
    exit 1
fi

DB_H_DIR=$FABRE_SPOOL_DIR/db-h
if [ ! -d "$DB_H_DIR" ]; then
    echo "ERROR: no such <PATH_TO_SPOOL>/db-h directory: <${DB_H_DIR}>"
    exit 1
fi

if [ ! -d "$FABRE_DATA_DIR" ]; then
    echo "ERROR: no such <PATH_TO_DATA> directory: <${FABRE_DATA_DIR}>"
    exit 1
fi

cd $DB_H_DIR

FABRE_QUEUE=$FABRE_DATA_DIR/fabre.synclog.list
rm -f $FABRE_QUEUE && touch $FABRE_QUEUE

N=0
while [ $N -le 99 ]; do
    DIR="$N"
    if [ $N -lt 10 ]; then
	DIR="0$N"
    fi

    echo "rsync --dry-run -avz rsync://bugs-mirror.debian.org/bts-spool-db/$DIR/ $DIR"
    rsync --dry-run -avz rsync://bugs-mirror.debian.org/bts-spool-db/$DIR/ $DIR | while read LINE; do
	case $LINE in
	    *.log)
		BASENAME=${LINE%.log}
		echo $BASENAME >> $FABRE_QUEUE
		;;
	    *)
		;;
	esac
    done
    N=$(( N + 1 ))
done
