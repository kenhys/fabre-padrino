#!/usr/bin/ruby
#
# Copyright 2021 by Kentaro Hayashi <kenhys@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require "optparse"
require "fileutils"
require "concurrent"

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib'))
require "fabre/command/build/pkgjson"

unless ENV["FABRE_DATA_DIR"]
  puts "ERROR: FABRE_DATA_DIR is empty"
  exit 1
end

pkgjson = Fabre::Command::Build::PkgJson.new
exit(pkgjson.run(ARGV))
