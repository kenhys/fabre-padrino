#!/bin/ruby

#
# Copyright 2020 by Kentaro Hayashi <kenhys@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

File.open(ARGV[0]) do |file|
  lines = file.readlines
  table = ""
  lines.each_with_index do |line, index|
    case line.chomp
    when /^load --table (.+)/
      table = $1
      puts start_index = index
    when /^\]/
      puts end_index = index
      File.open("#{table.downcase}.dump", "w+") do |output|
        output.puts(lines[start_index..end_index])
      end
    end
  end
end
