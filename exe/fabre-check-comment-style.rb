#!/usr/bin/ruby

#
# Copyright 2020 by Kentaro Hayashi <kenhys@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require "pg"
require "date"
require "optparse"
require "groonga"
require "kconv"
require "optparse"

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib'))
require "fabre/checker/format/comment"
$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'models'))
require "database"
require "comments"

begin
  checker = Fabre::Checker::CommentFormat.new
  checker.run(ARGV)
rescue StandardError => e
  puts e.message
  exit 1
end
