# How to install


$ sudo apt install libmagic-dev libsqlite3-dev ruby-dev libicu-dev zlib1g-dev g++ cmake libssl-dev
$ gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
$ curl -sSL https://get.rvm.io | bash -s stable
$ rvm autolibs disable
$ rvm install ruby-2.7.1
$ rvm use ruby-2.7.1@fabre --create --default
$ gem install bundler -v 1.17.3
$ bundle _1.17.3_ install
