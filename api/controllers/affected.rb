Fabre::Api.controllers :salsa do
  post :create, map: '/v1/affected/create', params: %i(authenticity_token bug username state api_key), provides: :json do
    data = {
      status: 403
    }
    open_database do
      @sessions = Fabre::Client::Sessions.new
      @users = Fabre::Client::Users.new
      if @sessions.authenticated?({
                                 key: params[:username],
                                 uuid: params[:state]
                               })
        @user = @users.find_by_username(params[:username])
        ids = @user["affected"].collect do |record|
          record._key
        end
        if ids.empty? or not ids.include?(params[:bug].to_i)
          ids << params[:bug].to_i
          @user["affected"] = ids
        end
        data = {
          status: 200
        }
      else
      end
      data.to_json
    end
  end
  
  post :delete, map: '/v1/affected/delete', params: %i(authenticity_token bug username state api_key), provides: :json do
    data = {
      status: 403
    }
    open_database do
      @sessions = Fabre::Client::Sessions.new
      @users = Fabre::Client::Users.new
      if @sessions.authenticated?({
                                 key: params[:username],
                                 uuid: params[:state]
                               })
        @user = @users.find_by_username(params[:username])
        ids = @user["affected"].collect do |record|
          record._key
        end
        unless ids.empty?
          ids.delete(params[:bug].to_i)
          @user["affected"] = ids
          data = {
            status: 200
          }
        end
      end
    end
    data.to_json
  end
end
