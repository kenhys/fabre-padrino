require "open-uri"

Fabre::Api.controllers :salsa do
  get :callback, map: '/v1/auth/salsa/callback', params: %i(code state) do
    form_data = {
      client_id: ENV["SALSA_APPLICATION_ID"],
      client_secret: ENV["SALSA_SECRET_ID"],
      redirect_uri: ENV["SALSA_REDIRECT_URI"],
      code: params[:code],
      grant_type: "authorization_code"
    }
    @auth = Fabre::Salsa::Authentication.new
    @auth.authenticate!(form_data)
    unless @auth.success? and session[:state] == params[:state]
      @salsa_user = nil
    else
      @salsa_user = @auth.user
      open_database do
        @session = Fabre::Client::Sessions.new
        uuid = SecureRandom.uuid
        @session.create({
                          key: uuid,
                          expired_at: Time.now.to_i + 60 * 60 * 24 * 7
                        })
        @user = Fabre::Client::Users.new
        @user = @user.create({
                               key: uuid,
                               display_name: @salsa_user.username,
                               avatar: @salsa_user.avatar_url
                             })
        session[:username] = @salsa_user.username
        session[:name] = @salsa_user.name
        session[:timezone] = @user.timezone
        session[:state] = uuid
        if @user["avatar_path"] and File.exist?(File.join("public", "images", @user["avatar_path"]))
          session[:avatar] = @user["avatar_path"]
        else
          session[:avatar] = @salsa_user.avatar_url

          begin
            URI.open(@salsa_user.avatar_url) do |f|
              Tempfile.create do |tmp|
                tmp.write(f.read)
                tmp.rewind
                mime = FileMagic.new(FileMagic::MAGIC_MIME).file(tmp.path).split(";")[0].to_s
                sub_directory = uuid[0..1].downcase
                if mime.start_with?("image/")
                  extention = mime.split("/").last
                  base_image_dir = File.join(
                    "public",
                    "images",
                    "avatar",
                    sub_directory)
                  FileUtils.mkdir_p(base_image_dir)
                  path = File.join(base_image_dir, "#{uuid}.#{extention}")
                  FileUtils.copy(tmp.path, path)
                  @user["avatar_path"] = File.join("avatar",
                                                   sub_directory,
                                                   "#{uuid}.#{extention}")
                else
                  path = nil
                end
              end
            rescue OpenURI::HTTPError
            end
          end

        end
        session[:weburl] = @salsa_user.web_url
        session[:email] = @salsa_user.email
      end
    end
    check_x_fabre_account
    render 'salsa/callback'
  end
end
