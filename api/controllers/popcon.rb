Fabre::Api.controllers :popcon do
  post :index, map: '/v1/popcon', with: [:username, :api_key], :csrf_protection => false do
    data = {
      status: 403
    }
    open_database do
      @users = Fabre::Client::Users.new
      options = {
        key: params[:username],
        api_key: params[:api_key]
      }
      if @users.api_authenticated?(options)
        mime = FileMagic.new(FileMagic::MAGIC_MIME).file(params[:popcondata][:tempfile].path).split(";")[0]
        path = File.join(ENV["FABRE_DATA_DIR"],
                         "uploads",
                         "#{params[:username]}.gpg")
        if mime == "application/gzip"
          `gunzip -c #{params[:popcondata][:tempfile].path} > #{path}`
        else
          FileUtils.copy(params[:popcondata][:tempfile].path, path)
        end
        parser = Fabre::Parser::Popcon.new({input: path, decrypt: true})
        metadata = parser.parse
        options = {
          key: params[:username],
          installed: metadata[:packages]
        }
        @users.update(options)
        data[:status] = 200
        data.merge!(metadata)
      end
      data.to_json
    end
  end
end
