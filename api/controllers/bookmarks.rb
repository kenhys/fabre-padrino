Fabre::Api.controllers :salsa do
  post :create, map: '/v1/bookmarks/create', params: %i(authenticity_token bug username state api_key), provides: :json do
    data = {
      status: 403
    }
    open_database do
      @sessions = Fabre::Client::Sessions.new
      @users = Fabre::Client::Users.new
      if @sessions.authenticated?({
                                 key: params[:username],
                                 uuid: params[:state]
                               })
        @user = @users.find_by_username(params[:username])
        ids = @user["bookmark"].collect do |record|
          record._key
        end
        if ids.empty? or not ids.include?(params[:bug].to_i)
          ids << params[:bug].to_i
          @user["bookmark"] = ids
        end
        data = {
          status: 200
        }
      else
        data = {
          title: "forbidden access to /v1/bookmarks/create",
          detail: "#{params[:username]} can't create a bookmark: <#{params[:bug]}>",
          status: 403
        }
      end
      data.to_json
    end
  end
  
  post :delete, map: '/v1/bookmarks/delete', params: %i(authenticity_token bug username state api_key), provides: :json do
    data = {
      status: 403
    }
    open_database do
      @sessions = Fabre::Client::Sessions.new
      @users = Fabre::Client::Users.new
      if @sessions.authenticated?({
                                 key: params[:uid],
                                 state: params[:state]
                               })
        @user = @users.find_by_username(params[:username])
        ids = @user["bookmark"].collect do |record|
          record._key
        end
        unless ids.empty?
          ids.delete(params[:bug].to_i)
          @user["bookmark"] = ids
          data = {
            status: 200
          }
        end
      else
        data = {
          title: "forbidden access to /v1/bookmarks/delete",
          detail: "#{params[:username]} can't delete a bookmark: <#{params[:bug]}>",
          status: 403
        }
      end
    end
    data.to_json
  end
end
