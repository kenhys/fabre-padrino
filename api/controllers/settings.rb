Fabre::Api.controllers :settings do
  post :index, map: "/v1/settings", :csrf_protection => false do
    data = {
      status: 403
    }
    open_database do
      @users = Fabre::Users.new
      options = {
        key: params[:username],
        api_key: params[:api_key]
      }
      if @users.api_authenticated?(options)
        tzinfo = TZInfo::Timezone.get(params[:timezone])
        options = {
          key: params[:username],
          display_score: params[:display_score] == "1",
          timezone: tzinfo.current_period.utc_offset
        }
        @users.update(options)
        data = {
          status: 200
        }
      end
      data.to_json
    end
  end
end
