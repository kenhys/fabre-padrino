module Fabre
  class Api
    module SalsaHelper
      def salsa_avatar_url
        ""
      end

      def page_title
        "Fabre - Yet another front-end of bugs.debian.org"
      end

      def check_x_fabre_account
        @enable_search = false
        @enable_login = false
        case request.env["HTTP_X_FABRE_ACCOUNT"]
        when "user"
          @enable_search = true
          @enable_login = true
        end
      end
    end

    helpers SalsaHelper
  end
end
